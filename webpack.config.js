const path = require('path');

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
        },
        fallback: {
            'stream': false,
            'http': false,
            'https': false,
            'os': false
        },
    },
};
