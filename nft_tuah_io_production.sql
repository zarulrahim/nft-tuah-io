--
-- PostgreSQL database dump
--

-- Dumped from database version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: campaigns; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.campaigns (
    id bigint NOT NULL,
    title character varying(255),
    remark character varying(255),
    start_date character varying(255) NOT NULL,
    end_date character varying(255) NOT NULL,
    target_amount character varying(255),
    user_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.campaigns OWNER TO admin;

--
-- Name: campaigns_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.campaigns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.campaigns_id_seq OWNER TO admin;

--
-- Name: campaigns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.campaigns_id_seq OWNED BY public.campaigns.id;


--
-- Name: event_custom_addresses; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.event_custom_addresses (
    id bigint NOT NULL,
    address character varying(255),
    campaign_id bigint,
    giveaway_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.event_custom_addresses OWNER TO admin;

--
-- Name: event_custom_addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.event_custom_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_custom_addresses_id_seq OWNER TO admin;

--
-- Name: event_custom_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.event_custom_addresses_id_seq OWNED BY public.event_custom_addresses.id;


--
-- Name: event_gifts; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.event_gifts (
    id bigint NOT NULL,
    redeem_quantity integer,
    hold_amount integer,
    gift_id bigint,
    campaign_id bigint,
    giveaway_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.event_gifts OWNER TO admin;

--
-- Name: event_gifts_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.event_gifts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_gifts_id_seq OWNER TO admin;

--
-- Name: event_gifts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.event_gifts_id_seq OWNED BY public.event_gifts.id;


--
-- Name: event_nfts; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.event_nfts (
    id bigint NOT NULL,
    nft_id character varying(255),
    campaign_id bigint,
    giveaway_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.event_nfts OWNER TO admin;

--
-- Name: event_nfts_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.event_nfts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_nfts_id_seq OWNER TO admin;

--
-- Name: event_nfts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.event_nfts_id_seq OWNED BY public.event_nfts.id;


--
-- Name: gift_custom_field_datas; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.gift_custom_field_datas (
    id bigint NOT NULL,
    value character varying(255) NOT NULL,
    redemption_id bigint NOT NULL,
    gift_custom_field_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.gift_custom_field_datas OWNER TO admin;

--
-- Name: gift_custom_field_datas_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.gift_custom_field_datas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gift_custom_field_datas_id_seq OWNER TO admin;

--
-- Name: gift_custom_field_datas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.gift_custom_field_datas_id_seq OWNED BY public.gift_custom_field_datas.id;


--
-- Name: gift_custom_fields; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.gift_custom_fields (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    gift_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.gift_custom_fields OWNER TO admin;

--
-- Name: gift_custom_fields_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.gift_custom_fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gift_custom_fields_id_seq OWNER TO admin;

--
-- Name: gift_custom_fields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.gift_custom_fields_id_seq OWNED BY public.gift_custom_fields.id;


--
-- Name: gifts; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.gifts (
    id bigint NOT NULL,
    title character varying(255),
    remark character varying(255),
    amount numeric(8,2),
    image_url character varying(255),
    user_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    gift_type_id integer,
    nft_collection_id bigint,
    nft_collection_group_id bigint
);


ALTER TABLE public.gifts OWNER TO admin;

--
-- Name: gifts_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.gifts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gifts_id_seq OWNER TO admin;

--
-- Name: gifts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.gifts_id_seq OWNED BY public.gifts.id;


--
-- Name: giveaways; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.giveaways (
    id bigint NOT NULL,
    title character varying(255),
    remark character varying(255),
    image_url character varying(255),
    start_date timestamp(0) without time zone NOT NULL,
    end_date timestamp(0) without time zone NOT NULL,
    user_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    type text
);


ALTER TABLE public.giveaways OWNER TO admin;

--
-- Name: giveaways_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.giveaways_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.giveaways_id_seq OWNER TO admin;

--
-- Name: giveaways_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.giveaways_id_seq OWNED BY public.giveaways.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO admin;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO admin;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: nft_collection_groups; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.nft_collection_groups (
    id bigint NOT NULL,
    title character varying(255),
    remark character varying(255),
    user_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.nft_collection_groups OWNER TO admin;

--
-- Name: nft_collection_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.nft_collection_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nft_collection_groups_id_seq OWNER TO admin;

--
-- Name: nft_collection_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.nft_collection_groups_id_seq OWNED BY public.nft_collection_groups.id;


--
-- Name: nft_collection_nft_collection_group; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.nft_collection_nft_collection_group (
    nft_collection_id bigint NOT NULL,
    nft_collection_group_id bigint NOT NULL
);


ALTER TABLE public.nft_collection_nft_collection_group OWNER TO admin;

--
-- Name: nft_collections; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.nft_collections (
    id bigint NOT NULL,
    title character varying(255),
    remark character varying(255),
    amount character varying(255),
    currency character varying(255),
    marketplace_url character varying(255),
    external_url character varying(255),
    image_url character varying(255),
    user_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.nft_collections OWNER TO admin;

--
-- Name: nft_collections_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.nft_collections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nft_collections_id_seq OWNER TO admin;

--
-- Name: nft_collections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.nft_collections_id_seq OWNED BY public.nft_collections.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO admin;

--
-- Name: permission_role; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.permission_role (
    role_id bigint NOT NULL,
    permission_id bigint NOT NULL
);


ALTER TABLE public.permission_role OWNER TO admin;

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    title character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO admin;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO admin;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: personal_access_tokens; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.personal_access_tokens OWNER TO admin;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_access_tokens_id_seq OWNER TO admin;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.personal_access_tokens_id_seq OWNED BY public.personal_access_tokens.id;


--
-- Name: redemption_gift_details; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.redemption_gift_details (
    id bigint NOT NULL,
    redemption_id bigint,
    nft_collection_id bigint,
    nft_collection_group_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.redemption_gift_details OWNER TO admin;

--
-- Name: redemption_gift_details_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.redemption_gift_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.redemption_gift_details_id_seq OWNER TO admin;

--
-- Name: redemption_gift_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.redemption_gift_details_id_seq OWNED BY public.redemption_gift_details.id;


--
-- Name: redemption_status_detail; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.redemption_status_detail (
    id bigint NOT NULL,
    redemption_id bigint,
    message character varying(255),
    status character varying(255),
    is_current boolean,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.redemption_status_detail OWNER TO admin;

--
-- Name: redemption_status_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.redemption_status_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.redemption_status_detail_id_seq OWNER TO admin;

--
-- Name: redemption_status_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.redemption_status_detail_id_seq OWNED BY public.redemption_status_detail.id;


--
-- Name: redemptions; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.redemptions (
    id bigint NOT NULL,
    name character varying(255),
    email character varying(255),
    twitter_username character varying(255),
    code character varying(255),
    status character varying(255),
    eth_address character varying(255),
    giveaway_id bigint,
    campaign_id bigint,
    gift_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.redemptions OWNER TO admin;

--
-- Name: redemptions_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.redemptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.redemptions_id_seq OWNER TO admin;

--
-- Name: redemptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.redemptions_id_seq OWNED BY public.redemptions.id;


--
-- Name: role_user; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.role_user (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    role_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.role_user OWNER TO admin;

--
-- Name: role_user_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.role_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_user_id_seq OWNER TO admin;

--
-- Name: role_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.role_user_id_seq OWNED BY public.role_user.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    title character varying(255)
);


ALTER TABLE public.roles OWNER TO admin;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO admin;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.sessions (
    id character varying(255) NOT NULL,
    user_id bigint,
    ip_address character varying(45),
    user_agent text,
    payload text NOT NULL,
    last_activity integer NOT NULL
);


ALTER TABLE public.sessions OWNER TO admin;

--
-- Name: subscription_packages; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.subscription_packages (
    id bigint NOT NULL,
    title character varying(255),
    remark character varying(255),
    rate_per_month double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.subscription_packages OWNER TO admin;

--
-- Name: subscription_packages_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.subscription_packages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subscription_packages_id_seq OWNER TO admin;

--
-- Name: subscription_packages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.subscription_packages_id_seq OWNED BY public.subscription_packages.id;


--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.subscriptions (
    id bigint NOT NULL,
    status character varying(255) DEFAULT 'pending'::character varying,
    validity_in_days integer,
    user_id bigint,
    subscription_package_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.subscriptions OWNER TO admin;

--
-- Name: subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subscriptions_id_seq OWNER TO admin;

--
-- Name: subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.subscriptions_id_seq OWNED BY public.subscriptions.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255),
    email character varying(255),
    eth_address character varying(255) NOT NULL,
    subdomain character varying(255),
    status character varying(255) DEFAULT 'pending'::character varying,
    email_verified_at timestamp(0) without time zone,
    password character varying(255),
    api_token character varying(80),
    remember_token character varying(100),
    current_team_id bigint,
    profile_photo_path character varying(2048),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    two_factor_secret text,
    two_factor_recovery_codes text
);


ALTER TABLE public.users OWNER TO admin;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO admin;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: campaigns id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.campaigns ALTER COLUMN id SET DEFAULT nextval('public.campaigns_id_seq'::regclass);


--
-- Name: event_custom_addresses id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.event_custom_addresses ALTER COLUMN id SET DEFAULT nextval('public.event_custom_addresses_id_seq'::regclass);


--
-- Name: event_gifts id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.event_gifts ALTER COLUMN id SET DEFAULT nextval('public.event_gifts_id_seq'::regclass);


--
-- Name: event_nfts id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.event_nfts ALTER COLUMN id SET DEFAULT nextval('public.event_nfts_id_seq'::regclass);


--
-- Name: gift_custom_field_datas id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.gift_custom_field_datas ALTER COLUMN id SET DEFAULT nextval('public.gift_custom_field_datas_id_seq'::regclass);


--
-- Name: gift_custom_fields id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.gift_custom_fields ALTER COLUMN id SET DEFAULT nextval('public.gift_custom_fields_id_seq'::regclass);


--
-- Name: gifts id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.gifts ALTER COLUMN id SET DEFAULT nextval('public.gifts_id_seq'::regclass);


--
-- Name: giveaways id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.giveaways ALTER COLUMN id SET DEFAULT nextval('public.giveaways_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: nft_collection_groups id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.nft_collection_groups ALTER COLUMN id SET DEFAULT nextval('public.nft_collection_groups_id_seq'::regclass);


--
-- Name: nft_collections id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.nft_collections ALTER COLUMN id SET DEFAULT nextval('public.nft_collections_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: personal_access_tokens id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.personal_access_tokens_id_seq'::regclass);


--
-- Name: redemption_gift_details id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.redemption_gift_details ALTER COLUMN id SET DEFAULT nextval('public.redemption_gift_details_id_seq'::regclass);


--
-- Name: redemption_status_detail id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.redemption_status_detail ALTER COLUMN id SET DEFAULT nextval('public.redemption_status_detail_id_seq'::regclass);


--
-- Name: redemptions id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.redemptions ALTER COLUMN id SET DEFAULT nextval('public.redemptions_id_seq'::regclass);


--
-- Name: role_user id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.role_user ALTER COLUMN id SET DEFAULT nextval('public.role_user_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: subscription_packages id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.subscription_packages ALTER COLUMN id SET DEFAULT nextval('public.subscription_packages_id_seq'::regclass);


--
-- Name: subscriptions id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.subscriptions ALTER COLUMN id SET DEFAULT nextval('public.subscriptions_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: campaigns; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.campaigns (id, title, remark, start_date, end_date, target_amount, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: event_custom_addresses; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.event_custom_addresses (id, address, campaign_id, giveaway_id, created_at, updated_at) FROM stdin;
13	0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0	\N	10	2022-02-08 11:30:40	2022-02-08 11:30:40
14	0x7f0432ef6E3742c4406737aB66A6FeD46371b223	\N	10	2022-02-08 11:30:40	2022-02-08 11:30:40
12	0x87b463374Ee072DaDD19D5Eb67878c93e9757352	\N	10	2022-02-08 11:30:40	2022-02-08 11:58:53
19	0x61dEEafdDEf6cAB550e45E075d0A7283Ecdbd378	\N	10	2022-02-09 00:44:34	2022-02-09 00:44:34
20	0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0	\N	11	2022-02-20 21:16:39	2022-02-20 21:16:39
21	0x818ad13e1f690a9f3edcf54ee19e08ad661bfc1a	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
22	0xC019146EE69233Be53c96bA2778A5267ce7f7E8D	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
23	0x00A57fF08Eb1D8171e5eA6FC94fE7D0d615d9700	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
24	0x5D28b41D126928aB2205113d91e85453ECbAF218	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
25	0x950FEC7e16506E2f3738D95cC20C2b55E8A970F5	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
26	0x608e5393C1cd3E7ba0E287fC53ac1b16f358Abb4	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
27	0xc3d72590d7123fdF9A39922269360A668505aa78	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
28	0x87694bDD4A633173de441D09fa3D9b65E1B9E34a	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
29	0x2050621857880c745feff1127B30c2336a2Dca1a	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
30	0x7B84e57B94f94D9DE4782bE5149B49Fe10fcc376	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
31	0xa01dd358Cd194b3adFd7a2A8477bB7f25C69374a	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
32	0xF153F68357681Dec5aC068E26ce18387585870B3	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
33	0x4d4ef32e5330161471da87f5dd972c1b2e838041	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
34	0x18Fda583DB788DF6BB38cB44a8f45bd8Eb2E5749	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
35	0x048eefab600f52d0d3b83012ff92a4c8ab419fca	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
36	0xad39143e77468cD057Fbf37150840CCFCeE1F385	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
37	0xEa31A2C5F42D7E7c815c2c2222E6E97A25818E91	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
38	0x9cdD9fDdC0e712858B3b42086F9d875a9707D472	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
39	0x28cd01Ba1686574B4e0EF848862BC1e9CBFe4fFE	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
40	0xF35fC7Feaa2e17253074C473f8C9a58BF5461C78	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
41	0x14b37280FcC04256c7fc03d5a7B1e8564eE28A8f	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
42	0x0fB46b8438B757663c91aE728E36deCce2C4d000	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
43	0x23ff048719fd3b0f33CbE1184178878B1781DEDa	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
44	0xF1D8F13Fd137669ac768EDf467f358200FafbA72	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
45	0xaD52182dc4fCbB6F91cF5b1e1D61e170f885513A	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
46	0x0e4eCf1aF7C603adF59e2B4953569d79a4087DE4	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
47	0x2f56f0435274C12A19016F86329028D295994abD	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
48	0xfB55ba1be6aeD490Ba58bAD9517308eBcA8501B2	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
49	0xA26780eb85347B069351E403F7F8724F2331Ef38	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
50	0x2c2882399cB958BbB682C8a0976B590B451A32bB	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
51	0x828DE0799953E81f8D3D9f160bc5c4C8a5D40E4E	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
52	0xFe2E1D458a4EdaB962179DA3A7CDd8bFABf39B81	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
53	0xFe2E1D458a4EdaB962179DA3A7CDd8bFABf39B81	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
54	0xA318B977Ec8E4b04e9715327a1F9db70A0680Dd1	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
55	0x40D7DC757b1Ae760bFB372402Be622fF2A2D482F	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
56	0xfF7eB276A2F499501Cc4376125D9879C1Bc6bA3f	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
57	0xa34254C9d79f03cE838e5005719f8e77cbDe444c	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
58	0xbb83322decb10c79c18ae1945c997db333c37f85	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
59	0x44c264F47A5ee082CB99C23d9036484f573dCe4a	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
60	0x253Dcb4e61F1E715fcfd478b9B9Ba9e01a448dcb	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
61	0xA58358bE7d94B48b830575AA516e32F398267716	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
62	0xf186c01A6b429eca496ed0bAa46c61170f5fA9C8	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
63	0xacd9Af3C539331d667A5b45aFbc32F82d42858E5	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
64	0x17D8bcb904f7f9d45DC2974C796D20Ed73332fEe	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
65	0xabC32FA26c041852f5B6E1BAb81558F6d68f31ba	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
66	0xB880d7FE168042c431B20745E47ea6Dc7a7f071e	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
67	0xAd540883342B74EfA65A2B8345f4416152f429dF	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
68	0xd3a037A5D53A972baF17A3bb28fA317dB1c18014	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
69	0xf5FF6CE038D5470FC1c592263C0964554E2C0262	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
70	0x26dF7FD3213Ddaac527C76B7C60a8fE16E90B2e5	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
71	0x0987df284C40FA7Ecc5fe01d16604b0Ad00F7902	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
72	0x57Bc42cB21A149e3Bcd3e085AD98Ec58C751177E	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
73	0x3a39eeae464344d2999132bddacf415112525b28	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
74	0xb738946a4a97B86D0C3BEbc32c0632Ff0EeB4381	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
75	0x1a70fd5fc8725e0d0cf0d1b2e64a029dc1c7625d	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
76	0x021cb9dd7c0fa60ede1b35dbccc52757c21e11ec	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
77	0x3D3B18cE0786bF6be9611C80f1D280554026A37f	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
78	0x69FE55b13Dc66FE6c19e88525Bf890740fb28600	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
79	0x0C41337a9CCd52895636D212fa0115A3BcFC8DD5	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
80	0x44d33C835b7a20EeC91f451E62222b62f9481450	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
81	0x3F274e69bc09eD3276cCf0C982a655d3941de546	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
82	0xC2595D811420BA76CC2E85b0b60c66b2B3d3Ac14	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
83	0x60A0c5E2a677AbB499568b4fB422873bb3433204	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
84	0x0CC97AcF1B54D56e2A05d2055A14feD2a98A800B	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
85	0x9C17A813b40201b7bE5BfF5E964C7343559Ad3bF	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
86	0x48C099D3046976650032320bd15cABd4dab2F6dD	\N	12	2022-02-21 18:31:53	2022-02-21 18:31:53
87	0x4D4EF32E5330161471dA87F5dD972C1b2e838041	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
88	0x048eEfaB600F52d0D3B83012fF92A4c8aB419FCa	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
89	0xbb83322dEcB10c79C18AE1945c997Db333C37f85	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
90	0x3a39Eeae464344D2999132BDdaCF415112525B28	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
91	0x94f25e4DC37459dEB42AE9a7E2Fb4235dE4A8D33	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
92	0x818ad13e1f690A9F3edCF54Ee19E08AD661bfc1A	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
93	0x1a70fD5Fc8725E0d0Cf0D1b2E64a029DC1c7625D	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
94	0x021cb9DD7c0fa60eDe1b35DbccC52757C21E11EC	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
\.


--
-- Data for Name: event_gifts; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.event_gifts (id, redeem_quantity, hold_amount, gift_id, campaign_id, giveaway_id, created_at, updated_at) FROM stdin;
10	10	10	10	\N	10	2022-02-08 11:30:40	2022-02-08 11:30:40
13	10	10	11	\N	11	2022-02-20 21:16:39	2022-02-20 21:16:39
14	10	10	13	\N	12	2022-02-21 18:31:52	2022-02-21 18:31:52
17	10	10	14	\N	12	2022-02-21 23:22:35	2022-02-21 23:22:35
18	10	10	17	\N	12	2022-02-22 13:44:53	2022-02-22 13:44:53
19	10	10	18	\N	12	2022-02-22 13:45:34	2022-02-22 13:45:34
20	10	10	19	\N	12	2022-02-22 16:03:23	2022-02-22 16:03:23
\.


--
-- Data for Name: event_nfts; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.event_nfts (id, nft_id, campaign_id, giveaway_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: gift_custom_field_datas; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.gift_custom_field_datas (id, value, redemption_id, gift_custom_field_id, created_at, updated_at) FROM stdin;
1	XL	1	2	2022-02-04 03:32:13	2022-02-04 03:32:13
\.


--
-- Data for Name: gift_custom_fields; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.gift_custom_fields (id, name, type, gift_id, created_at, updated_at) FROM stdin;
1	Size	text	1	2022-02-03 15:07:52	2022-02-03 15:07:52
6	Location Outlet	text	14	2022-03-13 14:30:01	2022-03-13 14:30:01
7	Location Outlet	text	15	2022-03-13 14:30:04	2022-03-13 14:30:04
8	Location Outlet	text	16	2022-03-13 14:30:19	2022-03-13 14:30:19
9	Lokasi Outlet	text	17	2022-03-13 14:30:58	2022-03-13 14:30:58
\.


--
-- Data for Name: gifts; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.gifts (id, title, remark, amount, image_url, user_id, created_at, updated_at, gift_type_id, nft_collection_id, nft_collection_group_id) FROM stdin;
1	Test	testing..	0.00	tuah-io-gift-img-test-1643872072WJKNoY.png	4	2022-02-03 15:07:52	2022-02-03 15:07:52	\N	\N	\N
10	Robo Studio 200 Random Airdrops Giveaway	Exclusive 22.2.22	0.00	images/tuahnft/gifts/tuah-io-gift-img-10-16442918894BSTH6.png	1	2022-02-08 11:28:54	2022-02-08 11:44:49	3	\N	5
11	FIFA 9 Random Players Gift	Win a chance to get random one NFT out of 9	0.00	images/tuahnft/gifts/tuah-io-gift-img-11-1645363405xtI8Pv.jpeg	1	2022-02-20 21:16:20	2022-02-20 21:23:25	3	\N	6
13	Phase 2 Robots	1 Robo for 1 Wallet	0.00	\N	6	2022-02-21 18:21:54	2022-02-21 18:21:54	3	\N	7
14	Kopi Saigon	Sila Pilih Lokasi Outlet (Lihat Sini https://kopisaigon.com/lokasi/)	1.00	images/orkesahizadin/gifts/tuah-io-gift-img-14-1647153001xbz29k.png	45	2022-03-13 14:30:01	2022-03-13 14:30:01	1	\N	\N
15	Kopi Saigon	Sila Pilih Lokasi Outlet (Lihat Sini https://kopisaigon.com/lokasi/)	1.00	images/orkesahizadin/gifts/tuah-io-gift-img-15-1647153004jXsRC3.png	45	2022-03-13 14:30:04	2022-03-13 14:30:05	1	\N	\N
16	Kopi Saigon	Sila Pilih Lokasi Outlet (Lihat Sini https://kopisaigon.com/lokasi/)	1.00	images/orkesahizadin/gifts/tuah-io-gift-img-16-1647153019jK6Cig.png	45	2022-03-13 14:30:19	2022-03-13 14:30:20	1	\N	\N
17	aa	aaa	0.00	images/orkesahizadin/gifts/tuah-io-gift-img-17-1647153058YV8h5S.png	45	2022-03-13 14:30:58	2022-03-13 14:30:59	1	\N	\N
\.


--
-- Data for Name: giveaways; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.giveaways (id, title, remark, image_url, start_date, end_date, user_id, created_at, updated_at, type) FROM stdin;
10	Robo Studio 22.2.22 Special Whitelist Giveaway	Exclusive 22.2.22 giveaway!	images/tuahnft/giveaways/tuah-io-ga-img-10-1644291040yimGWs.png	2022-01-28 00:00:00	2022-02-24 00:00:00	1	2022-02-08 11:30:40	2022-02-09 00:44:34	1
11	FIFA NFT Special Whitelist Giveaway	Exclusive 22.2.22 giveaway!	images/tuahnft/giveaways/tuah-io-ga-img-11-1645362999d5h0nS.jpeg	2022-02-01 00:00:00	2022-02-28 00:00:00	1	2022-02-20 21:16:39	2022-02-20 21:16:39	1
12	Redeem Your Bots	1 Robo for 1 Wallet	images/robostudio/giveaways/tuah-io-ga-img-12-1645456955LpOHTO.jpeg	2022-02-19 00:00:00	2022-02-21 00:00:00	6	2022-02-21 18:31:52	2022-02-22 16:03:23	1
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.migrations (id, migration, batch) FROM stdin;
21	2014_10_12_000000_create_users_table	1
22	2014_10_12_100000_create_password_resets_table	1
23	2014_10_12_200000_add_two_factor_columns_to_users_table	1
24	2019_12_14_000001_create_personal_access_tokens_table	1
25	2021_12_20_053406_create_sessions_table	1
26	2021_12_28_160622_create_roles_table	1
27	2021_12_28_162902_create_role_user_table	1
28	2021_12_28_183423_create_permissions_table	1
29	2021_12_28_183522_create_permission_role_pivot_table	1
30	2022_01_08_115928_create_campaigns_table	1
31	2022_01_08_120334_create_giveaways_table	1
32	2022_01_08_120559_create_gifts_table	1
33	2022_01_08_120943_create_event_gifts_table	1
34	2022_01_08_121122_create_event_nfts_table	1
35	2022_01_11_182422_create_redemptions_table	1
36	2022_01_13_052433_create_subscription_packages_table	1
37	2022_01_13_061124_create_subscriptions_table	1
38	2022_01_19_105306_create_gift_custom_fields_table	1
39	2022_01_19_125142_create_gift_custom_field_datas_table	1
40	2022_01_22_021433_create_redemption_status_detail_table	1
41	2022_02_04_003624_create_event_custom_addresses_table	2
42	2022_02_04_005816_add_type_into_giveaways_table	2
43	2022_02_05_200727_create_nft_collections_table	3
44	2022_02_05_200736_create_nft_collection_groups_table	3
45	2022_02_05_212634_create_nft_collections_group_pivot_table	3
46	2022_02_06_145658_rename_table	4
47	2022_02_06_234046_add_columns_to_gifts_table	5
48	2022_02_07_133806_create_redemption_gift_details_table	6
\.


--
-- Data for Name: nft_collection_groups; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.nft_collection_groups (id, title, remark, user_id, created_at, updated_at) FROM stdin;
5	MR ROBOT SERIES	\N	1	2022-02-08 11:23:00	2022-02-08 11:23:00
6	Top Players All Time FIFA	\N	1	2022-02-20 21:05:38	2022-02-20 21:05:38
7	Whitelist	\N	6	2022-02-21 17:15:53	2022-02-21 17:15:53
\.


--
-- Data for Name: nft_collection_nft_collection_group; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.nft_collection_nft_collection_group (nft_collection_id, nft_collection_group_id) FROM stdin;
12	5
13	5
23	6
24	6
16	6
17	6
18	6
19	6
20	6
21	6
22	6
41	7
50	7
25	7
28	7
27	7
26	7
43	7
29	7
30	7
31	7
32	7
33	7
35	7
36	7
37	7
38	7
39	7
40	7
44	7
34	7
45	7
46	7
47	7
48	7
49	7
51	7
52	7
53	7
55	7
56	7
57	7
58	7
59	7
60	7
61	7
62	7
54	7
63	7
64	7
65	7
66	7
67	7
68	7
69	7
70	7
71	7
72	7
73	7
74	7
75	7
76	7
77	7
78	7
79	7
80	7
81	7
82	7
83	7
84	7
85	7
91	7
87	7
89	7
90	7
88	7
86	7
93	7
94	7
95	7
96	7
97	7
98	7
99	7
100	7
101	7
102	7
103	7
104	7
106	7
107	7
108	7
109	7
110	7
111	7
112	7
113	7
115	7
116	7
114	7
117	7
118	7
121	7
122	7
123	7
124	7
125	7
126	7
127	7
128	7
129	7
130	7
131	7
132	7
133	7
134	7
135	7
136	7
137	7
138	7
139	7
140	7
141	7
142	7
143	7
146	7
145	7
144	7
119	7
147	7
148	7
149	7
150	7
151	7
152	7
153	7
154	7
155	7
156	7
157	7
158	7
159	7
160	7
161	7
162	7
163	7
164	7
165	7
166	7
167	7
168	7
169	7
170	7
171	7
172	7
173	7
174	7
175	7
176	7
177	7
178	7
179	7
180	7
181	7
182	7
183	7
184	7
185	7
186	7
187	7
188	7
189	7
190	7
191	7
192	7
193	7
194	7
195	7
196	7
197	7
198	7
199	7
200	7
201	7
202	7
204	7
203	7
92	7
120	7
205	7
206	7
207	7
208	7
209	7
\.


--
-- Data for Name: nft_collections; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.nft_collections (id, title, remark, amount, currency, marketplace_url, external_url, image_url, user_id, created_at, updated_at) FROM stdin;
23	Neymar #10	Neymar - PSG - 10	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-23-1645362238WDvWee.jpeg	1	2022-02-20 21:03:58	2022-02-20 21:03:59
24	Ronaldo #07	Ronaldo - MU - 7	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-24-1645362279YgSNnM.jpeg	1	2022-02-20 21:04:39	2022-02-20 21:04:39
41	MrRobot 117	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-41-1645415138RJeF30.png	6	2022-02-21 11:45:38	2022-02-21 11:45:38
50	MrRobot 126	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-50-1645421620TPNnVq.png	6	2022-02-21 13:33:40	2022-02-21 13:33:41
25	MrRobot 101	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-25-1645414626uJZH0t.png	6	2022-02-21 11:37:06	2022-02-21 11:38:52
28	MrRobot 104	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-28-16454147171hs9Ud.png	6	2022-02-21 11:38:37	2022-02-21 11:39:04
27	MrRobot 103	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-27-164541468967tP9q.png	6	2022-02-21 11:38:09	2022-02-21 11:39:15
26	MrRobot 102	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-26-1645414674Urxfby.png	6	2022-02-21 11:37:54	2022-02-21 11:39:23
43	MrRobot 119	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-43-16454151806MgQyQ.png	6	2022-02-21 11:46:20	2022-02-21 11:46:21
29	MrRobot 105	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-29-1645414788WIEp12.png	6	2022-02-21 11:39:48	2022-02-21 11:40:10
12	MR. ROBOT #0099 _ GANG	\N	0.8	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-12-1644289704ULyN5Z.jpeg	1	2022-02-08 10:54:32	2022-02-08 11:08:24
13	MR. ROBOT #0098 _ PROPHET	\N	0.3	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-13-1644289760TplFqX.jpeg	1	2022-02-08 11:09:20	2022-02-08 23:22:27
30	MrRobot 106	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-30-1645414839GvKXJQ.png	6	2022-02-21 11:40:39	2022-02-21 11:40:40
16	Lionel Messi #10	Lionel Messi - Barcelona - 10	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-16-1645361964Rs9s1M.jpeg	1	2022-02-20 20:59:24	2022-02-20 20:59:24
17	Hazard #10	Hazard - Chelsea - 10	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-17-1645362013Pf8rcc.jpeg	1	2022-02-20 21:00:13	2022-02-20 21:00:13
18	M Salah #11	M Salah - Liverpool - 11	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-18-1645362053E2zZrb.jpeg	1	2022-02-20 21:00:53	2022-02-20 21:00:53
19	Haaland #17	Haaland - Dortmund - 17	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-19-1645362084C2A4LV.jpeg	1	2022-02-20 21:01:24	2022-02-20 21:01:24
20	Kroos #8	Kroos - Real Madrid - 8	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-20-1645362126kClrdi.jpeg	1	2022-02-20 21:02:06	2022-02-20 21:02:06
21	Buffon #77	Buffon - Juventus - 77	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-21-1645362160yTwI3S.jpeg	1	2022-02-20 21:02:40	2022-02-20 21:02:40
22	Lewandowski #9	Lewandowski - Bayern Munich - 9	0.1	BNB	\N	\N	images/tuahnft/nft_collections/tuah-io-nft-img-22-164536219078J8cN.jpeg	1	2022-02-20 21:03:10	2022-02-20 21:03:10
31	MrRobot 107	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-31-1645414863k7eJBu.png	6	2022-02-21 11:41:03	2022-02-21 11:41:04
32	MrRobot 108	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-32-1645414890ZfIQnA.png	6	2022-02-21 11:41:30	2022-02-21 11:41:31
33	MrRobot 109	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-33-16454149314rxinB.png	6	2022-02-21 11:42:11	2022-02-21 11:42:12
35	MrRobot 111	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-35-1645415007zWzgfw.png	6	2022-02-21 11:43:27	2022-02-21 11:43:28
36	MrRobot 112	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-36-1645415030Pe4xYv.png	6	2022-02-21 11:43:50	2022-02-21 11:43:51
37	MrRobot 113	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-37-1645415059hqt2Tp.png	6	2022-02-21 11:44:19	2022-02-21 11:44:20
38	MrRobot 114	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-38-1645415076aUcIsi.png	6	2022-02-21 11:44:36	2022-02-21 11:44:37
39	MrRobot 115	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-39-1645415103L4m6PK.png	6	2022-02-21 11:45:03	2022-02-21 11:45:03
40	MrRobot 116	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-40-16454151239oONrg.png	6	2022-02-21 11:45:23	2022-02-21 11:45:24
44	MrRobot 120	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-44-16454152323GHhH8.png	6	2022-02-21 11:47:12	2022-02-21 11:47:13
34	MrRobot 110	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-34-1645414956ddioLh.png	6	2022-02-21 11:42:36	2022-02-21 11:47:24
45	MrRobot 121	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-45-1645421114OGOa3Z.png	6	2022-02-21 13:25:14	2022-02-21 13:25:14
46	MrRobot 122	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-46-1645421132d1qs0D.png	6	2022-02-21 13:25:32	2022-02-21 13:25:33
47	MrRobot 123	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-47-1645421167aTOSpW.png	6	2022-02-21 13:26:07	2022-02-21 13:26:08
48	MrRobot 124	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-48-1645421583H3kUHP.png	6	2022-02-21 13:33:03	2022-02-21 13:33:03
49	MrRobot 125	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-49-1645421604Xe5oIs.png	6	2022-02-21 13:33:24	2022-02-21 13:33:25
51	MrRobot 127	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-51-1645421668c1DJSo.png	6	2022-02-21 13:34:28	2022-02-21 13:34:29
52	MrRobot 128	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-52-1645421688n0dm8o.png	6	2022-02-21 13:34:48	2022-02-21 13:34:49
53	MrRobot 129	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-53-1645421717eSWluv.png	6	2022-02-21 13:35:17	2022-02-21 13:35:18
55	MrRobot 131	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-55-16454217563Y2E5C.png	6	2022-02-21 13:35:56	2022-02-21 13:40:14
56	MrRobot 132	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-56-16454217853Mu29m.png	6	2022-02-21 13:36:25	2022-02-21 13:36:26
57	MrRobot 133	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-57-1645421818tDqEJ8.png	6	2022-02-21 13:36:58	2022-02-21 13:36:58
58	MrRobot 134	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-58-1645421843fVqv6G.png	6	2022-02-21 13:37:23	2022-02-21 13:37:24
59	MrRobot 135	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-59-1645421873pTQGzF.png	6	2022-02-21 13:37:53	2022-02-21 13:37:54
60	MrRobot 136	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-60-1645421893d1gWVp.png	6	2022-02-21 13:38:13	2022-02-21 13:38:14
61	MrRobot 137	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-61-1645421927vrnmmA.png	6	2022-02-21 13:38:47	2022-02-21 13:38:48
62	MrRobot 138	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-62-1645421949d57N6N.png	6	2022-02-21 13:39:09	2022-02-21 13:39:10
54	MrRobot 130	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-54-1645421734gUdVad.png	6	2022-02-21 13:35:34	2022-02-21 13:39:49
63	MrRobot 139	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-63-1645421974AlUkim.png	6	2022-02-21 13:39:34	2022-02-21 13:40:32
64	MrRobot 140	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-64-1645422133eTwVwV.png	6	2022-02-21 13:42:13	2022-02-21 13:42:14
65	MrRobot 141	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-65-16454221688P4TW3.png	6	2022-02-21 13:42:48	2022-02-21 13:42:48
66	MrRobot 142	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-66-1645422187jfpUJ2.png	6	2022-02-21 13:43:07	2022-02-21 13:43:07
67	MrRobot 143	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-67-1645422206aulflf.png	6	2022-02-21 13:43:26	2022-02-21 13:43:27
68	MrRobot 144	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-68-1645422387PFBQWN.png	6	2022-02-21 13:46:27	2022-02-21 13:46:27
69	MrRobot 145	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-69-1645422419kdr42a.png	6	2022-02-21 13:46:59	2022-02-21 13:47:00
70	MrRobot 146	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-70-16454224352HkFRc.png	6	2022-02-21 13:47:15	2022-02-21 13:47:15
71	MrRobot 147	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-71-16454224594sPGpH.png	6	2022-02-21 13:47:39	2022-02-21 13:47:40
72	MrRobot 148	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-72-1645422504pQf04a.png	6	2022-02-21 13:48:24	2022-02-21 13:48:24
73	MrRobot 149	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-73-1645422528MB5Kic.png	6	2022-02-21 13:48:48	2022-02-21 13:48:48
74	MrRobot 150	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-74-1645422550ECeM2M.png	6	2022-02-21 13:49:10	2022-02-21 13:49:11
75	MrRobot 151	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-75-1645422573efXnJQ.png	6	2022-02-21 13:49:33	2022-02-21 13:49:33
76	MrRobot 152	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-76-1645422592jdYOoo.png	6	2022-02-21 13:49:52	2022-02-21 13:49:52
77	MrRobot 153	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-77-1645422656odXdAl.png	6	2022-02-21 13:50:56	2022-02-21 13:50:56
78	MrRobot 154	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-78-1645422672sQPUOs.png	6	2022-02-21 13:51:12	2022-02-21 13:51:12
79	MrRobot 155	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-79-1645422691sHsqMD.png	6	2022-02-21 13:51:31	2022-02-21 13:52:17
80	MrRobot 156	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-80-1645422707Jv5wFs.png	6	2022-02-21 13:51:47	2022-02-21 13:52:32
81	MrRobot 157	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-81-1645422785kfvSw7.png	6	2022-02-21 13:53:05	2022-02-21 13:53:06
82	MrRobot 158	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-82-1645422817MGRz6z.png	6	2022-02-21 13:53:37	2022-02-21 13:53:37
83	MrRobot 159	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-83-1645422839mRgTUS.png	6	2022-02-21 13:53:59	2022-02-21 13:54:00
84	MrRobot 160	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-84-16454228664ixbeL.png	6	2022-02-21 13:54:26	2022-02-21 13:54:27
85	MrRobot 161	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-85-1645422883r9lDR7.png	6	2022-02-21 13:54:43	2022-02-21 13:54:43
91	MrRobot 168	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-91-1645423091VBys3d.png	6	2022-02-21 13:58:11	2022-02-21 17:07:42
87	MrRobot 163	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-87-16454229391IC8jm.png	6	2022-02-21 13:55:39	2022-02-21 13:55:40
89	MrRobot 166	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-89-1645422984337iVl.png	6	2022-02-21 13:56:24	2022-02-21 13:56:25
90	MrRobot 167	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-90-1645423012XvWaJT.png	6	2022-02-21 13:56:52	2022-02-21 13:56:53
88	MrRobot 164	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-88-1645422958pJmXy3.png	6	2022-02-21 13:55:58	2022-02-21 13:57:55
86	MrRobot 162	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-86-1645422916Zb9XTZ.png	6	2022-02-21 13:55:16	2022-02-21 17:07:32
93	MrRobot 170	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-93-1645423150NfirhJ.png	6	2022-02-21 13:59:10	2022-02-21 13:59:11
94	MrRobot 171	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-94-1645423179pRgaFl.png	6	2022-02-21 13:59:39	2022-02-21 13:59:39
95	MrRobot 172	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-95-1645423199bc7k8e.png	6	2022-02-21 13:59:59	2022-02-21 14:00:00
96	MrRobot 173	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-96-1645423220KEk6rE.png	6	2022-02-21 14:00:20	2022-02-21 14:00:21
97	MrRobot 174	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-97-1645423249GJKCgQ.png	6	2022-02-21 14:00:49	2022-02-21 14:00:50
98	MrRobot 175	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-98-1645423281k4vcgh.png	6	2022-02-21 14:01:21	2022-02-21 14:01:21
99	MrRobot 176	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-99-1645423315ONI8cV.png	6	2022-02-21 14:01:55	2022-02-21 14:01:55
100	MrRobot 177	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-100-1645423344qFAHv7.png	6	2022-02-21 14:02:24	2022-02-21 14:02:25
101	MrRobot 178	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-101-1645423368wUUAU7.png	6	2022-02-21 14:02:48	2022-02-21 14:02:49
102	MrRobot 179	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-102-1645423406vknJ2x.png	6	2022-02-21 14:03:26	2022-02-21 14:03:27
103	MrRobot 180	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-103-16454234298ITkwT.png	6	2022-02-21 14:03:49	2022-02-21 14:03:50
104	MrRobot 181	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-104-1645423466sw8uNp.png	6	2022-02-21 14:04:26	2022-02-21 14:04:27
106	MrRobot 183	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-106-1645423514cEI5jl.png	6	2022-02-21 14:05:14	2022-02-21 14:05:14
107	MrRobot 184	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-107-1645423537nWWmmX.png	6	2022-02-21 14:05:37	2022-02-21 14:05:38
108	MrRobot 185	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-108-1645423562758Jpt.png	6	2022-02-21 14:06:02	2022-02-21 14:06:03
109	MrRobot 186	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-109-1645423591EQUcos.png	6	2022-02-21 14:06:31	2022-02-21 14:06:31
110	MrRobot 187	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-110-1645423613yrlD9B.png	6	2022-02-21 14:06:53	2022-02-21 14:06:53
111	MrRobot 188	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-111-1645423642ZT5PuO.png	6	2022-02-21 14:07:22	2022-02-21 14:07:23
112	MrRobot 189	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-112-1645423673o33JgD.png	6	2022-02-21 14:07:53	2022-02-21 14:07:53
113	MrRobot 190	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-113-1645423737sKHJmf.png	6	2022-02-21 14:08:57	2022-02-21 14:08:57
115	MrRobot 192	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-115-16454237940KmKTB.png	6	2022-02-21 14:09:54	2022-02-21 14:09:55
116	MrRobot 193	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-116-1645423816ie8lu3.png	6	2022-02-21 14:10:16	2022-02-21 14:10:17
114	MrRobot 191	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-114-1645423756PVwSMg.png	6	2022-02-21 14:09:16	2022-02-21 14:10:32
117	MrRobot 194	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-117-1645423890DYhjr4.png	6	2022-02-21 14:11:30	2022-02-21 14:11:31
118	MrRobot 195	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-118-1645423919xelXwB.png	6	2022-02-21 14:11:59	2022-02-21 14:12:00
121	MrRobot 198	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-121-1645424002wuktB9.png	6	2022-02-21 14:13:22	2022-02-21 14:13:22
122	MrRobot 199	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-122-1645424022xWIFfl.png	6	2022-02-21 14:13:42	2022-02-21 14:13:42
123	MrRobot 201	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-123-1645424041IZsTZX.png	6	2022-02-21 14:14:01	2022-02-21 14:14:02
124	MrRobot 202	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-124-1645424066c7L59g.png	6	2022-02-21 14:14:26	2022-02-21 14:14:26
125	MrRobot 204	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-125-1645424093Aia5EP.png	6	2022-02-21 14:14:53	2022-02-21 14:14:54
126	MrRobot 205	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-126-1645424111vZtYLG.png	6	2022-02-21 14:15:11	2022-02-21 14:15:12
127	MrRobot 206	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-127-1645424139H0wh2U.png	6	2022-02-21 14:15:39	2022-02-21 14:15:39
128	MrRobot 207	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-128-1645424172UgU6z3.png	6	2022-02-21 14:16:12	2022-02-21 14:16:13
129	MrRobot 208	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-129-1645424204yJPYXq.png	6	2022-02-21 14:16:44	2022-02-21 14:16:45
130	MrRobot 209	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-130-16454242318TWa17.png	6	2022-02-21 14:17:11	2022-02-21 14:17:12
131	MrRobot 210	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-131-164542425724ELyC.png	6	2022-02-21 14:17:37	2022-02-21 14:17:37
132	MrRobot 211	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-132-1645424299skrnKh.png	6	2022-02-21 14:18:19	2022-02-21 14:18:20
133	MrRobot 212	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-133-1645424327FaZvrl.png	6	2022-02-21 14:18:47	2022-02-21 14:18:47
134	MrRobot 213	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-134-1645424364TuVwXe.png	6	2022-02-21 14:19:24	2022-02-21 14:19:24
135	MrRobot 214	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-135-1645424385D3k2LC.png	6	2022-02-21 14:19:45	2022-02-21 14:19:46
136	MrRobot 215	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-136-16454244140S8OPI.png	6	2022-02-21 14:20:14	2022-02-21 14:20:15
137	MrRobot 216	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-137-1645424437zDm8Ei.png	6	2022-02-21 14:20:37	2022-02-21 14:20:38
138	MrRobot 217	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-138-1645424476IVLtGx.png	6	2022-02-21 14:21:16	2022-02-21 14:21:16
139	MrRobot 218	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-139-1645424504r1ri4j.png	6	2022-02-21 14:21:44	2022-02-21 14:21:45
140	MrRobot 219	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-140-1645424538nAGtUM.png	6	2022-02-21 14:22:18	2022-02-21 14:22:18
141	MrRobot 220	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-141-1645424568DzipAv.png	6	2022-02-21 14:22:48	2022-02-21 14:22:49
142	MrRobot 221	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-142-1645426713JFFGeA.png	6	2022-02-21 14:58:33	2022-02-21 14:58:34
143	MrRobot 223	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-143-1645426821FAyFHj.png	6	2022-02-21 15:00:21	2022-02-21 15:00:21
146	MrRobot 226	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-146-1645427147CdtE36.png	6	2022-02-21 15:05:47	2022-02-21 15:05:48
145	MrRobot 225	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-145-1645426871utWY5m.png	6	2022-02-21 15:01:11	2022-02-21 15:01:12
144	MrRobot 224	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-144-1645426922bCWE7V.png	6	2022-02-21 15:00:47	2022-02-21 15:02:02
119	MrRobot 196	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-119-1645423945qtFP3p.png	6	2022-02-21 14:12:25	2022-02-21 15:02:21
147	MrRobot 227	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-147-1645427171G2NAQs.png	6	2022-02-21 15:06:11	2022-02-21 15:06:11
148	MrRobot 228	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-148-1645427190O6y2Jk.png	6	2022-02-21 15:06:30	2022-02-21 15:06:31
149	MrRobot 229	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-149-1645427218XFP3rH.png	6	2022-02-21 15:06:58	2022-02-21 15:06:58
150	MrRobot 230	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-150-1645427242ACu1ZE.png	6	2022-02-21 15:07:22	2022-02-21 15:07:23
151	MrRobot 231	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-151-16454272704hSsZN.png	6	2022-02-21 15:07:50	2022-02-21 15:07:51
152	MrRobot 232	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-152-1645427301CtFDwg.png	6	2022-02-21 15:08:21	2022-02-21 15:08:22
153	MrRobot 233	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-153-1645427381ypXSyj.png	6	2022-02-21 15:09:41	2022-02-21 15:09:41
154	MrRobot 234	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-154-1645427401Sz5I7n.png	6	2022-02-21 15:10:01	2022-02-21 15:10:02
155	MrRobot 235	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-155-1645427432PWWlvJ.png	6	2022-02-21 15:10:32	2022-02-21 15:10:32
156	MrRobot 236	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-156-1645427451NZlWbE.png	6	2022-02-21 15:10:51	2022-02-21 15:10:52
157	MrRobot 237	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-157-1645427490rc0OyZ.png	6	2022-02-21 15:11:30	2022-02-21 15:11:31
158	MrRobot 238	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-158-1645427561yyIJW3.png	6	2022-02-21 15:12:41	2022-02-21 15:12:41
159	MrRobot 239	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-159-1645427854h3oJGe.png	6	2022-02-21 15:17:34	2022-02-21 15:17:35
160	MrRobot 240	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-160-1645427874MQYdjQ.png	6	2022-02-21 15:17:54	2022-02-21 15:17:55
161	MrRobot 241	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-161-1645427904hLLZf6.png	6	2022-02-21 15:18:24	2022-02-21 15:18:25
162	MrRobot 242	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-162-1645427991ZcfkBH.png	6	2022-02-21 15:19:51	2022-02-21 15:19:52
163	MrRobot 243	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-163-1645428033GMMb4f.png	6	2022-02-21 15:20:33	2022-02-21 15:20:34
164	MrRobot 244	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-164-1645428065wLj6AM.png	6	2022-02-21 15:21:05	2022-02-21 15:21:06
165	MrRobot 245	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-165-1645428106yb3g9v.png	6	2022-02-21 15:21:46	2022-02-21 15:21:46
166	MrRobot 246	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-166-1645428149S05Fmr.png	6	2022-02-21 15:22:29	2022-02-21 15:22:30
167	MrRobot 248	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-167-1645428178SJB0e5.png	6	2022-02-21 15:22:58	2022-02-21 15:22:59
168	MrRobot 249	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-168-1645428198rlVHDZ.png	6	2022-02-21 15:23:18	2022-02-21 15:23:18
169	MrRobot 250	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-169-1645428386R2xlYx.png	6	2022-02-21 15:26:26	2022-02-21 15:26:26
170	MrRobot 251	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-170-1645428437FM7Vpw.png	6	2022-02-21 15:27:17	2022-02-21 15:27:17
171	MrRobot 252	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-171-1645428530m16On9.png	6	2022-02-21 15:28:50	2022-02-21 15:28:50
172	MrRobot 253	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-172-16454285522GPVGi.png	6	2022-02-21 15:29:12	2022-02-21 15:29:13
173	MrRobot 254	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-173-1645428579HZMOC4.png	6	2022-02-21 15:29:39	2022-02-21 15:29:40
174	MrRobot 255	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-174-1645428617YvHtvW.png	6	2022-02-21 15:30:17	2022-02-21 15:30:17
175	MrRobot 256	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-175-1645428838cHrquw.png	6	2022-02-21 15:33:58	2022-02-21 15:33:58
176	MrRobot 257	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-176-1645428865xky3nr.png	6	2022-02-21 15:34:25	2022-02-21 15:34:26
177	MrRobot 258	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-177-1645428899m6jOOg.png	6	2022-02-21 15:34:59	2022-02-21 15:35:00
178	MrRobot 259	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-178-1645428936YnEfam.png	6	2022-02-21 15:35:36	2022-02-21 15:35:37
179	MrRobot 260	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-179-16454289573f9MMd.png	6	2022-02-21 15:35:57	2022-02-21 15:35:57
180	MrRobot 261	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-180-1645428991zIibOQ.png	6	2022-02-21 15:36:31	2022-02-21 15:36:31
181	MrRobot 262	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-181-1645429038Uy2fny.png	6	2022-02-21 15:37:18	2022-02-21 15:37:19
182	MrRobot 263	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-182-1645429087gV3Mx5.png	6	2022-02-21 15:38:07	2022-02-21 15:38:07
183	MrRobot 264	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-183-1645429134UINGsF.png	6	2022-02-21 15:38:54	2022-02-21 15:38:55
184	MrRobot 265	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-184-1645429166TXWXQf.png	6	2022-02-21 15:39:26	2022-02-21 15:40:47
185	MrRobot 266	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-185-1645429268862Vd5.png	6	2022-02-21 15:41:08	2022-02-21 15:41:09
186	MrRobot 267	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-186-1645429291qhYvPl.png	6	2022-02-21 15:41:31	2022-02-21 15:41:32
187	MrRobot 268	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-187-1645429318z0WYu2.png	6	2022-02-21 15:41:58	2022-02-21 15:41:58
188	MrRobot 269	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-188-1645429341yOt7UO.png	6	2022-02-21 15:42:21	2022-02-21 15:42:22
189	MrRobot 270	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-189-1645429366XGfNh1.png	6	2022-02-21 15:42:46	2022-02-21 15:42:46
190	MrRobot 271	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-190-1645429398RpwJ1o.png	6	2022-02-21 15:43:18	2022-02-21 15:43:19
191	MrRobot 272	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-191-1645429428zMRaYK.png	6	2022-02-21 15:43:48	2022-02-21 15:43:49
192	MrRobot 273	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-192-16454294663BTd3k.png	6	2022-02-21 15:44:26	2022-02-21 15:44:27
193	MrRobot 274	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-193-1645434119qEXxSG.png	6	2022-02-21 17:01:59	2022-02-21 17:02:00
194	MrRobot 275	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-194-1645434141AZba7T.png	6	2022-02-21 17:02:21	2022-02-21 17:02:22
195	MrRobot 276	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-195-1645434170YXQYxi.png	6	2022-02-21 17:02:50	2022-02-21 17:02:51
196	MrRobot 277	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-196-1645434201oBIWMT.png	6	2022-02-21 17:03:21	2022-02-21 17:03:21
197	MrRobot 278	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-197-1645434227IyMmJ1.png	6	2022-02-21 17:03:47	2022-02-21 17:03:48
198	MrRobot 279	\N	0	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-198-1645434250bKpz8H.png	6	2022-02-21 17:04:10	2022-02-21 17:04:11
199	MrRobot 280	\N	0	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-199-1645434269svMq3R.png	6	2022-02-21 17:04:29	2022-02-21 17:04:30
200	MrRobot 281	\N	0	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-200-1645434295mZkjho.png	6	2022-02-21 17:04:55	2022-02-21 17:04:56
201	MrRobot 282	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-201-1645434314mg4Abm.png	6	2022-02-21 17:05:14	2022-02-21 17:05:15
202	MrRobot 283	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-202-1645434340N9BiTG.png	6	2022-02-21 17:05:40	2022-02-21 17:05:40
204	MrRobot 285	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-204-1645434395ELoGIb.png	6	2022-02-21 17:06:35	2022-02-21 17:06:36
203	MrRobot 284	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-203-1645434369sXYovD.png	6	2022-02-21 17:06:09	2022-02-21 17:06:52
92	MrRobot 169	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-92-1645423123ydU4pI.png	6	2022-02-21 13:58:43	2022-02-21 17:07:09
120	MrRobot 197	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-120-1645423968h7k7nr.png	6	2022-02-21 14:12:48	2022-02-21 17:07:22
205	MrRobot 286	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-205-16454345095NrSpu.png	6	2022-02-21 17:08:29	2022-02-21 17:08:30
206	MrRobot 287	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-206-1645434532n1iHgy.png	6	2022-02-21 17:08:52	2022-02-21 17:08:52
207	MrRobot 288	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-207-1645434558C9SIa9.png	6	2022-02-21 17:09:18	2022-02-21 17:09:19
208	MrRobot 289	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-208-1645434585oDLNF7.png	6	2022-02-21 17:09:45	2022-02-21 17:09:46
209	MrRobot 290	\N	0.04	BNB	\N	\N	images/robostudio/nft_collections/tuah-io-nft-img-209-1645434617lNkHya.png	6	2022-02-21 17:10:17	2022-02-21 17:10:17
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.permission_role (role_id, permission_id) FROM stdin;
1	1
1	2
1	3
1	4
2	2
2	3
2	4
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.permissions (id, title, created_at, updated_at, deleted_at) FROM stdin;
1	admin_access	\N	\N	\N
2	user_basic_access	\N	\N	\N
3	user_pro_access	\N	\N	\N
4	user_adv_access	\N	\N	\N
\.


--
-- Data for Name: personal_access_tokens; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: redemption_gift_details; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.redemption_gift_details (id, redemption_id, nft_collection_id, nft_collection_group_id, created_at, updated_at) FROM stdin;
1	1	12	5	2022-02-09 02:15:47	2022-02-09 02:15:47
2	2	13	5	2022-02-09 17:37:09	2022-02-09 17:37:09
3	3	21	6	2022-02-21 00:12:50	2022-02-21 00:12:50
4	4	203	7	2022-02-21 18:41:59	2022-02-21 18:41:59
5	5	40	7	2022-02-21 18:45:53	2022-02-21 18:45:53
6	6	125	7	2022-02-21 18:45:57	2022-02-21 18:45:57
7	7	104	7	2022-02-21 19:13:53	2022-02-21 19:13:53
8	8	36	7	2022-02-21 19:14:21	2022-02-21 19:14:21
9	9	135	7	2022-02-21 19:14:35	2022-02-21 19:14:35
10	10	77	7	2022-02-21 19:15:43	2022-02-21 19:15:43
11	11	142	7	2022-02-21 19:15:44	2022-02-21 19:15:44
12	12	151	7	2022-02-21 19:17:30	2022-02-21 19:17:30
13	13	160	7	2022-02-21 19:19:09	2022-02-21 19:19:09
14	14	205	7	2022-02-21 19:20:11	2022-02-21 19:20:11
15	15	116	7	2022-02-21 19:22:31	2022-02-21 19:22:31
16	16	165	7	2022-02-21 19:23:03	2022-02-21 19:23:03
17	17	88	7	2022-02-21 19:23:53	2022-02-21 19:23:53
18	18	119	7	2022-02-21 19:24:26	2022-02-21 19:24:26
19	19	208	7	2022-02-21 19:32:19	2022-02-21 19:32:19
20	20	156	7	2022-02-21 19:36:08	2022-02-21 19:36:08
21	21	110	7	2022-02-21 19:38:34	2022-02-21 19:38:34
22	22	189	7	2022-02-21 19:43:42	2022-02-21 19:43:42
23	23	67	7	2022-02-21 19:48:03	2022-02-21 19:48:03
24	24	111	7	2022-02-21 19:51:07	2022-02-21 19:51:07
25	25	108	7	2022-02-21 19:58:02	2022-02-21 19:58:02
26	26	184	7	2022-02-21 19:58:03	2022-02-21 19:58:03
27	27	138	7	2022-02-21 19:59:14	2022-02-21 19:59:14
28	28	149	7	2022-02-21 20:00:36	2022-02-21 20:00:36
29	29	97	7	2022-02-21 20:03:52	2022-02-21 20:03:52
30	30	62	7	2022-02-21 20:04:49	2022-02-21 20:04:49
31	31	58	7	2022-02-21 20:05:12	2022-02-21 20:05:12
32	32	75	7	2022-02-21 20:08:28	2022-02-21 20:08:28
33	33	74	7	2022-02-21 20:16:52	2022-02-21 20:16:52
34	34	63	7	2022-02-21 20:21:34	2022-02-21 20:21:34
35	35	172	7	2022-02-21 20:24:32	2022-02-21 20:24:32
36	36	194	7	2022-02-21 20:26:43	2022-02-21 20:26:43
37	37	136	7	2022-02-21 20:27:49	2022-02-21 20:27:49
38	38	87	7	2022-02-21 20:28:37	2022-02-21 20:28:37
39	39	175	7	2022-02-21 20:31:04	2022-02-21 20:31:04
40	40	25	7	2022-02-21 20:35:10	2022-02-21 20:35:10
41	41	44	7	2022-02-21 20:39:03	2022-02-21 20:39:03
42	42	34	7	2022-02-21 20:40:21	2022-02-21 20:40:21
43	43	89	7	2022-02-21 20:41:38	2022-02-21 20:41:38
44	44	76	7	2022-02-21 20:44:30	2022-02-21 20:44:30
45	45	202	7	2022-02-21 20:45:06	2022-02-21 20:45:06
46	46	143	7	2022-02-21 21:09:12	2022-02-21 21:09:12
47	47	134	7	2022-02-21 21:20:09	2022-02-21 21:20:09
48	48	183	7	2022-02-21 21:35:39	2022-02-21 21:35:39
49	49	51	7	2022-02-21 21:39:15	2022-02-21 21:39:15
50	50	102	7	2022-02-21 21:44:36	2022-02-21 21:44:36
51	51	190	7	2022-02-21 21:46:12	2022-02-21 21:46:12
52	52	79	7	2022-02-21 21:46:41	2022-02-21 21:46:41
53	53	59	7	2022-02-21 22:50:18	2022-02-21 22:50:18
54	54	31	7	2022-02-21 23:03:45	2022-02-21 23:03:45
55	55	133	7	2022-02-21 23:25:51	2022-02-21 23:25:51
56	56	127	7	2022-02-22 13:47:25	2022-02-22 13:47:25
57	57	39	7	2022-02-22 13:48:12	2022-02-22 13:48:12
58	58	177	7	2022-02-22 13:56:17	2022-02-22 13:56:17
59	59	99	7	2022-02-22 13:58:19	2022-02-22 13:58:19
60	60	201	7	2022-02-22 14:06:52	2022-02-22 14:06:52
61	61	188	7	2022-02-22 14:32:43	2022-02-22 14:32:43
62	62	33	7	2022-02-22 14:50:33	2022-02-22 14:50:33
63	63	130	7	2022-02-22 15:01:22	2022-02-22 15:01:22
64	64	181	7	2022-02-22 15:20:07	2022-02-22 15:20:07
65	65	96	7	2022-02-22 15:25:49	2022-02-22 15:25:49
\.


--
-- Data for Name: redemption_status_detail; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.redemption_status_detail (id, redemption_id, message, status, is_current, created_at, updated_at) FROM stdin;
1	1	User successfully redeemed the reward	processing	t	2022-02-09 02:15:47	2022-02-09 02:15:47
2	1	Here is your shipping number #10221023913i9	completed	t	2022-02-09 02:17:44	2022-02-09 02:17:44
3	2	User successfully redeemed the reward	processing	t	2022-02-09 17:37:09	2022-02-09 17:37:09
4	2	Thank you for your redemption!	completed	t	2022-02-10 22:41:18	2022-02-10 22:41:18
5	3	User successfully redeemed the reward	processing	t	2022-02-21 00:12:50	2022-02-21 00:12:50
6	4	User successfully redeemed the reward	processing	t	2022-02-21 18:41:59	2022-02-21 18:41:59
7	5	User successfully redeemed the reward	processing	t	2022-02-21 18:45:53	2022-02-21 18:45:53
8	6	User successfully redeemed the reward	processing	t	2022-02-21 18:45:57	2022-02-21 18:45:57
9	7	User successfully redeemed the reward	processing	t	2022-02-21 19:13:53	2022-02-21 19:13:53
10	8	User successfully redeemed the reward	processing	t	2022-02-21 19:14:21	2022-02-21 19:14:21
11	9	User successfully redeemed the reward	processing	t	2022-02-21 19:14:35	2022-02-21 19:14:35
12	10	User successfully redeemed the reward	processing	t	2022-02-21 19:15:43	2022-02-21 19:15:43
13	11	User successfully redeemed the reward	processing	t	2022-02-21 19:15:44	2022-02-21 19:15:44
14	12	User successfully redeemed the reward	processing	t	2022-02-21 19:17:30	2022-02-21 19:17:30
15	13	User successfully redeemed the reward	processing	t	2022-02-21 19:19:09	2022-02-21 19:19:09
16	14	User successfully redeemed the reward	processing	t	2022-02-21 19:20:11	2022-02-21 19:20:11
17	15	User successfully redeemed the reward	processing	t	2022-02-21 19:22:31	2022-02-21 19:22:31
18	16	User successfully redeemed the reward	processing	t	2022-02-21 19:23:03	2022-02-21 19:23:03
19	17	User successfully redeemed the reward	processing	t	2022-02-21 19:23:53	2022-02-21 19:23:53
20	18	User successfully redeemed the reward	processing	t	2022-02-21 19:24:26	2022-02-21 19:24:26
21	19	User successfully redeemed the reward	processing	t	2022-02-21 19:32:19	2022-02-21 19:32:19
22	20	User successfully redeemed the reward	processing	t	2022-02-21 19:36:08	2022-02-21 19:36:08
23	21	User successfully redeemed the reward	processing	t	2022-02-21 19:38:34	2022-02-21 19:38:34
24	22	User successfully redeemed the reward	processing	t	2022-02-21 19:43:42	2022-02-21 19:43:42
25	23	User successfully redeemed the reward	processing	t	2022-02-21 19:48:03	2022-02-21 19:48:03
26	24	User successfully redeemed the reward	processing	t	2022-02-21 19:51:07	2022-02-21 19:51:07
27	25	User successfully redeemed the reward	processing	t	2022-02-21 19:58:02	2022-02-21 19:58:02
28	26	User successfully redeemed the reward	processing	t	2022-02-21 19:58:03	2022-02-21 19:58:03
29	27	User successfully redeemed the reward	processing	t	2022-02-21 19:59:14	2022-02-21 19:59:14
30	28	User successfully redeemed the reward	processing	t	2022-02-21 20:00:36	2022-02-21 20:00:36
31	29	User successfully redeemed the reward	processing	t	2022-02-21 20:03:52	2022-02-21 20:03:52
32	30	User successfully redeemed the reward	processing	t	2022-02-21 20:04:49	2022-02-21 20:04:49
33	31	User successfully redeemed the reward	processing	t	2022-02-21 20:05:12	2022-02-21 20:05:12
34	32	User successfully redeemed the reward	processing	t	2022-02-21 20:08:28	2022-02-21 20:08:28
35	33	User successfully redeemed the reward	processing	t	2022-02-21 20:16:52	2022-02-21 20:16:52
36	34	User successfully redeemed the reward	processing	t	2022-02-21 20:21:34	2022-02-21 20:21:34
37	35	User successfully redeemed the reward	processing	t	2022-02-21 20:24:32	2022-02-21 20:24:32
38	36	User successfully redeemed the reward	processing	t	2022-02-21 20:26:43	2022-02-21 20:26:43
39	37	User successfully redeemed the reward	processing	t	2022-02-21 20:27:49	2022-02-21 20:27:49
40	38	User successfully redeemed the reward	processing	t	2022-02-21 20:28:37	2022-02-21 20:28:37
41	39	User successfully redeemed the reward	processing	t	2022-02-21 20:31:04	2022-02-21 20:31:04
42	40	User successfully redeemed the reward	processing	t	2022-02-21 20:35:10	2022-02-21 20:35:10
43	41	User successfully redeemed the reward	processing	t	2022-02-21 20:39:03	2022-02-21 20:39:03
44	42	User successfully redeemed the reward	processing	t	2022-02-21 20:40:21	2022-02-21 20:40:21
45	43	User successfully redeemed the reward	processing	t	2022-02-21 20:41:38	2022-02-21 20:41:38
46	44	User successfully redeemed the reward	processing	t	2022-02-21 20:44:30	2022-02-21 20:44:30
47	45	User successfully redeemed the reward	processing	t	2022-02-21 20:45:06	2022-02-21 20:45:06
48	46	User successfully redeemed the reward	processing	t	2022-02-21 21:09:12	2022-02-21 21:09:12
49	47	User successfully redeemed the reward	processing	t	2022-02-21 21:20:09	2022-02-21 21:20:09
50	48	User successfully redeemed the reward	processing	t	2022-02-21 21:35:39	2022-02-21 21:35:39
51	49	User successfully redeemed the reward	processing	t	2022-02-21 21:39:15	2022-02-21 21:39:15
52	50	User successfully redeemed the reward	processing	t	2022-02-21 21:44:36	2022-02-21 21:44:36
53	51	User successfully redeemed the reward	processing	t	2022-02-21 21:46:12	2022-02-21 21:46:12
54	52	User successfully redeemed the reward	processing	t	2022-02-21 21:46:41	2022-02-21 21:46:41
55	53	User successfully redeemed the reward	processing	t	2022-02-21 22:50:18	2022-02-21 22:50:18
56	54	User successfully redeemed the reward	processing	t	2022-02-21 23:03:45	2022-02-21 23:03:45
57	55	User successfully redeemed the reward	processing	t	2022-02-21 23:25:51	2022-02-21 23:25:51
58	56	User successfully redeemed the reward	processing	t	2022-02-22 13:47:25	2022-02-22 13:47:25
59	57	User successfully redeemed the reward	processing	t	2022-02-22 13:48:12	2022-02-22 13:48:12
60	58	User successfully redeemed the reward	processing	t	2022-02-22 13:56:17	2022-02-22 13:56:17
61	59	User successfully redeemed the reward	processing	t	2022-02-22 13:58:19	2022-02-22 13:58:19
62	60	User successfully redeemed the reward	processing	t	2022-02-22 14:06:52	2022-02-22 14:06:52
63	61	User successfully redeemed the reward	processing	t	2022-02-22 14:32:43	2022-02-22 14:32:43
64	62	User successfully redeemed the reward	processing	t	2022-02-22 14:50:33	2022-02-22 14:50:33
65	63	User successfully redeemed the reward	processing	t	2022-02-22 15:01:22	2022-02-22 15:01:22
66	64	User successfully redeemed the reward	processing	t	2022-02-22 15:20:07	2022-02-22 15:20:07
67	65	User successfully redeemed the reward	processing	t	2022-02-22 15:25:49	2022-02-22 15:25:49
68	4	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:18:06	2022-02-22 16:18:06
69	5	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:18:55	2022-02-22 16:18:55
70	6	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:22:12	2022-02-22 16:22:12
71	7	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:25:09	2022-02-22 16:25:09
72	8	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:25:11	2022-02-22 16:25:11
73	9	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:28:30	2022-02-22 16:28:30
74	10	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:28:33	2022-02-22 16:28:33
75	11	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:28:38	2022-02-22 16:28:38
76	12	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:36:45	2022-02-22 16:36:45
77	13	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:43:28	2022-02-22 16:43:28
78	14	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:43:54	2022-02-22 16:43:54
79	15	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:43:59	2022-02-22 16:43:59
80	16	Your robot is in your wallet now serr.	completed	t	2022-02-22 16:44:32	2022-02-22 16:44:32
81	17	Your robot is in your wallet now serr.	completed	t	2022-02-22 17:04:14	2022-02-22 17:04:14
82	18	Your robot is in your wallet now serr.	completed	t	2022-02-22 17:04:16	2022-02-22 17:04:16
83	19	Your robot is in your wallet now serr.	completed	t	2022-02-22 17:04:19	2022-02-22 17:04:19
84	20	Your robot is in your wallet now serr.	completed	t	2022-02-22 17:04:23	2022-02-22 17:04:23
85	21	Your robot is in your wallet now serr.	completed	t	2022-02-22 17:04:28	2022-02-22 17:04:28
86	58	Your robot is in your wallet now serr.	completed	t	2022-02-22 17:29:39	2022-02-22 17:29:39
87	65	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:15	2022-02-22 18:23:15
88	64	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:17	2022-02-22 18:23:17
89	63	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:21	2022-02-22 18:23:21
90	62	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:24	2022-02-22 18:23:24
91	61	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:27	2022-02-22 18:23:27
92	60	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:30	2022-02-22 18:23:30
93	22	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:42	2022-02-22 18:23:42
94	23	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:45	2022-02-22 18:23:45
95	24	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:49	2022-02-22 18:23:49
96	25	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:52	2022-02-22 18:23:52
97	26	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:55	2022-02-22 18:23:55
98	27	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:23:59	2022-02-22 18:23:59
99	28	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:02	2022-02-22 18:24:02
100	29	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:05	2022-02-22 18:24:05
101	30	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:09	2022-02-22 18:24:09
102	31	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:12	2022-02-22 18:24:12
103	32	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:15	2022-02-22 18:24:15
104	33	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:19	2022-02-22 18:24:19
105	34	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:22	2022-02-22 18:24:22
106	35	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:24	2022-02-22 18:24:24
107	36	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:28	2022-02-22 18:24:28
108	37	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:31	2022-02-22 18:24:31
109	38	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:34	2022-02-22 18:24:34
110	39	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:38	2022-02-22 18:24:38
111	59	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:56	2022-02-22 18:24:56
112	57	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:24:59	2022-02-22 18:24:59
113	56	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:02	2022-02-22 18:25:02
114	55	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:06	2022-02-22 18:25:06
115	54	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:09	2022-02-22 18:25:09
116	53	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:12	2022-02-22 18:25:12
117	51	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:15	2022-02-22 18:25:15
118	50	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:19	2022-02-22 18:25:19
119	49	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:22	2022-02-22 18:25:22
120	48	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:25	2022-02-22 18:25:25
121	47	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:28	2022-02-22 18:25:28
122	46	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:32	2022-02-22 18:25:32
123	45	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:34	2022-02-22 18:25:34
124	44	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:37	2022-02-22 18:25:37
125	43	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:40	2022-02-22 18:25:40
126	42	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:43	2022-02-22 18:25:43
127	40	Your robot is in your wallet now serr.	completed	t	2022-02-22 18:25:46	2022-02-22 18:25:46
\.


--
-- Data for Name: redemptions; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.redemptions (id, name, email, twitter_username, code, status, eth_address, giveaway_id, campaign_id, gift_id, created_at, updated_at) FROM stdin;
1	Holo NFT	myholonft@gmail.com	\N	pLyYlV	\N	0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0	10	\N	10	2022-02-09 02:15:47	2022-02-09 02:15:47
2	Nando	robo.studio2021@gmail.com	\N	0EVAhK	\N	0x61dEEafdDEf6cAB550e45E075d0A7283Ecdbd378	10	\N	10	2022-02-09 17:37:09	2022-02-09 17:37:09
3	Holo NFT	myholonft@gmail.com	\N	fFGCym	\N	0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0	11	\N	11	2022-02-21 00:12:50	2022-02-21 00:12:50
4	folsano	tunmatfan@gmail.com	\N	W55zdw	\N	0x2050621857880c745feff1127B30c2336a2Dca1a	12	\N	13	2022-02-21 18:41:59	2022-02-21 18:41:59
5	Azman Nurhakim	azman260500@gmail.com	\N	zlDK93	\N	0x44d33C835b7a20EeC91f451E62222b62f9481450	12	\N	13	2022-02-21 18:45:53	2022-02-21 18:45:53
6	limau	limauarts@gmail.com	\N	kYnNox	\N	0xF153F68357681Dec5aC068E26ce18387585870B3	12	\N	13	2022-02-21 18:45:57	2022-02-21 18:45:57
7	KriptoKidd	kriptokidd.my@gmail.com	\N	3nlCCG	\N	0x69FE55b13Dc66FE6c19e88525Bf890740fb28600	12	\N	13	2022-02-21 19:13:53	2022-02-21 19:13:53
8	Wildan Rizqullah	wrknizam@gmail.com	\N	G1vNEc	\N	0x44c264F47A5ee082CB99C23d9036484f573dCe4a	12	\N	13	2022-02-21 19:14:21	2022-02-21 19:14:21
9	Muhammad Najmuddin	riotsquadeh@gmail.com	\N	glI18x	\N	0xabC32FA26c041852f5B6E1BAb81558F6d68f31ba	12	\N	13	2022-02-21 19:14:35	2022-02-21 19:14:35
10	kidd	kidd.kudi@outlook.com	\N	CpSUti	\N	0x18Fda583DB788DF6BB38cB44a8f45bd8Eb2E5749	12	\N	13	2022-02-21 19:15:43	2022-02-21 19:15:43
11	Octo	octosquadd@gmail.com	\N	SPjJoX	\N	0xb738946a4a97B86D0C3BEbc32c0632Ff0EeB4381	12	\N	13	2022-02-21 19:15:44	2022-02-21 19:15:44
12	Richie	richteo2@gmail.com	\N	HlscHJ	\N	0x3F274e69bc09eD3276cCf0C982a655d3941de546	12	\N	13	2022-02-21 19:17:30	2022-02-21 19:17:30
13	Abu Hafiz	abuhafiz99@gmail.com	\N	2LM36B	\N	0xAd540883342B74EfA65A2B8345f4416152f429dF	12	\N	13	2022-02-21 19:19:09	2022-02-21 19:19:09
14	Kudoukiddd	aminmzln20@gmail.com	\N	XtAKql	\N	0x0CC97AcF1B54D56e2A05d2055A14feD2a98A800B	12	\N	13	2022-02-21 19:20:11	2022-02-21 19:20:11
15	safsatu	safuw1moon@gmail.com	\N	katkGM	\N	0x828DE0799953E81f8D3D9f160bc5c4C8a5D40E4E	12	\N	13	2022-02-21 19:22:31	2022-02-21 19:22:31
16	glscld	echo_jittery_0p@icloud.com	\N	9kae5D	\N	0xa34254C9d79f03cE838e5005719f8e77cbDe444c	12	\N	13	2022-02-21 19:23:03	2022-02-21 19:23:03
17	Nevaeh of NFT	mthrfckr.nft@hotmail.com	\N	HEb29D	\N	0xfB55ba1be6aeD490Ba58bAD9517308eBcA8501B2	12	\N	13	2022-02-21 19:23:53	2022-02-21 19:23:53
18	Thir	atthirmizi10@gmail.com	\N	DcYEmS	\N	0x40D7DC757b1Ae760bFB372402Be622fF2A2D482F	12	\N	13	2022-02-21 19:24:26	2022-02-21 19:24:26
19	Potion Master	ammarshakirinzawawi@gmail.com	\N	LA8zSd	\N	0x0C41337a9CCd52895636D212fa0115A3BcFC8DD5	12	\N	13	2022-02-21 19:32:19	2022-02-21 19:32:19
20	Rimo	rimo.in.meta@gmail.com	\N	qZov5o	\N	0x9C17A813b40201b7bE5BfF5E964C7343559Ad3bF	12	\N	13	2022-02-21 19:36:08	2022-02-21 19:36:08
21	SCiO	syafiqamran94@gmail.com	\N	VHBncF	\N	0x253Dcb4e61F1E715fcfd478b9B9Ba9e01a448dcb	12	\N	13	2022-02-21 19:38:34	2022-02-21 19:38:34
22	faliqfahmie	faliqfahmi@gmail.com	\N	3NX4Jo	\N	0x2c2882399cB958BbB682C8a0976B590B451A32bB	12	\N	13	2022-02-21 19:43:42	2022-02-21 19:43:42
23	Afric Guild	dakwah.indah11@gmail.com	\N	KMC6EK	\N	0xA26780eb85347B069351E403F7F8724F2331Ef38	12	\N	13	2022-02-21 19:48:03	2022-02-21 19:48:03
24	Hazlami Effendi	hazlami.work@gmail.com	\N	ht562O	\N	0xFe2E1D458a4EdaB962179DA3A7CDd8bFABf39B81	12	\N	13	2022-02-21 19:51:07	2022-02-21 19:51:07
25	Hazirul Haqim	hazirulz@gmail.com	\N	NidxBi	\N	0xa01dd358Cd194b3adFd7a2A8477bB7f25C69374a	12	\N	13	2022-02-21 19:58:02	2022-02-21 19:58:02
26	Doodle Boy	doodleboymy@gmail.com	\N	zDseXe	\N	0xacd9Af3C539331d667A5b45aFbc32F82d42858E5	12	\N	13	2022-02-21 19:58:03	2022-02-21 19:58:03
27	Pimo	faheem.olorin@gmail.com	\N	cLXyCD	\N	0x2f56f0435274C12A19016F86329028D295994abD	12	\N	13	2022-02-21 19:59:14	2022-02-21 19:59:14
28	Doodle Boy Kotak	doodleboymy@gmail.com	\N	uXFr53	\N	0x17D8bcb904f7f9d45DC2974C796D20Ed73332fEe	12	\N	13	2022-02-21 20:00:36	2022-02-21 20:00:36
29	Huxsterized	huxsterized@gmail.com	\N	gGIoZ5	\N	0xf186c01A6b429eca496ed0bAa46c61170f5fA9C8	12	\N	13	2022-02-21 20:03:52	2022-02-21 20:03:52
30	Danny Ding	dannydinganakjimmy@gmail.com	\N	SgQIwd	\N	0xd3a037A5D53A972baF17A3bb28fA317dB1c18014	12	\N	13	2022-02-21 20:04:49	2022-02-21 20:04:49
31	Rongiebyul	rongiebyul@gmail.com	\N	Nrdz3w	\N	0xEa31A2C5F42D7E7c815c2c2222E6E97A25818E91	12	\N	13	2022-02-21 20:05:12	2022-02-21 20:05:12
32	GuitaPlec	guitaplec@gmail.com	\N	MDX6CU	\N	0xA318B977Ec8E4b04e9715327a1F9db70A0680Dd1	12	\N	13	2022-02-21 20:08:28	2022-02-21 20:08:28
33	Ahmad Sanuri Zulkefli	san_msia@yahoo.com	\N	HfQvOY	\N	0x950FEC7e16506E2f3738D95cC20C2b55E8A970F5	12	\N	13	2022-02-21 20:16:52	2022-02-21 20:16:52
34	Aizat Hamdi	aizathamdiyas@gmail.com	\N	TkhmXx	\N	0xB880d7FE168042c431B20745E47ea6Dc7a7f071e	12	\N	13	2022-02-21 20:21:34	2022-02-21 20:21:34
35	Jimi Jambul	jimijambul.nft@gmail.com	\N	HjcDsg	\N	0x5D28b41D126928aB2205113d91e85453ECbAF218	12	\N	13	2022-02-21 20:24:32	2022-02-21 20:24:32
36	Sanuri Zulkefli	ee10pi3r@yahoo.com	\N	LH9MMd	\N	0x608e5393C1cd3E7ba0E287fC53ac1b16f358Abb4	12	\N	13	2022-02-21 20:26:43	2022-02-21 20:26:43
37	Sein	sein.xv@gmail.com	\N	csvnEV	\N	0xC019146EE69233Be53c96bA2778A5267ce7f7E8D	12	\N	13	2022-02-21 20:27:49	2022-02-21 20:27:49
38	san	sanuri.zulkefli@gmail.com	\N	dTuHKk	\N	0xc3d72590d7123fdF9A39922269360A668505aa78	12	\N	13	2022-02-21 20:28:37	2022-02-21 20:28:37
39	gumpunk	fakisha08@gmail.com	\N	aLMjM1	\N	0x00A57fF08Eb1D8171e5eA6FC94fE7D0d615d9700	12	\N	13	2022-02-21 20:31:04	2022-02-21 20:31:04
40	MeetFauzanHere	fznhkm.1992@gmail.com	\N	VvmyKg	\N	0x23ff048719fd3b0f33CbE1184178878B1781DEDa	12	\N	13	2022-02-21 20:35:10	2022-02-21 20:35:10
41	Pak Lang Choi	dannchoi.mdz@gmail.com	\N	d1QPTE	\N	0xF35fC7Feaa2e17253074C473f8C9a58BF5461C78	12	\N	13	2022-02-21 20:39:03	2022-02-21 20:39:03
42	Pak Lang Choi	dannchoi.mdz@gmail.com	\N	JAxIKP	\N	0x28cd01Ba1686574B4e0EF848862BC1e9CBFe4fFE	12	\N	13	2022-02-21 20:40:21	2022-02-21 20:40:21
43	Pak Lang Choi	dannchoi.mdz@gmail.com	\N	ENAiIJ	\N	0x9cdD9fDdC0e712858B3b42086F9d875a9707D472	12	\N	13	2022-02-21 20:41:38	2022-02-21 20:41:38
44	TunNft	katonaqhari@gmail.com	\N	vydbtW	\N	0x14b37280FcC04256c7fc03d5a7B1e8564eE28A8f	12	\N	13	2022-02-21 20:44:30	2022-02-21 20:44:30
45	TunNft	katonaqhari@gmail.com	\N	SOXEYU	\N	0x0fB46b8438B757663c91aE728E36deCce2C4d000	12	\N	13	2022-02-21 20:45:06	2022-02-21 20:45:06
46	Jentayu11	aqn.official@gmail.com	\N	ebrFe2	\N	0x0987df284C40FA7Ecc5fe01d16604b0Ad00F7902	12	\N	13	2022-02-21 21:09:12	2022-02-21 21:09:12
47	Irfan Zai	irfanzaini0807@gmail.com	\N	4aIp4c	\N	0xad39143e77468cD057Fbf37150840CCFCeE1F385	12	\N	13	2022-02-21 21:20:09	2022-02-21 21:20:09
48	Sepet Vernz	littlerabbito@gmail.com	\N	Qla8OE	\N	0x3D3B18cE0786bF6be9611C80f1D280554026A37f	12	\N	13	2022-02-21 21:35:39	2022-02-21 21:35:39
49	Hakimi Hamizi	hakimihamizi15@gmail.com	\N	NJrbAO	\N	0xF1D8F13Fd137669ac768EDf467f358200FafbA72	12	\N	13	2022-02-21 21:39:15	2022-02-21 21:39:15
50	Afif Nur	apipnoi95@gmail.com	\N	cODzDH	\N	0xfF7eB276A2F499501Cc4376125D9879C1Bc6bA3f	12	\N	13	2022-02-21 21:44:36	2022-02-21 21:44:36
51	Akmal Muhammad	m.akmalmhmmd@gmail.com	\N	wCm2tb	\N	0x26dF7FD3213Ddaac527C76B7C60a8fE16E90B2e5	12	\N	13	2022-02-21 21:46:12	2022-02-21 21:46:12
52	wankidd	wankidd@gmail.com	\N	40cWRP	\N	0x7B84e57B94f94D9DE4782bE5149B49Fe10fcc376	12	\N	13	2022-02-21 21:46:41	2022-02-21 21:46:41
53	Adri Alfred	adriartnna@gmail.com	\N	hjUx4y	\N	0x87694bDD4A633173de441D09fa3D9b65E1B9E34a	12	\N	13	2022-02-21 22:50:18	2022-02-21 22:50:18
54	siraj husam	sirajmuqaddas@gmail.com	\N	HGvdNb	\N	0x0e4eCf1aF7C603adF59e2B4953569d79a4087DE4	12	\N	13	2022-02-21 23:03:45	2022-02-21 23:03:45
55	Wan Arif	0ne4rif@gmail.com	\N	vtpU1j	\N	0x1a70fD5Fc8725E0d0Cf0D1b2E64a029DC1c7625D	12	\N	13	2022-02-21 23:25:51	2022-02-21 23:25:51
56	Aizat Azhar	m.aizatazhar21@gmail.com	\N	CA3SRk	\N	0xaD52182dc4fCbB6F91cF5b1e1D61e170f885513A	12	\N	13	2022-02-22 13:47:25	2022-02-22 13:47:25
57	xyz parrots	kidd.kudi@outlook.com	\N	S0c3tx	\N	0x021cb9DD7c0fa60eDe1b35DbccC52757C21E11EC	12	\N	13	2022-02-22 13:48:12	2022-02-22 13:48:12
58	ilham ishak aman	ilhamsahak@gmail.com	\N	6C9hpV	\N	0x818ad13e1f690A9F3edCF54Ee19E08AD661bfc1A	12	\N	13	2022-02-22 13:56:17	2022-02-22 13:56:17
59	Naqib Salleh	sallehnaqib22@gmail.com	\N	D4IOn7	\N	0x48C099D3046976650032320bd15cABd4dab2F6dD	12	\N	13	2022-02-22 13:58:19	2022-02-22 13:58:19
60	limau	limauarts@gmail.com	\N	bvrtjN	\N	0x4D4EF32E5330161471dA87F5dD972C1b2e838041	12	\N	13	2022-02-22 14:06:52	2022-02-22 14:06:52
61	Kembara Naga	kembaranaga.my@gmail.com	\N	MVxJkD	\N	0x94f25e4DC37459dEB42AE9a7E2Fb4235dE4A8D33	12	\N	13	2022-02-22 14:32:43	2022-02-22 14:32:43
62	Nymea	nymea.art@gmail.com	\N	wGTB0Y	\N	0xf5FF6CE038D5470FC1c592263C0964554E2C0262	12	\N	13	2022-02-22 14:50:33	2022-02-22 14:50:33
63	Jackt	azuhribusiness@gmail.com	\N	5Q5ax4	\N	0x3a39Eeae464344D2999132BDdaCF415112525B28	12	\N	13	2022-02-22 15:01:22	2022-02-22 15:01:22
64	Zakwan Jaafar	zakwanjaafarr@gmail.com	\N	z7A78j	\N	0xbb83322dEcB10c79C18AE1945c997Db333C37f85	12	\N	13	2022-02-22 15:20:07	2022-02-22 15:20:07
65	abbas	abbas2bizz@gmail.com	\N	eakza2	\N	0x57Bc42cB21A149e3Bcd3e085AD98Ec58C751177E	12	\N	13	2022-02-22 15:25:49	2022-02-22 15:25:49
\.


--
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.role_user (id, created_at, updated_at, role_id, user_id) FROM stdin;
1	\N	\N	2	1
2	\N	\N	2	2
3	\N	\N	2	3
4	\N	\N	2	4
5	\N	\N	2	5
6	\N	\N	2	6
7	\N	\N	2	7
8	\N	\N	2	8
9	\N	\N	2	9
10	\N	\N	2	10
11	\N	\N	2	11
12	\N	\N	2	12
13	\N	\N	2	13
14	\N	\N	2	14
15	\N	\N	2	15
16	\N	\N	2	16
17	\N	\N	2	17
18	\N	\N	2	18
19	\N	\N	2	19
20	\N	\N	2	20
21	\N	\N	2	21
22	\N	\N	2	22
23	\N	\N	2	23
24	\N	\N	2	24
25	\N	\N	2	25
26	\N	\N	2	26
27	\N	\N	2	27
28	\N	\N	2	28
29	\N	\N	2	29
30	\N	\N	2	30
31	\N	\N	2	31
32	\N	\N	2	32
33	\N	\N	2	33
34	\N	\N	2	34
35	\N	\N	2	35
36	\N	\N	2	36
37	\N	\N	2	37
38	\N	\N	2	38
39	\N	\N	2	39
40	\N	\N	2	40
41	\N	\N	2	41
42	\N	\N	2	42
43	\N	\N	2	43
44	\N	\N	2	44
45	\N	\N	2	45
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.roles (id, created_at, updated_at, title) FROM stdin;
1	\N	\N	Admin
2	\N	\N	User
\.


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.sessions (id, user_id, ip_address, user_agent, payload, last_activity) FROM stdin;
FY9Ykq1RdspGQjui8QL1PDpIHV1vuc7WQDP8idzR	\N	178.128.232.42	Go-http-client/1.1	YTozOntzOjY6Il90b2tlbiI7czo0MDoiemRNWHF3d3BxaEtjaGpzbVU2azVIU2xrejZlNUJ1QUFrdjJqT0QxSCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTY6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYvP3Jlc3Rfcm91dGU9JTJGd3AlMkZ2MiUyRnVzZXJzJTJGIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==	1653604504
o8ZE8VKMzeFCIfLOylQgLADb2IA7nHa9Ni4cnxhl	\N	35.233.62.116	python-requests/2.27.1	YTozOntzOjY6Il90b2tlbiI7czo0MDoiQjdDeFF2Y2NQM0txVjRUZE9vT1JyRTRMUm1STUJSRkU1VWh5bGVwRCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653616354
ChAUnyNvkVbwgUhdtUJlWcqI4RvKoF7KaFddg6MQ	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoibkU5aW9KMFVMODl6WVY2V3VJQWpYdFN3a2wzeFMxbTl5eHc1bXIxYiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU4OiJodHRwczovLzE1OS4yMjMuMzMuMTA2L2luZGV4LnBocD9mdW5jdGlvbj1jYWxsX3VzZXJfZnVuY19hcnJheSZzPSUyRkluZGV4JTJGJTVDdGhpbmslNUNhcHAlMkZpbnZva2VmdW5jdGlvbiZ2YXJzJTVCMCU1RD1tZDUmdmFycyU1QjElNUQlNUIwJTVEPUhlbGxvVGhpbmtQSFAyMSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653624194
9194RlU1F1w1HpSxyxqn7sulKUMCtBBrQjjIO2uV	\N	45.141.157.180	Mozilla/5.0 (Linux; Android 11; M2003J15SC) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.58 Mobile Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiWmszdXNZZGc1dDZYNWN6WTlTN0xhNElia3JYV3VwNTFIcFdqS0ZoRSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653635101
5G3NecnYqpHJ7YOCQnh6T9j4TLDh6jy5PFJmTo8b	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiRTFOUElPeTJ6TnNWakpYVEl0cUM1SFBlRGdkdEkyVlhiaHdIM2J6dCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653643297
4cSwWwYezoAE1M6ZWyNeq03wvaM2PC14mcvtdSao	\N	193.46.254.155	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoibjRlNlFHdlNwd2gzdXprRTBwdkZyM0FkNkphemVOOEh3d3NGSWg5VSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vd3d3LndpYmFzZS5jb20iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653650471
NyzziZwy3eD9dhPAN5rxyq5EwrRteiG1u3GtnBHs	\N	45.33.65.249	curl/7.54.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiRmRPN2ZXUGdQRk1UdU8yNTE0ckRMVU96U3M2VlRESEx4WFliSVpxbCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653661686
QnCUBuy0AashwqsBnfIg5ORtwZs7J5wBW6s4k60b	\N	45.33.65.249	curl/7.54.0	YToyOntzOjY6Il90b2tlbiI7czo0MDoiSllROGJKNmkyYVIwY0t6RzlaZjJCYnpxU1NBRWZieFJlaHV6WDFSMiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==	1653661687
ket7aJ6Kn5WSBYQrxv9MC99t2ZNvorD1POycMKmz	\N	167.94.138.45		YTozOntzOjY6Il90b2tlbiI7czo0MDoiNzBNbG9YQXdTb1p4QVRRT2hMS1dNVEtjQVJEMXYxTFZ3bU1BbVVVVCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653672682
FOwagLN98EayXY8BONyqrDmRpvBpqOIOQAe2i2Sm	\N	192.133.77.16	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiNE9pNmdVMm82eVVFZlYyWTRKNGRZZmYySTlnOWQxSGxpenl3SjdleiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653678121
hoL3D7fIz4hQXn6Wrp1rR84bUXnNJiTpRfG8qyNh	\N	3.101.143.157	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiVkNESVVPUldlaTBsSmVNdGFBalFvQjZ4Mk9XcmZ2SGVzQWlmM01nZSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHBzOi8vdHVhaC5pbyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653687154
fM9CzD3Bddh8OvccBwqQUdLQU2h2zQGCHPBRCYEq	\N	51.158.108.61		YTozOntzOjY6Il90b2tlbiI7czo0MDoiSkxrbXJRcU1xQWNQd0l3a1NSa0p0eG5kMjVFTmtwdnhIU0JyMVBlaCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjQ6Imh0dHBzOi8vZXpuaC5lZHV2aXAubGl2ZSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653695363
r2o2Maf6Fts77ATV4KMQbEXJuG073lTIE4IvfpTz	\N	178.79.169.64	Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoia3lEdFBCZkRvYUhoTkZFbEVHbFZwWjFlRHZtakZMVXk0SXpMRjVGaCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653703637
dA7qhv06Rp6sLdnZtT4NffVCoJ1KEVbD98La46NH	\N	184.105.139.70		YTozOntzOjY6Il90b2tlbiI7czo0MDoiVFVxZnh0RjNQd1ZpT0dvZ2owR2dRbGRVYXB1VE9XSkJUTkkyR1RWdSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653714113
dLUvQLCZV98kmRwJR9hIM0u2ZSSFQPZaHRdaa6tT	\N	192.241.220.40	Mozilla/5.0 zgrab/0.x	YTozOntzOjY6Il90b2tlbiI7czo0MDoiY0hBSjR3VmhhQlBUbjVpTm9hczZzR1RvWlZnWHlrbDRzbjZLM2RPcCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653718703
v1hcuCNsi5yPDjXcpfTeGMjvQ7yDFkJ4yr4RNs8B	\N	192.133.77.18	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiaEFjTXplVGpsYVgwbVhuaElHTXpRb1J2QUtTS0I2N3lIVWZrMVRaayI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653592690
N3gz7EKYOrNbBRzPrnC6kYUaR0rJrMfVBZ58LlAk	\N	178.128.232.42	l9tcpid/v1.1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiYmw0WHBUN2RUM3llSU9mWVB5RXdmQmJXaVJTU3dTdjVRVHpjcG5FdCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653604504
kbAVmJIp5p2lNyE607JaqN6Y1akiFpPk53Aw2CL5	\N	193.118.53.210	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiZDl0OURXOEoxZjhqOHVocXBtMjNPYUpBSzBNUDNPakdMTmQzSXV2bCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653616735
tfvBbbUdAsKFWMzUkH8UYFwrZ2l49Jm1bpcdxCb3	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiY2dDQTZWcFlhQW05TmZmcDFXdEdwek1YWm5CSlJwMzBURE8weUVPWSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTM6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYvP1hERUJVR19TRVNTSU9OX1NUQVJUPXBocHN0b3JtIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==	1653626647
8cSUiDZJHWOmXr5BI7akrsS24Hn5bqm5HyIVizhR	\N	66.240.236.116	Mozilla/5.0 zgrab/0.x	YTozOntzOjY6Il90b2tlbiI7czo0MDoiQ042ekRrY3NlT0dreUhiVThqaVEzRXRYMW1aVWRtZG5jcVlmRFRESyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653638238
P9eVGVJC63fcXnZmuYPkCneV77As89oNxws2ggUS	\N	185.181.246.200	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiSmlqQ0ZMTVc3czk3NDRGMnB5ZmY3UnNDbmVWZk5hOWFIQVdIVWJrcSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653643656
xL2B9QzaehhjnTZHtP0N3zDyAxdXJoOYirp7IQtH	\N	192.133.77.16	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiRlhOYjhxandQZG1qckFwSTdaQmk3QUxraGJOejZiTzkyNXlqZlc5WiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653650992
z6SAfpJScnlUqaNhVCxv7FMqqsZEFoiScV94ApmM	\N	45.33.65.249	curl/7.54.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiU0RReHBqQlBXZEloaWlNRDhURVVrSTZSVHBvNXZnQTBSWFJFZHNJNyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653661686
2nWYIi0EC7LgMt1eMxBgz8Wu1NpS7TapN8z8IaLU	\N	45.33.65.249	curl/7.54.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiVHYwOG5VcHNBY25RQk9raHVudGpPWEVvZXU1T00yU1hXNmx2M2VpNyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653661688
SxEy37dL3vE9P6zmPDbRqiVZnKkgyVhxa9RdtEpL	\N	167.94.138.45	Mozilla/5.0 (compatible; CensysInspect/1.1; +https://about.censys.io/)	YTozOntzOjY6Il90b2tlbiI7czo0MDoiNm1wZ2l6dFJWekU4SHZYSjlxMm9MYk9YSng1MHRsaTl4TDJHQ2dZTiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653672683
pfbS9tXY7Zc18n7ku2Vp0h7g1eQf4YGTAPTkWQxs	\N	192.133.77.16	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiVkJCOW5kRWl4Z1pPTEJzcWhaVE13TEdHZFFCcUxPWWE4S2pUSFdVViI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653681691
dC1CSgpRK24dS3dvBLjEgkjfE8qB0y7EIEYHpPfy	\N	23.251.102.74	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiODZPcWJEbkdDNHIydkhVYk1OTUs2Tk9yNTFBOERObmJDYkFiSzVkMSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653688613
GMqeaGnMEiYBsYZ8dKpWHbpMzUqZRYIuLLTCVAeG	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiWHVoREp3TDFENDg4eEtoT0ZMQVQydG5VeXhJQmFYT1phRGZrQURSUCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU4OiJodHRwczovLzE1OS4yMjMuMzMuMTA2L2luZGV4LnBocD9mdW5jdGlvbj1jYWxsX3VzZXJfZnVuY19hcnJheSZzPSUyRkluZGV4JTJGJTVDdGhpbmslNUNhcHAlMkZpbnZva2VmdW5jdGlvbiZ2YXJzJTVCMCU1RD1tZDUmdmFycyU1QjElNUQlNUIwJTVEPUhlbGxvVGhpbmtQSFAyMSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653698249
QY3QnJnoTC3pjYx7v0QFj88pTdBpXiKQIBQJAPBU	\N	178.73.215.171		YTozOntzOjY6Il90b2tlbiI7czo0MDoiTEdYR0hTQVlCcEtKdWRDYU9jTHpYcXU1MmtyemdUblhoNjl0Ulp0YyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHBzOi8vdHVhaC5pbyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653705932
ECzfz7PdAVDQibgxwc15cuG2pidlAsqd1YApBJvn	\N	176.58.123.108	Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiN0Iwbm9KVkhUam1Hc3F5ZnpQd2oxbXRyNmxOUXVhUHpDMzZqeUtVWSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653715006
FjgXabdYnri47e6vX7O8qY61UqpgRsWcKUEVQvtb	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiRjZmWEpXdEZxNmVnVk11eGhrc3l5azNtSVZJaXJKelRtTHdCYTA3cyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653720651
a6j998K6qUERziza1NhJmPipUD9bvK4nL8QTWjpt	\N	192.133.77.16	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiOVZ2SmV2TlkxQ1ZBUFZrMW1RbVBhYktIdWpMeXl3ZEppUnVJVklEWiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653595211
S5Lq5ll2WgXGyqPO9ECPgoqWmnwjiFdVQGfAH0Bm	\N	167.94.146.58		YTozOntzOjY6Il90b2tlbiI7czo0MDoicFVwYUdpMFV3ZnNQNmZ1bE1nbHlFTzNiNFZxR0lSU2F2ZFNYT3h1TyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653607261
BJNs7CFOi9UJEytdIMSuudpRodfPyU5o5T85enLp	\N	185.180.143.72	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiamwwT1hkbjQ5Tmt1aVdzNDRPenFlc1ZpMTI2UVIwVjNJZUE5NlpFNiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653617473
6DGRoGfuPFYyzUNUBOkrr02FlpCfrJ4pKeTCqoGW	\N	74.82.47.3		YTozOntzOjY6Il90b2tlbiI7czo0MDoiVE9GS0R0aDdiTGFRbVVkVGc0Q3ozV0Nob2MycWFQVmsxS2ZqZDZkVCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653629346
JdJ9nbkDy2zlBNxnKq71IvxzypIoH1vA5vHZcjpv	\N	103.203.57.10	HTTP Banner Detection (https://security.ipip.net)	YTozOntzOjY6Il90b2tlbiI7czo0MDoiU3VIVUpSRHQ4eHQzTVU4YjlnRmhxaER5NXlUUXJiZDE5dnlvczB3UCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653642149
qT1mf4SZS1qjIHcMVKwRPhmpzSIgKJpnl5I3RNGF	\N	154.89.5.70		YTozOntzOjY6Il90b2tlbiI7czo0MDoiWU15V3g4WWRiR2ZLejc5ZmpoOGZiVFZWMVl3MExmVTkyTDFLdHpBeSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653644951
LTlh7RY4Ptzo1KhcvKk0WMVpIkInpDu6r3L06C6G	\N	139.162.201.69	Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoid2RNdndtTHh6MFQ2Y2ZNaDFaT0dmcEtxNE14blh4OWM4VE9VVlkwNSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653652750
MvDX7XvAsSkJ41QwLbbYxADtyNLRFjQUKR0d48yC	\N	45.33.65.249	curl/7.54.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoibnBxVXh2ZlhLeVpuclVVUDNSOHZxQ3d2SmhwV0I0aG9WTHZOWEhrWSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653661686
mrd4VgkFbmXSEUlnTazr31v0S7XukjW7bWMuGKB3	\N	139.162.207.84	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiZ3BZd1VwaWtSOVVPMm1xVUd1R280alRmS3ZzbG1paGxFVFJQaWVTYiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653662530
bkb8WaiwXLOAMqvmGtFZdaQGipVJiZ0sVqfb8zuN	\N	205.185.115.33	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiaVN4VFJPa1R2WGswaUJWa0EySDZlcWdqY2o0VkZvelIwb2F1TlVpRSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653673449
pT2XBBoKl0dh56b3MXMnMiGuYsGohNWuPk0pbLLX	\N	94.102.61.8	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiOHVMSU01TTRMU0JjR0Vjb1NxdUVPUjloNEtZSzYwV3IzVlNHOEhBcCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653683765
W8enmyi8FMZmAcmLsbxMhgf8zvnSFdNL1Hdb7tQf	\N	193.106.29.122	Mozilla/5.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiWmxiejdnbW5NQm1qbzdlR0N5NHg1YlBYbVJxVFFOUk4yVkw5U3ZycyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHBzOi8vdHVhaC5pbyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653690357
0edMcdcNrlsTm4ie11W5mWKmFma5Mrctt1Atz8MX	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiTm1GdVZSV2pyRXRqTEpEWVZDRTl1cUVNdDRlWUJiTmxSd3RFVmtaTiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NTM6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYvP1hERUJVR19TRVNTSU9OX1NUQVJUPXBocHN0b3JtIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==	1653698783
4A07bEzX8QNwGgNU8UJBiZdBTPevnnzB2gu3ROJg	\N	154.209.125.54	Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiZVF3aTg3UEZsQ2V5ZzF4em1abU1kSGUycWlxaWxWV2V5R00zVTNmYSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653706039
9GNm0hseOpA6ZW8d35mBjR4fHGO8M9NMNVZIDgf0	\N	192.133.77.18	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiOW5MR3lSWVNCcFFjVGF2NGJYRWFWZXY3YXI3REhBbUZpdVRqemZyZyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653715677
zNdcDN6J30gcxcPKPMu1h27a0kOtGMxUo3052gX4	\N	192.133.77.15	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoieFpUWTNSdFNOWms1bUFjR3ZuMzZ4OE1SdEcxa1kwc0RkanpzcnpUaSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653720966
oXE6oGhpiRgi8LY0IT7B83KUU9DLiNgWYmkYOI0J	\N	128.14.209.162	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiNThGdlhNQzNOQ3lkYVlXVlV3cEtOdXczeFhMMXVyTU93TDNTUHZjOSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653603259
4JUEQmIdm8V0FRaXupH1K3WTmVIyPlBZ8z5pIDCH	\N	167.94.146.58	Mozilla/5.0 (compatible; CensysInspect/1.1; +https://about.censys.io/)	YTozOntzOjY6Il90b2tlbiI7czo0MDoiVWZiWXNqc0I1VllySWpvQkNMQXNKRkd5dVhHc2N2SFdManpkdHNVYyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653607262
MNqW4MnP2p9WTlX08JHSqoiBTfZ9LqKsK1Byd1zI	\N	192.241.222.85	Mozilla/5.0 zgrab/0.x	YTozOntzOjY6Il90b2tlbiI7czo0MDoienp6WXJRZVNoMURkRE1HUVI0NFc3aEwwanRic3FZbFk2d2o4bm5URyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653623117
X4eHh9GalsWuiI02YxnQoPvfVYZOgCblOMi2iTXd	\N	5.45.207.143	Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)	YTozOntzOjY6Il90b2tlbiI7czo0MDoiWjI4VTlNa0RIRnhUY25ZdHg5MkxIbktvOW5iUm8xSkRFU01UektwNiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHBzOi8vdHVhaC5pbyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653630712
KQ0j7HyjzgkJV3jn5Z5XB4e1PmF0g186LN1mLBm6	\N	162.142.125.212		YTozOntzOjY6Il90b2tlbiI7czo0MDoiaDR3MUxjNmloUXo2TEc2Sjc4T0RiUFFhVjhKQzUyQ2tVNG1LWWNUYiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653642166
HBvIMKaZCotNiq0mWzMXpBbGKYoeCIj2TBt4LQDF	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoidzY0S0dxWERDbkg4TWlxN3RsQ1Rud0RscGhzTVdyb0Zvb3NCdUplOSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653645309
j4d0K6kn4sNKu138ajBeN5GQUsc8prdkSPDFki36	\N	180.75.250.77	Mozilla/5.0 (iPhone; CPU iPhone OS 13_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/76.0.3809.123 Mobile/15E148 Safari/605.1	YTozOntzOjY6Il90b2tlbiI7czo0MDoiRWV0SzRyOVZPSFhvRk02S1dtaWxCTkd4ekhxajJlcGxFbVNPUmtUQiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653658372
nOCtIwk56yjVaLQ9q1u45fV03FS5OUL2Jp8QYKOS	\N	45.33.65.249		YTozOntzOjY6Il90b2tlbiI7czo0MDoiM1A5WUlJSWdYbUFLN3E1anl3NzhXc2VPT1R4WGM0SWpjdE1yam1ZTyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHBzOi8vdHVhaC5pbyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653661687
0JqNieg1vo3qqr2XShC5iEZuRlotPcj3cKBafb6m	\N	128.14.141.34	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoidUpvd2lTVlpsY3VPWnN4bzFRWmFRQkdvMXlTS3h3TWZKZDY3Q2pHaCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653668432
EfAnYae26wSn83eNeJg7sdDLg7KFi1NdjO3QxDiC	\N	20.113.27.135	Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiZ2VqMlpwOGRhSDZBYlhqQUJGb2cyQkdoVlhYdmRVczFtVGdQU21xVyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653674882
cefSpr8k5vdfjTiLwPqBhgzmjLW6ur9Q8KJhMUTD	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoianc4bWtxaEozeTBzVnd5bTVBekNvZEJLcUFQTWRycFhOZEJBeHp5aiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653684415
OfeCNFZZyHJk5UZooAtAJoKuOtJ7Bu2Ank1pArKW	\N	51.159.23.43		YTozOntzOjY6Il90b2tlbiI7czo0MDoiYVJ5SUFYWk9iMGNNNWRsbTFTUG92OGsxSmV0Qkh3bjZzUE5LZnQ5ciI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653690528
QhJZAegEdZnju2yix6K44BGfoveYZAWn2VnjAgTP	\N	130.211.54.158	python-requests/2.27.1	YTozOntzOjY6Il90b2tlbiI7czo0MDoiZk1mSUZQRDRLUVlydDhFSGZYVDRSYVhxYjRKVmJkVTg1emxRWjl4eiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653701393
I1a8su9o4ka19FM7W56DoSyxm0ztKJRD7GHeKqE8	\N	3.85.223.44	Mozilla/5.0 (Windows NT 6.2;en-US) AppleWebKit/537.32.36 (KHTML, live Gecko) Chrome/51.0.3008.85 Safari/537.32	YTozOntzOjY6Il90b2tlbiI7czo0MDoiMnpid0I0QzZyeHIwTHZod2M4UlJzS3hOYkZqRzNFUmZxYVVGekdtbSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653707052
x0c16QchLoZPobRgVKYxQ0fBETQSMnZCajiEU5jF	\N	192.133.77.18	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiWDB1MndGZFlOakxVODFSMTVqd1ZGSDBHVk81ZHFpaE1GbnNGQlN3NyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653717996
kLgIsYbuvAf5r2l3DjGwmnil7XmI6qInHjmQH5pz	\N	51.159.23.43		YTozOntzOjY6Il90b2tlbiI7czo0MDoialhkRXdQTWNvUk9CbzVZR0V6d0dSRENKVXNIdVZIYUhndnFjVGJtRyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653604128
x2511sTFoPkBg8kaxFo4SxO7CZ4Zerqvhsd6WIO6	\N	174.138.61.44	Mozilla/5.0 (Linux; Android 8.0.0;) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Mobile Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoieXZNTjNWbmY4aG1CcmdSalJJaGxJMXd3NEE1RkpkMzE5MVgxWlJpZiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653616271
1a4pMpu1Ob0oSyo6bx7F0Hy4seCr5QDeaeauvOCs	\N	87.250.224.90	Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)	YTozOntzOjY6Il90b2tlbiI7czo0MDoiVDJxSm5jOTJ0MERPZ0c4dm9ZSW9yenlIZ09HSlJqNExSUDJMUENTTyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHBzOi8vdHVhaC5pbyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653623210
s3BKu1Wm4GosFJGw8yDxn4Oxy4yQTqAiBOLHevez	\N	192.133.77.16	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiUlU3Tzc4aWNXYU1BdXdVc05FUzNSaVZ5SURkR1dXM2xzRkpYdk5VQiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653634124
udAmsSnPl5BdxGLdqfOlY6TIVdAiluAGUwbKa1Ic	\N	162.142.125.212	Mozilla/5.0 (compatible; CensysInspect/1.1; +https://about.censys.io/)	YTozOntzOjY6Il90b2tlbiI7czo0MDoiTGNZcTNLaWxZc016REhPaTB3UW1OZjUzS1lwR2FlSDBLd1VCckwxTiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653642166
ZkOW8Pmp2xop0ooKnzUlXvcYGMvL1fNdy8NAfWtt	\N	128.14.133.58	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiNVVvd0J6TW1lVWRwbnN3RmlkUVFSY1FpOEQ2Sk8wckhIbExSWGl4ciI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653648156
INneM5DR3cYMFfi3EuZWWV4eJufKwTWpXsVIXtFk	\N	45.33.65.249		YTozOntzOjY6Il90b2tlbiI7czo0MDoicnZLTTNjd0lJcWNCUm56VUIwTGl4WXR1ZjM1eGNrYklncTV6eU1jZiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHBzOi8vdHVhaC5pbyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653661685
DjvA0q1geTNe968xkHEqwwAH3M6XG1EAYqZlkVQR	\N	45.33.65.249	curl/7.54.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiR2QxVTlxa3gwNU5NVnBlWk9ibURQNjNzZ0ZoRGhlSzY5Qm01TEpENyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653661687
FSnTTcEvj6WjCCoeK5CpBhI9Uu1zGh1XLA7smOSE	\N	180.75.250.77	Mozilla/5.0 (iPhone; CPU iPhone OS 13_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/76.0.3809.123 Mobile/15E148 Safari/605.1	YTozOntzOjY6Il90b2tlbiI7czo0MDoiM0g4cDBiQ0lHcWp5emthTTZaYWJrWXZ3QnJTZG1CbUpXb3FmT0tCaCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653668608
t1vmSUyGr2kjHQIpHzAj5Hr0tpNl0jBdsHbRlxEw	\N	192.133.77.16	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiV285RkRuOWhhUXoydXFja25CWUZ2S0xTVXNLOVZpc1NYMGsyTVhociI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653674969
qcFJG3tYqyzaHO8zgaQqlFN8J4CTrhIFakupNYjM	\N	192.133.77.18	Twitterbot/1.0	YTozOntzOjY6Il90b2tlbiI7czo0MDoiblNWSkk5ZnpreWdHNzBVSXJoVHVsRUdQZDdOaEt0T1lHYnZnR2dHUCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHBzOi8vdHVhaC5pby9yb2Jvc3R1ZGlvL2cvMTIiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653686880
89A4nOFgZomFtRTARVduyXK62SsLANjc2zcKHr4U	\N	23.251.102.74	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiQnJ6SEh4ajhPYXkyVExhbFdQUExFTzQ0VVJKdlF6dExTU1JIaVBKciI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653692769
trghxbaUsJGrK2lnNKxZ7fZZp36PH7lJXghFHMPN	\N	3.89.242.155	Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.16 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiU09jQkxDdnBDS1lKVlBxMkYwM1lwVDJuZ0tFNHFjQTBRcElkZ2tUViI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHBzOi8vdHVhaC5pbyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=	1653702199
BVmnze9aaoKI1UWakEFo0p6TU6coAqAHM3mbtRC4	\N	89.248.165.178	Apache-HttpClient/4.5.12 (Java/1.8.0_312)	YTozOntzOjY6Il90b2tlbiI7czo0MDoieGdvaTRHejlIVUp6d2d6R0R2TE54cVNrZDY5OUlSbVl3VEJYUDh3YiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTk6Imh0dHBzOi8vb3JiMTF0YS5jb20iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653709798
JPDuqqBzQOnfwDoJoxeoTjry0qyR5jEuyRbRsAij	\N	193.106.191.48	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36	YTozOntzOjY6Il90b2tlbiI7czo0MDoiVnplNkNSUUd6ZXl2SHNKSWNPMUxISXZnS2xaN0tlTjFja2JSUlIzdyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjI6Imh0dHBzOi8vMTU5LjIyMy4zMy4xMDYiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19	1653718683
\.


--
-- Data for Name: subscription_packages; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.subscription_packages (id, title, remark, rate_per_month, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: subscriptions; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.subscriptions (id, status, validity_in_days, user_id, subscription_package_id, created_at, updated_at) FROM stdin;
1	pending	\N	1	1	2022-01-26 13:50:29	2022-01-26 13:50:29
2	pending	\N	2	1	2022-01-29 13:08:01	2022-01-29 13:08:01
3	pending	\N	3	1	2022-01-31 06:16:03	2022-01-31 06:16:03
4	pending	\N	4	1	2022-02-03 13:55:46	2022-02-03 13:55:46
5	pending	\N	5	1	2022-02-09 17:37:49	2022-02-09 17:37:49
6	pending	\N	6	1	2022-02-17 16:36:38	2022-02-17 16:36:38
7	pending	\N	7	1	2022-02-21 18:49:25	2022-02-21 18:49:25
8	pending	\N	8	1	2022-02-21 18:51:15	2022-02-21 18:51:15
9	pending	\N	9	1	2022-02-21 19:04:13	2022-02-21 19:04:13
10	pending	\N	10	1	2022-02-21 19:05:46	2022-02-21 19:05:46
11	pending	\N	11	1	2022-02-21 19:05:53	2022-02-21 19:05:53
12	pending	\N	12	1	2022-02-21 19:06:11	2022-02-21 19:06:11
13	pending	\N	13	1	2022-02-21 19:06:13	2022-02-21 19:06:13
14	pending	\N	14	1	2022-02-21 19:08:34	2022-02-21 19:08:34
15	pending	\N	15	1	2022-02-21 19:10:15	2022-02-21 19:10:15
16	pending	\N	16	1	2022-02-21 19:12:06	2022-02-21 19:12:06
17	pending	\N	17	1	2022-02-21 19:17:06	2022-02-21 19:17:06
18	pending	\N	18	1	2022-02-21 19:19:39	2022-02-21 19:19:39
19	pending	\N	19	1	2022-02-21 19:21:33	2022-02-21 19:21:33
20	pending	\N	20	1	2022-02-21 19:22:29	2022-02-21 19:22:29
21	pending	\N	21	1	2022-02-21 19:31:28	2022-02-21 19:31:28
22	pending	\N	22	1	2022-02-21 19:33:38	2022-02-21 19:33:38
23	pending	\N	23	1	2022-02-21 19:37:35	2022-02-21 19:37:35
24	pending	\N	24	1	2022-02-21 19:42:21	2022-02-21 19:42:21
25	pending	\N	25	1	2022-02-21 19:47:18	2022-02-21 19:47:18
26	pending	\N	26	1	2022-02-21 19:49:47	2022-02-21 19:49:47
27	pending	\N	27	1	2022-02-21 19:57:45	2022-02-21 19:57:45
28	pending	\N	28	1	2022-02-21 20:00:51	2022-02-21 20:00:51
29	pending	\N	29	1	2022-02-21 20:03:13	2022-02-21 20:03:13
30	pending	\N	30	1	2022-02-21 20:05:30	2022-02-21 20:05:30
31	pending	\N	31	1	2022-02-21 20:08:55	2022-02-21 20:08:55
32	pending	\N	32	1	2022-02-21 20:09:25	2022-02-21 20:09:25
33	pending	\N	33	1	2022-02-21 20:13:02	2022-02-21 20:13:02
34	pending	\N	34	1	2022-02-21 20:25:09	2022-02-21 20:25:09
35	pending	\N	35	1	2022-02-21 20:28:08	2022-02-21 20:28:08
36	pending	\N	36	1	2022-02-21 20:34:23	2022-02-21 20:34:23
37	pending	\N	37	1	2022-02-21 20:37:33	2022-02-21 20:37:33
38	pending	\N	38	1	2022-02-21 22:44:42	2022-02-21 22:44:42
39	pending	\N	39	1	2022-02-22 06:57:39	2022-02-22 06:57:39
40	pending	\N	40	1	2022-02-23 16:03:03	2022-02-23 16:03:03
41	pending	\N	41	1	2022-02-26 15:02:44	2022-02-26 15:02:44
42	pending	\N	42	1	2022-03-06 03:53:47	2022-03-06 03:53:47
43	pending	\N	43	1	2022-03-07 23:13:05	2022-03-07 23:13:05
44	pending	\N	44	1	2022-03-08 13:38:25	2022-03-08 13:38:25
45	pending	\N	45	1	2022-03-13 14:15:36	2022-03-13 14:15:36
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.users (id, name, email, eth_address, subdomain, status, email_verified_at, password, api_token, remember_token, current_team_id, profile_photo_path, created_at, updated_at, two_factor_secret, two_factor_recovery_codes) FROM stdin;
13	kidd	kidd.kudi@outlook.com	0x021cb9DD7c0fa60eDe1b35DbccC52757C21E11EC	kidd	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=kidd	2022-02-21 19:06:13	2022-02-21 19:06:13	\N	\N
2	Test	test@gmail.com	0x72923C858437eC61d65CcCC531EeDFF4ac33B640	testaccount	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Test	2022-01-29 13:08:01	2022-01-29 13:08:01	\N	\N
3	Ilham Ishak Aman	ilhamsahak@gmail.com	0x818ad13e1f690A9F3edCF54Ee19E08AD661bfc1A	ilhamishakaman	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Ilham Ishak Aman	2022-01-31 06:16:03	2022-01-31 06:16:03	\N	\N
4	AnonY	\N	0xc67b823974e1BDd1D5aC3f6231FD82C0c7029734	\N	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=AnonY	2022-02-03 13:55:46	2022-02-03 13:55:46	\N	\N
14	Neveah of NFT	mthrfckr.nft@hotmail.com	0xfB55ba1be6aeD490Ba58bAD9517308eBcA8501B2	Nevaeh	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Neveah of NFT	2022-02-21 19:08:34	2022-02-21 19:08:34	\N	\N
15	Muhammad Najmuddin	riotsquadeh@gmail.com	0xabC32FA26c041852f5B6E1BAb81558F6d68f31ba	mudennn	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Muhammad Najmuddin	2022-02-21 19:10:14	2022-02-21 19:10:14	\N	\N
16	GuitaPlec	guitaplec@gmail.com	0xA318B977Ec8E4b04e9715327a1F9db70A0680Dd1	GuitaPlec	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=GuitaPlec	2022-02-21 19:12:06	2022-02-21 19:12:06	\N	\N
17	Richie	richteo2@gmail.com	0x3F274e69bc09eD3276cCf0C982a655d3941de546	Richie	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Richie	2022-02-21 19:17:06	2022-02-21 19:17:06	\N	\N
1	Tuah NFT	mytuah.io@gmail.com	0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0	tuahnft	active	\N	\N	\N	\N	\N	images/tuahnft/general/tuah-io-profile-img-tuahnft-1644289395AENbUG.png	2022-01-26 13:50:29	2022-02-08 11:03:15	\N	\N
18	Amirul	aminmzln20@gmail.com	0x0CC97AcF1B54D56e2A05d2055A14feD2a98A800B	Kudoukiddd	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Amirul	2022-02-21 19:19:39	2022-02-21 19:19:39	\N	\N
5	Nando	aureliakairos@gmail.com	0x61dEEafdDEf6cAB550e45E075d0A7283Ecdbd378	robo	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Robo Studio	2022-02-09 17:37:48	2022-02-09 17:43:08	\N	\N
19	safsatu	safuw1moon@gmail.com	0x828DE0799953E81f8D3D9f160bc5c4C8a5D40E4E	safsatu	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=safsatu	2022-02-21 19:21:33	2022-02-21 19:21:33	\N	\N
6	Robo Studio	robo.studio2021@gmail.com	0xAaB64e6A7Ef7fcf457ae8a3f8E7142F18da023F6	robostudio	active	\N	\N	\N	\N	\N	images/robostudio/general/tuah-io-profile-img-robostudio-1645435063A6RyEM.png	2022-02-17 16:36:38	2022-02-21 17:17:43	\N	\N
7	limau	limauarts@gmail.com	0x4D4EF32E5330161471dA87F5dD972C1b2e838041	limauarts	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=limau	2022-02-21 18:49:25	2022-02-21 18:49:25	\N	\N
8	Not Yeti	totallynotyeti@gmail.com	0x3A264F42F4e37f6C0B3806B40B509Fb4BDD5A1b9	totallynotyeti	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Not Yeti	2022-02-21 18:51:15	2022-02-21 18:51:15	\N	\N
9	Abu Hafiz	abuhafiz99@gmail.com	0xAd540883342B74EfA65A2B8345f4416152f429dF	AbuHafiz	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Abu Hafiz	2022-02-21 19:04:13	2022-02-21 19:04:13	\N	\N
10	KriptoKidd	kriptokidd.my@gmail.com	0x69FE55b13Dc66FE6c19e88525Bf890740fb28600	kriptokiddmy	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=KriptoKidd	2022-02-21 19:05:46	2022-02-21 19:05:46	\N	\N
11	OctoSquad	octosquadd@gmail.com	0xb738946a4a97B86D0C3BEbc32c0632Ff0EeB4381	Octo	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=OctoSquad	2022-02-21 19:05:53	2022-02-21 19:05:53	\N	\N
12	Wildan Rizqullah	wrknizam@gmail.com	0x44c264F47A5ee082CB99C23d9036484f573dCe4a	0xWildan	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Wildan Rizqullah	2022-02-21 19:06:11	2022-02-21 19:06:11	\N	\N
20	glscld	echo_jittery_0p@icloud.com	0xa34254C9d79f03cE838e5005719f8e77cbDe444c	glscld	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=glscld	2022-02-21 19:22:29	2022-02-21 19:22:29	\N	\N
21	Potion Master	ammarshakirinzawawi@gmail.com	0x0C41337a9CCd52895636D212fa0115A3BcFC8DD5	PotionMaster	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Potion Master	2022-02-21 19:31:28	2022-02-21 19:31:28	\N	\N
22	Pim	faheem.olorin@gmail.com	0x2f56f0435274C12A19016F86329028D295994abD	PimoSupremo	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Pim	2022-02-21 19:33:38	2022-02-21 19:33:38	\N	\N
23	SCiO	syafiqamran94@gmail.com	0x253Dcb4e61F1E715fcfd478b9B9Ba9e01a448dcb	scio	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=SCiO	2022-02-21 19:37:35	2022-02-21 19:37:35	\N	\N
24	Faliq Fahmi	faliqfahmi@gmail.com	0x2c2882399cB958BbB682C8a0976B590B451A32bB	faliqfahmie	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Faliq Fahmi	2022-02-21 19:42:21	2022-02-21 19:42:21	\N	\N
25	Afric Guild	dakwah.indah11@gmail.com	0xA26780eb85347B069351E403F7F8724F2331Ef38	Afric Guild	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Afric Guild	2022-02-21 19:47:18	2022-02-21 19:47:18	\N	\N
26	Hazlami Effendi	hazlami.work@gmail.com	0xFe2E1D458a4EdaB962179DA3A7CDd8bFABf39B81	Ponderman_NFT	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Hazlami Effendi	2022-02-21 19:49:47	2022-02-21 19:49:47	\N	\N
27	Huxsterized	huxsterized@gmail.com	0xf186c01A6b429eca496ed0bAa46c61170f5fA9C8	Hux	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Huxsterized	2022-02-21 19:57:45	2022-02-21 19:57:45	\N	\N
28	Zakwan Jaafar	zakwanjaafarr@gmail.com	0xbb83322dEcB10c79C18AE1945c997Db333C37f85	zakwan	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Zakwan Jaafar	2022-02-21 20:00:51	2022-02-21 20:00:51	\N	\N
29	Wan Arif	0ne4rif@gmail.com	0x1a70fD5Fc8725E0d0Cf0D1b2E64a029DC1c7625D	0ne4rif	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Wan Arif	2022-02-21 20:03:13	2022-02-21 20:03:13	\N	\N
30	shxhrxlxr	shahrulxra@gmail.com	0x7a81fFF90e27DB324d9e113DF19F1aFD0A4D5B30	JoeSilverberg	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=shxhrxlxr	2022-02-21 20:05:30	2022-02-21 20:05:30	\N	\N
31	Ahmad Sanuri Zulkefli	san_msia@yahoo.com	0x950FEC7e16506E2f3738D95cC20C2b55E8A970F5	sanuri	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Ahmad Sanuri Zulkefli	2022-02-21 20:08:55	2022-02-21 20:08:55	\N	\N
32	Meow Social Club	topsyckrects@gmail.com	0x001dbB7EA73e772924e82EbcB2206181D904E8DD	meowsocialclub	active	\N	\N	\N	\N	\N	images/meowsocialclub/general/tuah-io-profile-img-meowsocialclub-1645445468pgZh7u.png	2022-02-21 20:09:25	2022-02-21 20:11:08	\N	\N
33	JACKT	azuhribusiness@gmail.com	0x3a39Eeae464344D2999132BDdaCF415112525B28	JacktBattousai	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=JACKT	2022-02-21 20:13:02	2022-02-21 20:13:02	\N	\N
34	Ahmad Sanuri Zulkefli	ee10pi3r@yahoo.com	0x608e5393C1cd3E7ba0E287fC53ac1b16f358Abb4	sanuria	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Ahmad Sanuri Zulkefli	2022-02-21 20:25:09	2022-02-21 20:25:09	\N	\N
35	Pawana	sanuri.zulkefli@gmail.com	0xc3d72590d7123fdF9A39922269360A668505aa78	san	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Pawana	2022-02-21 20:28:08	2022-02-21 20:28:08	\N	\N
36	MeetFauzanHere	fznhkm.1992@gmail.com	0x23ff048719fd3b0f33CbE1184178878B1781DEDa	kurkurofficial	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=MeetFauzanHere	2022-02-21 20:34:23	2022-02-21 20:34:23	\N	\N
37	Melmontot	meldebrastephen@gmail.com	0xe959B2b7C34C69FfABf5c12033C3C7994cc37A7A	montot	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Melmontot	2022-02-21 20:37:33	2022-02-21 20:37:33	\N	\N
38	Bearruang	asyraf67@gmail.com	0x1b5C7FCbB27562AB475c61Bb403ef9320E63ff62	Bearruang	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Bearruang	2022-02-21 22:44:42	2022-02-21 22:44:42	\N	\N
39	Hazhazran_nft	hazranhazrul@gmail.com	0xe12edFF2620DAC230776620C06572Aa3962e501c	Hazran	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Hazhazran_nft	2022-02-22 06:57:39	2022-02-22 06:57:39	\N	\N
40	Cusdel	cusdel@foodlive.asia	0xE36B44D5Ba5226f4161De4cE1AaF62E5566db80f	Cusdel	active	\N	\N	\N	\N	\N	images/Cusdel/general/tuah-io-profile-img-Cusdel-1645603425dJivwJ.png	2022-02-23 16:03:03	2022-02-23 16:03:46	\N	\N
41	MadGummy | Cibong	akif.mazlan3@gmail.com	0x7f5C2d5ddb06baE282Faf8De02A8cEe129B9b1e2	Cibong	active	\N	\N	\N	\N	\N	images/Cibong/general/tuah-io-profile-img-Cibong-1645859040n87ifN.png	2022-02-26 15:02:44	2022-02-26 15:04:00	\N	\N
42	Danish Wafi	hello@danishwafi.com	0x27dE2FcDd888D07f807b243576065cF6d4c436f9	danishwafi	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Danish Wafi	2022-03-06 03:53:47	2022-03-06 03:53:47	\N	\N
43	pelempunk	pelempunk.nft@gmail.com	0x5511142db2e5E873c5CD00E74c189f251A2ca4F1	pelempunk	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=pelempunk	2022-03-07 23:13:05	2022-03-07 23:13:05	\N	\N
44	Syahmi Rafsanjani	syahmishaarani@zoho.com	0x94c970c7c352AA8f9C1cca09396E07873f3FC5Be	rafsans	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=Syahmi Rafsanjani	2022-03-08 13:38:25	2022-03-08 13:38:25	\N	\N
45	orkes a hizadin	orkesahizadin@gmail.com	0x55b7F149ecB75c9f6E57383F9f8E9ea3E7483d68	orkesahizadin	active	\N	\N	\N	\N	\N	https://ui-avatars.com/api/?name=orkes a hizadin	2022-03-13 14:15:36	2022-03-13 14:15:36	\N	\N
\.


--
-- Name: campaigns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.campaigns_id_seq', 1, false);


--
-- Name: event_custom_addresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.event_custom_addresses_id_seq', 94, true);


--
-- Name: event_gifts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.event_gifts_id_seq', 20, true);


--
-- Name: event_nfts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.event_nfts_id_seq', 1, false);


--
-- Name: gift_custom_field_datas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.gift_custom_field_datas_id_seq', 1, true);


--
-- Name: gift_custom_fields_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.gift_custom_fields_id_seq', 9, true);


--
-- Name: gifts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.gifts_id_seq', 17, true);


--
-- Name: giveaways_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.giveaways_id_seq', 12, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.migrations_id_seq', 50, true);


--
-- Name: nft_collection_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.nft_collection_groups_id_seq', 7, true);


--
-- Name: nft_collections_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.nft_collections_id_seq', 209, true);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.permissions_id_seq', 1, false);


--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.personal_access_tokens_id_seq', 1, false);


--
-- Name: redemption_gift_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.redemption_gift_details_id_seq', 65, true);


--
-- Name: redemption_status_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.redemption_status_detail_id_seq', 127, true);


--
-- Name: redemptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.redemptions_id_seq', 65, true);


--
-- Name: role_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.role_user_id_seq', 45, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, false);


--
-- Name: subscription_packages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.subscription_packages_id_seq', 1, false);


--
-- Name: subscriptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.subscriptions_id_seq', 45, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.users_id_seq', 45, true);


--
-- Name: campaigns campaigns_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.campaigns
    ADD CONSTRAINT campaigns_pkey PRIMARY KEY (id);


--
-- Name: event_custom_addresses event_custom_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.event_custom_addresses
    ADD CONSTRAINT event_custom_addresses_pkey PRIMARY KEY (id);


--
-- Name: event_gifts event_gifts_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.event_gifts
    ADD CONSTRAINT event_gifts_pkey PRIMARY KEY (id);


--
-- Name: event_nfts event_nfts_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.event_nfts
    ADD CONSTRAINT event_nfts_pkey PRIMARY KEY (id);


--
-- Name: gift_custom_field_datas gift_custom_field_datas_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.gift_custom_field_datas
    ADD CONSTRAINT gift_custom_field_datas_pkey PRIMARY KEY (id);


--
-- Name: gift_custom_fields gift_custom_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.gift_custom_fields
    ADD CONSTRAINT gift_custom_fields_pkey PRIMARY KEY (id);


--
-- Name: gifts gifts_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.gifts
    ADD CONSTRAINT gifts_pkey PRIMARY KEY (id);


--
-- Name: giveaways giveaways_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.giveaways
    ADD CONSTRAINT giveaways_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: nft_collection_groups nft_collection_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.nft_collection_groups
    ADD CONSTRAINT nft_collection_groups_pkey PRIMARY KEY (id);


--
-- Name: nft_collections nft_collections_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.nft_collections
    ADD CONSTRAINT nft_collections_pkey PRIMARY KEY (id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_token_unique; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);


--
-- Name: redemption_gift_details redemption_gift_details_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.redemption_gift_details
    ADD CONSTRAINT redemption_gift_details_pkey PRIMARY KEY (id);


--
-- Name: redemption_status_detail redemption_status_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.redemption_status_detail
    ADD CONSTRAINT redemption_status_detail_pkey PRIMARY KEY (id);


--
-- Name: redemptions redemptions_code_unique; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.redemptions
    ADD CONSTRAINT redemptions_code_unique UNIQUE (code);


--
-- Name: redemptions redemptions_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.redemptions
    ADD CONSTRAINT redemptions_pkey PRIMARY KEY (id);


--
-- Name: role_user role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: subscription_packages subscription_packages_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.subscription_packages
    ADD CONSTRAINT subscription_packages_pkey PRIMARY KEY (id);


--
-- Name: subscriptions subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- Name: users users_api_token_unique; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_api_token_unique UNIQUE (api_token);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_eth_address_unique; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_eth_address_unique UNIQUE (eth_address);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_subdomain_unique; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_subdomain_unique UNIQUE (subdomain);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: personal_access_tokens_tokenable_type_tokenable_id_index; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);


--
-- Name: sessions_last_activity_index; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX sessions_last_activity_index ON public.sessions USING btree (last_activity);


--
-- Name: sessions_user_id_index; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX sessions_user_id_index ON public.sessions USING btree (user_id);


--
-- Name: nft_collection_nft_collection_group nft_collections_group_pivot_nft_collection_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.nft_collection_nft_collection_group
    ADD CONSTRAINT nft_collections_group_pivot_nft_collection_group_id_foreign FOREIGN KEY (nft_collection_group_id) REFERENCES public.nft_collection_groups(id) ON DELETE CASCADE;


--
-- Name: nft_collection_nft_collection_group nft_collections_group_pivot_nft_collection_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.nft_collection_nft_collection_group
    ADD CONSTRAINT nft_collections_group_pivot_nft_collection_id_foreign FOREIGN KEY (nft_collection_id) REFERENCES public.nft_collections(id) ON DELETE CASCADE;


--
-- Name: permission_role permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: permission_role permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: role_user role_user_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: role_user role_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

