<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Auth;

use Atymic\Twitter\Twitter as TwitterContract;
use Illuminate\Http\JsonResponse;
// use Twitter;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/api/owner_profile/{wallet_address}', [App\Http\Controllers\PentasApiController::class, 'owner_profile'])->name('getOwnerProfile');
Route::get('/api/nfts/{wallet_address}', [App\Http\Controllers\PentasApiController::class, 'nfts'])->name('getNfts');
Route::get('/api/nft/{pentas_id}', [App\Http\Controllers\PentasApiController::class, 'nft'])->name('getNft');
Route::get('/api/nfts/{subdomain}/{type}/{id}', [App\Http\Controllers\PentasApiController::class, 'nfts_based_on_user'])->name('getNftsBasedOnUser');
Route::get('/api/selected_nfts/{subdomain}/{type}/{id}', [App\Http\Controllers\PentasApiController::class, 'selected_nfts'])->name('getSelectedNfts');
Route::get('/api/nft/{pentas_id}/transaction', [App\Http\Controllers\PentasApiController::class, 'nft_transaction'])->name('getNftTransaction');
Route::get('/api/nft/{pentas_id}/sale', [App\Http\Controllers\PentasApiController::class, 'nft_sale_history'])->name('getSaleHistory');
Route::get('/api/nfts/users/{wallet_address}', [App\Http\Controllers\PentasApiController::class, 'user_with_nfts'])->name('getUserWithNfts');

Route::post('/api/verify_redemption', [App\Http\Controllers\ApiController::class, 'verifyRedemption'])->name('verifyRedemption');
Route::post('/api/update_redemption', [App\Http\Controllers\ApiController::class, 'updateRedemption'])->name('updateRedemption');
Route::post('/api/verify_user', [App\Http\Controllers\ApiController::class, 'verifyUser'])->name('verifyUser');
Route::post('/api/create_user', [App\Http\Controllers\ApiController::class, 'createUser'])->name('createUser');
// Route::middleware(['auth:sanctum', 'verified'])->post('/api/update_redemption_status', [App\Http\Controllers\ApiController::class, 'updateRedemptionStatus'])->name('updateRedemptionStatus');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard', [ "giveaways" => Auth::user()->giveaways()->get(), "gifts" => Auth::user()->gifts()->get(), "redemptions" => Auth::user()->redemptions(), "nft_collections" => Auth::user()->nft_collections, "nft_collection_groups" => Auth::user()->nft_collection_groups ]);
})->name('dashboard')->breadcrumb('Dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/edit_profile', [App\Http\Controllers\ProfileController::class, 'edit'])->name('editProfile');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/users/create', [App\Http\Controllers\UserController::class, 'create'])->name('createUser');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('storeUser');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/users/{id}', [App\Http\Controllers\UserController::class, 'show'])->name('showUser');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/users/edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('editUser');
Route::middleware(['auth:sanctum', 'verified'])->delete('/dashboard/users/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('deleteUser');
// Giveaway Routes
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/giveaways', [App\Http\Controllers\GiveawayController::class, 'index'])->name('giveaways')->breadcrumb('Giveaways', 'dashboard');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/giveaways/create', [App\Http\Controllers\GiveawayController::class, 'create'])->name('createGiveaway')->breadcrumb('Create', 'giveaways');
;
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/giveaways/store', [App\Http\Controllers\GiveawayController::class, 'store'])->name('storeGiveaway');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/giveaways/{id}', [App\Http\Controllers\GiveawayController::class, 'show'])->name('showGiveaway')->breadcrumb(fn($id) => Auth::user()->giveaways()->find($id)->title, 'giveaways');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/giveaways/{id}/edit', [App\Http\Controllers\GiveawayController::class, 'edit'])->name('editGiveaway')->breadcrumb("Edit", 'showGiveaway');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/giveaways/{id}/update', [App\Http\Controllers\GiveawayController::class, 'update'])->name('updateGiveaway');
Route::middleware(['auth:sanctum', 'verified'])->delete('/dashboard/giveaways/{id}', [App\Http\Controllers\GiveawayController::class, 'destroy'])->name('deleteGiveaway');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/gifts', [App\Http\Controllers\GiftController::class, 'index'])->name('gifts')->breadcrumb('Gifts', 'dashboard');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/gifts/create', [App\Http\Controllers\GiftController::class, 'create'])->name('createGift')->breadcrumb("Create", 'gifts');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/gifts/store', [App\Http\Controllers\GiftController::class, 'store'])->name('storeGift');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/gifts/{id}', [App\Http\Controllers\GiftController::class, 'show'])->name('showGift')->breadcrumb(fn($id) => Auth::user()->gifts()->find($id)->title, 'gifts');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/gifts/{id}/edit', [App\Http\Controllers\GiftController::class, 'edit'])->name('editGift')->breadcrumb("Edit", 'showGift');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/gifts/{id}/update', [App\Http\Controllers\GiftController::class, 'update'])->name('updateGift');
Route::middleware(['auth:sanctum', 'verified'])->delete('/dashboard/gifts/{id}', [App\Http\Controllers\GiftController::class, 'destroy'])->name('deleteGift');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/nfts', [App\Http\Controllers\NftCollectionController::class, 'index'])->name('nft_collections')->breadcrumb('NFTs', 'dashboard');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/nfts/create', [App\Http\Controllers\NftCollectionController::class, 'create'])->name('createNftCollection')->breadcrumb("Create", 'nft_collections');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/nfts/store', [App\Http\Controllers\NftCollectionController::class, 'store'])->name('storeNftCollection');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/nfts/{id}', [App\Http\Controllers\NftCollectionController::class, 'show'])->name('showNftCollection')->breadcrumb(fn($id) => Auth::user()->nft_collections()->find($id)->title, 'nft_collections');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/nfts/{id}/edit', [App\Http\Controllers\NftCollectionController::class, 'edit'])->name('editNftCollection')->breadcrumb("Edit", 'showNftCollection');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/nfts/{id}/update', [App\Http\Controllers\NftCollectionController::class, 'update'])->name('updateNftCollection');
Route::middleware(['auth:sanctum', 'verified'])->delete('/dashboard/nfts/{id}', [App\Http\Controllers\NftCollectionController::class, 'destroy'])->name('deleteNftCollection');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/collections', [App\Http\Controllers\NftCollectionGroupController::class, 'index'])->name('nft_collection_groups')->breadcrumb('Collections', 'dashboard');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/collections/create', [App\Http\Controllers\NftCollectionGroupController::class, 'create'])->name('createNftCollectionGroup')->breadcrumb("Create", 'nft_collection_groups');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/collections/store', [App\Http\Controllers\NftCollectionGroupController::class, 'store'])->name('storeNftCollectionGroup');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/collections/{id}', [App\Http\Controllers\NftCollectionGroupController::class, 'show'])->name('showNftCollectionGroup')->breadcrumb(fn($id) => Auth::user()->nft_collection_groups()->find($id)->title, 'nft_collections');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/collections/{id}/edit', [App\Http\Controllers\NftCollectionGroupController::class, 'edit'])->name('editNftCollectionGroup')->breadcrumb("Edit", 'showNftCollectionGroup');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/collections/{id}/update', [App\Http\Controllers\NftCollectionGroupController::class, 'update'])->name('updateNftCollectionGroup');
Route::middleware(['auth:sanctum', 'verified'])->delete('/dashboard/collections/{id}', [App\Http\Controllers\NftCollectionGroupController::class, 'destroy'])->name('deleteNftCollectionGroup');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/redemptions', [App\Http\Controllers\RedemptionController::class, 'index'])->name('redemptions')->breadcrumb('Redemptions', 'dashboard');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/redemptions/create', [App\Http\Controllers\RedemptionController::class, 'create'])->name('createRedemption');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/redemptions/store', [App\Http\Controllers\RedemptionController::class, 'store'])->name('storeRedemption');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/redemptions/{id}', [App\Http\Controllers\RedemptionController::class, 'show'])->name('showRedemption');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/redemptions/{id}/edit', [App\Http\Controllers\RedemptionController::class, 'edit'])->name('editRedemption');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/redemptions/{id}/update', [App\Http\Controllers\RedemptionController::class, 'update'])->name('updateRedemption');
Route::middleware(['auth:sanctum', 'verified'])->delete('/dashboard/redemptions/{id}', [App\Http\Controllers\RedemptionController::class, 'destroy'])->name('deleteRedemption');
Route::middleware(['auth:sanctum', 'verified'])->post('/dashboard/redemptions/{id}/update_status', [App\Http\Controllers\RedemptionController::class, 'updateStatus'])->name('updateStatusRedemption');

Route::get('/{subdomain}/g/{id}', [App\Http\Controllers\SiteController::class, 'showGiveaways'])->name('publicShowGiveaways');
Route::get('/{subdomain}/c/{id}', [App\Http\Controllers\SiteController::class, 'showCampaigns'])->name('publicShowCampaigns');
Route::get('/send/email', [App\Http\Controllers\MailController::class, 'mail'])->name('sendEmail'); // For testing only

Route::get('/', function () {
	return Inertia::render('Site/Homepage');
})->name('home');

Route::domain('circle.tuah.io')->group(function () {
    Route::get('/', [App\Http\Controllers\SiteController::class, 'circles'])->name('circles');    
    Route::get('/result', [App\Http\Controllers\SiteController::class, 'circles'])->name('circles');    
});

Route::get('/tweets/{id}', function($id)
{
    $params = [
		'place.fields' => 'country,name',
		'tweet.fields' => 'author_id,geo',
		'expansions' => 'author_id,in_reply_to_user_id',
		TwitterContract::KEY_RESPONSE_FORMAT => TwitterContract::RESPONSE_FORMAT_JSON,
	];
	return JsonResponse::fromJsonString(Twitter::getTweet($id, $params));
});

// Route::get('/episodes', function () {
// 	return Inertia::render('Site/Episodes');
// })->name('publicEpisodes');

// Route::get('/tokens', function () {
// 	return Inertia::render('Site/Tokens');
// })->name('tokens');

// Route::get('/tokens/{id}', function ($id) {
// 	return Inertia::render('Site/SingleToken', [ 'id' => $id ]);
// })->name('singleToken');

// Route::get('/episodes/{id}', function ($id) {
// 	return Inertia::render('Site/SingleEpisode', [ 'id' => $id ]);
// })->name('singleEpisode');