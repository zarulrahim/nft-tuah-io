<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="max-w-2xl mx-auto">
                    <a href="/admin/movies">
                        <div class="mb-6 btn">Back to list</div>
                    </a>
                </div>
                <div class="max-w-2xl mx-auto bg-white shadow-lg rounded-lg p-10">
                    <form method="post" action="{{ route('storeMovie') }}" enctype="multipart/form-data">
                        @csrf
                        <div>
                            <label for="title" class="block font-medium text-sm text-gray-700">
                                <span>Title</span>
                            </label>    
                            <input type="text" name="title" id="title" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('title', '') }}" />
                            @error('title')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <label for="genre" class="block font-medium text-sm text-gray-700">
                                <span>Genre</span>
                            </label>    
                            <select id="genre" class="block mt-1 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm cursor-pointer" name="genre" required>
                                <option value="0">Please Select Genre</option>
                                @foreach ($genres as $genre)
                                    <option value="{{ $genre->id }}">{{ $genre->label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mt-4">
                            <label for="Summary" class="block font-medium text-sm text-gray-700">
                                <span>Summary</span>
                            </label>    
                            <input type="text" name="summary" id="summary" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('summary', '') }}" />
                            @error('summary')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <label for="Duration" class="block font-medium text-sm text-gray-700">
                                <span>Duration</span>
                            </label>    
                            <input type="text" name="duration" id="duration" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('duration', '') }}" />
                            @error('duration')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <label for="release_date" class="block font-medium text-sm text-gray-700">
                                <span>Release Date</span>
                            </label>    
                            <input class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm" type="date" id="release_date" name="release_date">
                        </div>
                        <div class="mt-4">
                            <label for="poster" class="block font-medium text-sm text-gray-700">
                                <span>Poster</span>
                            </label>    
                            <input class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm" type="file" id="poster" name="poster">
                        </div>
                        <div class="mt-4">
                            <label for="transcode_media" class="block font-medium text-sm text-gray-700">
                                <span>Video</span>
                            </label>    
                            <input class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm" type="file" id="transcode_media" name="transcode_media">
                        </div>
                        <div class="flex items-center justify-end mt-4">
			                <button class="btn">
			                    Save & Upload Video
                            </button>
			            </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>