<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="max-w-2xl mx-auto">
                    <a href="/admin/shows/{{ $show_id }}">
                        <div class="mb-6 btn">Back</div>
                    </a>
                </div>
                <div class="max-w-2xl mx-auto bg-white shadow-lg rounded-lg p-10">
                    <form method="post" action="{{ route('storeEpisode') }}" enctype="multipart/form-data">
                        @csrf
                        <div>
                            <input type="hidden" value="{{ $show_id }}" name="show_id" />
                            <input type="hidden" value="{{ $season_id }}" name="season_id" />
                            <label for="title" class="block font-medium text-sm text-gray-700">
                                <span>Title</span>
                            </label>    
                            <input type="text" name="title" id="title" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('title', '') }}" />
                            @error('title')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <label for="Summary" class="block font-medium text-sm text-gray-700">
                                <span>Summary</span>
                            </label>    
                            <input type="text" name="summary" id="summary" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('summary', '') }}" />
                            @error('summary')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <label for="duration" class="block font-medium text-sm text-gray-700">
                                <span>Duration</span>
                            </label>    
                            <input type="text" name="duration" id="duration" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('duration', '') }}" />
                            @error('duration')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <label for="airdate" class="block font-medium text-sm text-gray-700">
                                <span>Airing Date</span>
                            </label>    
                            <input type="date" name="airdate" id="airdate" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('airdate', '') }}" />
                            @error('airdate')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <label for="poster" class="block font-medium text-sm text-gray-700">
                                <span>Poster</span>
                            </label>    
                            <input type="file" name="poster" id="poster" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('poster', '') }}" />
                            @error('poster')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mt-4">
                            <label for="transcode_media" class="block font-medium text-sm text-gray-700">
                                <span>Video</span>
                            </label>    
                            <input type="file" name="transcode_media" id="transcode_media" type="text" class="mt-2 w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                                   value="{{ old('transcode_media', '') }}" />
                            @error('transcode_media')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="flex items-center justify-end mt-4">
			                <button class="btn">
			                    Save & Upload Video
                            </button>
			            </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>