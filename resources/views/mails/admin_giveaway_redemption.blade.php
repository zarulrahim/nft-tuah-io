@component('mail::message')
# Hi, {{ $redemption->giveaway->user->name }}

You have received 1 new redemption on **{{ $redemption->giveaway->title }}** giveaway from **{{ $redemption->name }}**!

This is the redemption code, **{{ $redemption->code }}**

Please process the redemption accordingly and avoid them waiting for too long!
@component('mail::button', ['url' => 'https://tuah.io/' . $redemption->giveaway->user->subdomain . '/g/' . $redemption->giveaway->id ])
Go to Giveaway
@endcomponent
@component('mail::button', ['url' => 'https://tuah.io/dashboard/redemptions' ])
Process Redemption
@endcomponent

Thank you,
{{ config('app.name') }}
@endcomponent