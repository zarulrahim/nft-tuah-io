@component('mail::message')
# Hi, {{ $redemption->name }}

@if ($redemption->gift->gift_type_id === 1)
  We have received your redemption on **{{ $redemption->giveaway->title }}**!
@else
  Congratulations! 

  You got **{{ $redemption->redemption_gift_detail->nft_collection->title  }}** from **{{ $redemption->giveaway->title  }}**!

  ![{{$redemption->redemption_gift_detail->nft_collection->title}}]({{$redemption->redemption_gift_detail->nft_collection->stream_path}})
@endif

  This is your redemption code, **{{ $redemption->code }}**

Please keep it safely and **{{ $redemption->giveaway->user->name }}** will process your redemption accordingly.
@component('mail::button', ['url' => 'https://tuah.io/' . $redemption->giveaway->user->subdomain . '/g/' . $redemption->giveaway->id ])
Go to Giveaway
@endcomponent

Thank you,
{{ config('app.name') }}
@endcomponent