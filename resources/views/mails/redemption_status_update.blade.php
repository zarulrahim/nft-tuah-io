@component('mail::message')
# Hi, {{ $redemption->name }}

Your redemption **#{{ $redemption->code }}** on **{{ $redemption->giveaway->title }}** giveaway has been marked as **Completed** by **{{ $redemption->giveaway->user->name }}** (creator).

> {{ $message }}

If you have any enquiries regarding the redemption, please directly contact **{{ $redemption->giveaway->user->name }}** (creator).

@component('mail::button', ['url' => 'https://tuah.io/' . $redemption->giveaway->user->subdomain . '/g/' . $redemption->giveaway->id ])
Go to Giveaway
@endcomponent

Thanks,
{{ config('app.name') }}
@endcomponent