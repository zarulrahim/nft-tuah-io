import { createStore } from 'vuex'
import Web3 from 'web3/dist/web3.min.js'

const BASE_URL = 'https://api.pentas.io/'
const store = createStore({
  state: {
    // ownerAddress: "",
    nfts: [],
    // ownerProfile: [],
    // ownerNfts: [],
    // tempSingleToken: null,
    // tempSingleTokenInvalid: false,
    nftsGiveaway: [],
  },
  mutations: {
    // increment (state) {
    //   state.count++
    // },
    setNfts (state, data) {
      state.nfts = data
    },
    // setOwnerProfile (state, data) {
    //   state.ownerProfile = data
    // },
    // setOwnerNfts (state, data) {
    //   state.ownerNfts = data
    // },
    // sortNfts (state, type) {
    //   if (type === 'latest') {
    //     state.ownerNfts = state.ownerNfts.sort((a,b) => new Date(b.created) - new Date(a.created))
    //   } else {
    //     state.ownerNfts = state.ownerNfts.sort((a,b) => new Date(a.created) - new Date(b.created))
    //   }
    // },
    // setTempSingleToken (state, data)  {
    //   state.tempSingleToken = data
    // },
    // setTempSingleTokenInvalid (state, data) {
    //   state.tempSingleTokenInvalid = data
    // },
    setNftsGiveaway (state, data) {
      state.nftsGiveaway = data
    }
  },
  getters: {
    // ownerAddress: (state) => {
    //   return state.ownerAddress
    // },
    nfts: (state) => {
      return state.nfts
    },
    // ownerProfile: (state) => {
    //   return state.ownerProfile
    // },
    // ownerNfts: (state) => {
    //   return state.ownerNfts
    // },
    // nftsCount: (state) => {
    //   return state.nfts.length
    // },
    // tempSingleToken: (state) => {
    //   return JSON.parse(JSON.stringify(state.tempSingleToken))
    // },
    // tempSingleTokenInvalid: (state) => {
    //   return state.tempSingleTokenInvalid
    // },
    nftsGiveaway: (state) => {
      return state.nftsGiveaway
    }
  },
  actions: {
    async getNfts ({ commit, getters }, payload) {
      let array = []
      let newArray = []
      let startPage = payload.getType === 'get_all' ? 1 : payload.page
      let sortingType = "latest"
      let filter = ""

      if (payload.getType === "get_all") {
        do {
          const response = await axios.get("/api/nfts/" + payload.eth_address + "?filter=" + filter + "&page=" + startPage + "&sorting=" + sortingType)
          startPage++;
          newArray = response.data.slice();
          array.push(...newArray)
        } while (newArray.length === 8);
        commit('setNfts', array);
        // console.log("Check Response getNfts ===> ", array, newArray.length)
      } else {
        // do {
          const response = await axios.get("/api/nfts/" + payload.eth_address + "?filter=" + filter + "&page=" + startPage + "&sorting=" + sortingType)
          // startPage++;
          // newArray = response.data.slice();
          getters.nfts.push(...response.data.slice())
        // } while (newArray.length === 8);
        commit('setNfts', getters.nfts);
        // console.log("Check Response getNfts ===> ", array, newArray.length)
      }
    },

    async getNftsGiveaway ({ commit, getters }, payload) {
      try {
        const response = await axios.get("/api/nfts/" + payload.subdomain + "/giveaways/" + payload.id)
        commit('setNftsGiveaway', response.data)
        console.log("getNftsGiveaway response ===> ", response.data)
      } catch (err) {
        console.log("getNftsGiveaway error ===> ", err)
      }
    },

    // async getOwnerProfile ({ commit, getters }) {
    //   try {
    //     // Get Owner Profile
    //     const getProfile = await axios.get("/api/owner_profile/" + getters.ownerAddress)
    //     // Get Owner Analytics
    //     // const getAnalytic = await axios.get(BASE_URL + "analytics/" + getters.ownerAddress)
    //     // Combine all info
    //     let ownerProfile = {
    //       ...getProfile.data,
    //       // ...getAnalytic.data,
    //     }
    //     commit('setOwnerProfile', ownerProfile)
    //     console.log("getOwnerProfile response ===> ", ownerProfile)
    //     if (getProfile.data) {
    //       // Get Owner NFTS
    //       let array = []
    //       let newArray = []
    //       let startPage = 1
    //       let sortingType = "latest"
    //       let filter = ""
    //       do {
    //         const getNfts = await axios.get("/api/nfts/" + getters.ownerAddress + "?filter=" + filter + "&page=" + startPage + "&sorting=" + sortingType)
    //         // const getNfts = await axios.get(BASE_URL + "user/" + getters.ownerAddress + "/nft/?page=" + startPage + "&sorting=latest")
    //         startPage++;
    //         newArray = getNfts.data.slice();
    //         array.push(...newArray)
    //       } while (newArray.length === 6);
    //       commit('setOwnerNfts', array)
    //       console.log("getOwnerProfile ownerNfts response ===> ", array)
    //     }
    //   } catch (err) {
    //     console.log("getOwnerProfile error ===> ", err)
    //   }
    // },

    // async getSingleToken ({ commit, getters }, payload) {
    //   try {
    //     // const response = await axios.get(BASE_URL + 'nft/' + payload.id)
    //     const response = await axios.get('/api/nft/' + payload.id)
    //     const minterAddress = Web3.utils.toChecksumAddress(response.data.minter)
    //     const ownerAddress = Web3.utils.toChecksumAddress(getters?.ownerAddress)
    //     const foundToken = response.data.minter === ownerAddress
    //     if (foundToken) {
    //       const respTransaction = await axios.get('/api/nft/' + payload.id + 'transaction')
    //       let combinedData = { ...response.data, transactions: respTransaction.data }
    //       commit('setTempSingleToken', combinedData)
    //       commit('setTempSingleTokenInvalid', false)
    //       console.log("Check response getToken ===> ", getters?.tempSingleToken);
    //     } else {
    //       commit('setTempSingleToken', null)
    //       commit('setTempSingleTokenInvalid', true)
    //       // console.log("Check invalid response getToken ===> ", this.data, this.dataInvalid);
    //     }
    //   } catch (err) {
    //     commit('setTempSingleTokenInvalid', true)
    //     console.log("Check error getToken ===> ", err)
    //   }
    // },
  }
})

export default store;