<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'subdomain'      => 'konfederasi',
                'eth_address'    => "0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0",
                'api_token'      =>  hash('sha256', Str::random(60)),
            ],
            [
                'id'             => 2,
                'name'           => 'pelempunk.nft',
                'email'          => 'pelempunk.nft@mail.com',
                'subdomain'      => 'pelempunk',
                'eth_address'    => "0x87b463374Ee072DaDD19D5Eb67878c93e9757352",
                'api_token'      =>  hash('sha256', Str::random(60)),
            ],
        ];

        User::insert($users);
    }
}