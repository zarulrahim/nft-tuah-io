<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GiftUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gifts = array(
            [
                "title" => 'Tumbler',
                "remark" => 'Exclusive New Year Design',
                "amount" => 95.5,
                "image_url" => 'https://starbucks.com.my/images/merchandise/11128492_LINE-FRIENDS-Brown-and-Friends-Stainless-Steel-Tumbler-(12oz).png'
            ],
            [
                "title" => 'Snapback',
                "remark" => 'Exclusive New Year Design',
                "amount" => 150.5,
                "image_url" => 'https://www.blackskies.shop/media/image/7f/f6/e6/Blackskies-Port-Venice-Snapback-Cap-2_600x600.jpg'
            ]
        );
        $admin = \App\Models\User::find(1);
        $admin->gifts()->createMany($gifts);
    }
}
