<?php

namespace Database\Seeders;

use App\Models\Giveaway;
use App\Models\EventGift;
use App\Models\EventNft;
use Illuminate\Database\Seeder;

class GiveawayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new_giveaway = [
            "title" => "New Year Giveaway #001",
            "remark" => "Collect 3 or 1, follow our twitter, and redeem the rewards in tuah.io",
            "image_url" => "https://thumbs.dreamstime.com/b/giveaway-banner-post-template-win-prize-social-media-poster-vector-design-illustration-175072061.jpg",
            "start_date" => "2022-01-01",
            "end_date" => "2022-01-30",
            "user_id" => 1
        ];
        $giveaway = Giveaway::create($new_giveaway);
        if ($giveaway) {
            $new_event_nfts = [
                [
                    'nft_id' => 146170,
                    'giveaway_id' => $giveaway->id
                ],
                [
                    'nft_id' => 164318,
                    'giveaway_id' => $giveaway->id
                ],
                [
                    'nft_id' => 164317,
                    'giveaway_id' => $giveaway->id
                ],
            ];
            EventNft::insert($new_event_nfts);
            $new_event_gifts = [
                [
                    'redeem_quantity' => 10,
                    'hold_amount' => 1,
                    'gift_id' => 1,
                    'giveaway_id' => $giveaway->id
                ],
                [
                    'redeem_quantity' => 10,
                    'hold_amount' => 3,
                    'gift_id' => 2,
                    'giveaway_id' => $giveaway->id
                ],
            ];
            EventGift::insert($new_event_gifts);
        }
    
    }
}
