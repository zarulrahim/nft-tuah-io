<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'admin_access',
            ],
            [
                'id'    => 2,
                'title' => 'user_basic_access',
            ],
            [
                'id'    => 3,
                'title' => 'user_pro_access',
            ],
            [
                'id'    => 4,
                'title' => 'user_adv_access',
            ],
        ];

        Permission::insert($permissions);
    }
}