<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SubscriptionPackage;
use App\Models\Subscription;
use App\Models\User;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscription_package = SubscriptionPackage::create([
            "title" => "Basic Plan",
            "remark" => "Use anywhere, anytime",
            "rate_per_month" => 15
        ]);
        $subscription_package->save();
        // $user = User::find(1);
        // $new_subscription = $user->subscriptions()->createMany(array([
        //     "subscription_package_id" => $subscription_package->id,
        //     "status" => "active",
        //     "validity_in_days" => 9999
        // ]));
    }
}
