<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedemptionStatusDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redemption_status_detail', function (Blueprint $table) {
            $table->id();
            $table->foreignId('redemption_id')->nullable();
            $table->string('message')->nullable();
            $table->string('status')->nullable();
            $table->boolean('is_current')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redemption_status_detail');
    }
}
