<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_gifts', function (Blueprint $table) {
            $table->id();
            $table->integer('redeem_quantity')->nullable();
            $table->integer('hold_amount')->nullable();
            $table->foreignId('gift_id')->nullable();
            $table->foreignId('campaign_id')->nullable();
            $table->foreignId('giveaway_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_gifts');
    }
}
