<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedemptionGiftDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redemption_gift_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('redemption_id')->nullable();
            $table->foreignId('nft_collection_id')->nullable();
            $table->foreignId('nft_collection_group_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redemption_gift_details');
    }
}
