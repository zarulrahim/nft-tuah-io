<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRedemptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redemptions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('twitter_username')->nullable();
            $table->string('code')->unique()->nullable();
            $table->string('status')->nullable()->default(null);
            $table->string('eth_address')->nullable();
            $table->foreignId('giveaway_id')->nullable();
            $table->foreignId('campaign_id')->nullable();
            $table->foreignId('gift_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redemptions');
    }
}
