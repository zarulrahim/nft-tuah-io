<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNftCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nft_collections', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('remark')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency')->nullable();
            $table->string('marketplace_url')->nullable();
            $table->string('external_url')->nullable();
            $table->string('image_url')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nft_collections');
    }
}
