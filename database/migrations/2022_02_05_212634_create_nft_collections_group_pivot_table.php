<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNftCollectionsGroupPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nft_collections_group_pivot', function (Blueprint $table) {
            $table->foreignId('nft_collection_id')->references('id')->on('nft_collections')->cascadeOnDelete();
            $table->foreignId('nft_collection_group_id')->references('id')->on('nft_collection_groups')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nft_collections_group_pivot');
    }
}
