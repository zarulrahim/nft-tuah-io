<?php

namespace App\Actions;

use App\Models\User;
use App\Models\Redemption;
use App\Models\Giveaway;
use Illuminate\Http\Request;
use Elliptic\EC;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use kornrunner\Keccak;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\PentasApiController;

class RedeemUsingWeb3
{
  public function __invoke(Request $request)
  {
    if (! $this->authenticate($request)) {
      throw ValidationException::withMessages([
        'signature' => 'Invalid signature.'
      ]);
    }

    // Request will have -> eth_address, giveaway_id, gift_id
    $redemption = Redemption::where('eth_address', "0x5511142db2e5E873c5CD00E74c189f251A2ca4F1")->exists();
    if (!$redemption) {
      $giveaway = Giveaway::find($request->giveaway_id);
      // get assigned NFTs from giveaway
      $event_nfts = $giveaway->event_nfts;
      // get gift condition e.g requires 3 nfts or 1 nft
      $event_gift = $giveaway->event_gifts->find($request->gift_id);
      if ($event_gift->redeem_quantity > 0) {
        // get user's owned nfts from Pentas API
        $user_pentas_collected_nfts = [];
        $total_pages = ceil(json_decode(collect(PentasApiController::owner_profile("0x5511142db2e5E873c5CD00E74c189f251A2ca4F1")))->mintCount / 8);
        for ($current_page = 0; $current_page < $total_pages; $current_page++) {
          array_push($user_pentas_collected_nfts, PentasApiController::pullNfts($current_page, "collected", "latest", "0x5511142db2e5E873c5CD00E74c189f251A2ca4F1"));
        };
        if (collect($user_pentas_collected_nfts)->count() > 0) {
          // create new redemption
          if ($giveaway != null && $event_gift != null) {
            // filtered fetched pentas nfts based on event_nfts
            $filtered = [];
            foreach($event_nfts as $event_nft) {
              array_push($filtered, collect($user_pentas_collected_nfts)->flatten()->where('scTokenId', $event_nft->nft_id)->first());
            };
            if (collect($filtered)->filter()->count() > 0 && collect($filtered)->filter()->count() >= $event_gift->hold_amount) {
              $newRedemption = Redemption::firstOrCreate([
                'eth_address' => "0x5511142db2e5E873c5CD00E74c189f251A2ca4F1",
                'giveaway_id' => $giveaway->id,
                'gift_id' => $event_gift->id
              ]);
              $newRedemption->save();
              return Redirect::route('home')->with('message', "You have susccesfully redeemed the reward. Kindly check your email *******97@gmail.com to track the reward sending progress.");
            } else {
              return Redirect::route('home')->with('message', "redeem invalid, user don't have enough nfts");
              // redeem invalid, user don't have enough nfts
            }
          } else {
            return Redirect::route('home')->with('message', "giveaway or event_gift not exists");
            // $giveaway or $event_gift not exists
          }
        } else {
          return $user_pentas_collected_nfts;
          // return Redirect::route('home')->with('message', "user_pentas_collected_nfts invalid fetching");
          // $user_pentas_collected_nfts invalid fetching
        }
      } else {
        return Redirect::route('home')->with('message', "event_gift out of redeem_quantity");
        // $event_gift out of redeem_quantity 
      }
    } else {
      return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Reward already redeemed" ]);
      // return Redirect::back([ "error" => "The reward is already redeemed"  ]);
      // return Redirect::route('home')->with('message', "redemption is exist, you cannot redeem twice");
      // $redemption is exist, you cannot redeem twice
    }
  }

  protected function authenticate(Request $request): bool
  {
    return $this->verifySignature(
      $request->message,
      $request->signature,
      $request->eth_address,
    );
  }

  protected function verifySignature($message, $signature, $address): bool
  {
    $messageLength = strlen($message);
    $hash = Keccak::hash("\x19Ethereum Signed Message:\n{$messageLength}{$message}", 256);
    $sign = [
      "r" => substr($signature, 2, 64),
      "s" => substr($signature, 66, 64)
    ];

    $recId  = ord(hex2bin(substr($signature, 130, 2))) - 27;

    if ($recId != ($recId & 1)) {
      return false;
    }

    $publicKey = (new EC('secp256k1'))->recoverPubKey($hash, $sign, $recId);

    return $this->pubKeyToAddress($publicKey) === Str::lower($address);
  }

  protected function pubKeyToAddress($publicKey): string
  {
    return "0x" . substr(Keccak::hash(substr(hex2bin($publicKey->encode("hex")), 1), 256), 24);
  }
}