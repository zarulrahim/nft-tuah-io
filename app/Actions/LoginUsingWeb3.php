<?php

namespace App\Actions;

use App\Models\User;
use Illuminate\Http\Request;
use Elliptic\EC;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use kornrunner\Keccak;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class LoginUsingWeb3
{	
		// public static function getUserProfile($walletAddress) {
		// 	$response = Http::get("https://api.pentas.io/user/{$walletAddress}");
		// 	if ($response) {
		// 		return $response->json();
		// 	} else {
		// 		return null;
		// 	}
  //   }

  //   public static function getUserNfts($walletAddress) {
  //   	$response = Http::get("https://api.pentas.io/user/{$walletAddress}/nft/collected/?page=1&sorting=latest");
  //   	if ($response) {
  //   		return $response->json();
  //   	} else {
  //   		return null;
  //   	}
  //   }

    public function __invoke(Request $request)
    {
        if (! $this->authenticate($request)) {
            throw ValidationException::withMessages([
                'signature' => 'Invalid signature.'
            ]);
        }

        $user = User::where('eth_address', $request->address)->exists();
        // $profile = $this->getUserProfile($request->address);
      	// $nfts = $this->getUserNfts($request->address);
        // $profile = Http::get("https://api.pentas.io/user/{$request->address}")->json();

        if ($user) {
        	Auth::login(User::where('eth_address', $request->address)->first());	
        } else {
        	// $login = Auth::login(User::firstOrCreate([
	          // 'name' => $profile['name'],
	          // 'profile_photo_path' => $profile['profileImage'],
	          // 'eth_address' => $request->address,
	        // ]));
        	// $profile = $this->getUserProfile($request->address);
        	// $nfts = $this->getUserNfts($request->address);
        	// if ($profile->successful()) {
        	if ($request->address === "0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0") {
                $newUser = User::firstOrCreate([ 'name' => $request->address, 'eth_address' => $request->address ]);
                if ($newUser) {
                    $newUser->roles()->sync(1);
                }
            } else {
                $newUser = User::firstOrCreate([ 'name' => $request->address, 'eth_address' => $request->address ]);
                if ($newUser) {
                    $newUser->roles()->sync(2);
                }            }
            Auth::login($newUser);
        		// if ($login) {
        		// 	$records = [];
        		// 	$nfts->each(function ($item, $key) use (&$records) {
        		// 		$record = [
        		// 			'name' => $item->name,
        		// 		];
        		// 		$records[] = $record;
        		// 	});
        		// 	$create = $user->nfts()->saveMany($records);
        		// 	if ($create) {
        		// 		return Redirect::route('dashboard');
        		// 	}
        		// };
        };
      	return Redirect::route('home');
    }

    protected function authenticate(Request $request): bool
    {
        return $this->verifySignature(
            $request->message,
            $request->signature,
            $request->address,
        );
    }

    protected function verifySignature($message, $signature, $address): bool
    {
        $messageLength = strlen($message);
        $hash = Keccak::hash("\x19Ethereum Signed Message:\n{$messageLength}{$message}", 256);
        $sign = [
            "r" => substr($signature, 2, 64),
            "s" => substr($signature, 66, 64)
        ];

        $recId  = ord(hex2bin(substr($signature, 130, 2))) - 27;

        if ($recId != ($recId & 1)) {
            return false;
        }

        $publicKey = (new EC('secp256k1'))->recoverPubKey($hash, $sign, $recId);

        return $this->pubKeyToAddress($publicKey) === Str::lower($address);
    }

    protected function pubKeyToAddress($publicKey): string
    {
        return "0x" . substr(Keccak::hash(substr(hex2bin($publicKey->encode("hex")), 1), 256), 24);
    }
}