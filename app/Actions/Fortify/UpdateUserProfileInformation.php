<?php

namespace App\Actions\Fortify;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * Validate and update the given user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    public function update($user, array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],

            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
        ])->validateWithBag('updateProfileInformation');

        if ($input['email'] !== $user->email &&
            $user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser($user, $input);
        } else {
            // Upload Image
            if ($input['photo'] !== null) {
                Storage::disk('digitalocean')->delete($user->profile_photo_path);
                $directory = "images/" . Auth::user()->subdomain . "/general";
                $raw = $input['photo'];
                $extension = explode('/', explode(':', substr($raw, 0, strpos($raw, ';')))[1])[1];
                $fileName = 'tuah-io-profile-img-' . Auth::user()->subdomain . '-' . time() . Str::random(6) . '.' . $extension;
                $image = \Image::make($raw);
                $image->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::disk('digitalocean')->put($directory . '/' . $fileName, $image->stream(), 'public');
                $user->forceFill([
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'profile_photo_path' => $directory . '/' . $fileName
                ])->save();
                // $directory = "images";
                // $raw = $input['photo'];
                // $extension = explode('/', explode(':', substr($raw, 0, strpos($raw, ';')))[1])[1];
                // $fileName = 'tuah-io-profile-' .  Str::replace(" ", "-", Str::lower($input['name'])) . '-' . time() . Str::random(6) . '.' . $extension;
                // Storage::disk('digitalocean')->put($directory . '/' . $fileName, \Image::make($raw)->stream(), 'public');
                // $user->forceFill([
                //     'name' => $input['name'],
                //     'email' => $input['email'],
                //     'profile_photo_path' => $fileName
                // ])->save();
            } else {
                $user->forceFill([
                    'name' => $input['name'],
                    'email' => $input['email'],
                ])->save();
            }
        }
    }

    /**
     * Update the given verified user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    protected function updateVerifiedUser($user, array $input)
    {
        $user->forceFill([
            'name' => $input['name'],
            'email' => $input['email'],
            'email_verified_at' => null,
        ])->save();

        $user->sendEmailVerificationNotification();
    }
}
