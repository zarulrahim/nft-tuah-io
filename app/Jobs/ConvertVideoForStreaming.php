<?php

namespace App\Jobs;

use App\Models\TranscodeMedia;
use Carbon\Carbon;
use FFMpeg;
use FFMpeg\Format\Video\X264;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ConvertVideoForStreaming implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $transcode_media;

    public function __construct(TranscodeMedia $transcode_media)
    {
        $this->transcode_media = $transcode_media;
    }

    public function handle()
    {
        // create some video formats...
        $lowBitrateFormat  = (new X264)->setKiloBitrate(500);
        $midBitrateFormat  = (new X264)->setKiloBitrate(1500);
        $highBitrateFormat = (new X264)->setKiloBitrate(3000);

        FFMpeg::fromDisk($this->transcode_media->disk)
        // ->open('yesterday.mp3')
        ->exportForHLS()
        ->toDisk('digitalocean')
        ->addFormat($lowBitrateFormat)
        ->addFormat($midBitrateFormat)
        ->addFormat($highBitrateFormat)
        ->save('myvideo.m3u8');

        // open the uploaded video from the right disk...
        // FFMpeg::fromDisk($this->transcode_media->disk)
        //     ->open($this->transcode_media->path)

        // // call the 'exportForHLS' method and specify the disk to which we want to export...
        //     ->exportForHLS()
        //     ->toDisk('digitalocean')

        // // we'll add different formats so the stream will play smoothly
        // // with all kinds of internet connections...
        //     ->addFormat($lowBitrateFormat)
        //     ->addFormat($midBitrateFormat)
        //     ->addFormat($highBitrateFormat)

        // // call the 'save' method with a filename...
        //     ->save($this->transcode_media->id . '.m3u8');

        // update the database so we know the convertion is done!
        $this->transcode_media->update([
            'converted_for_streaming_at' => Carbon::now(),
        ]);
    }
}