<?php

namespace App\Observers;

use Illuminate\Support\Facades\Storage;
use App\Models\NftCollection;

class NftCollectionObserver
{
    /**
     * Handle the NftCollection "created" event.
     *
     * @param  \App\Models\NftCollection  $nftCollection
     * @return void
     */
    public function created(NftCollection $nftCollection)
    {
        //
    }

    /**
     * Handle the NftCollection "updated" event.
     *
     * @param  \App\Models\NftCollection  $nftCollection
     * @return void
     */
    public function updated(NftCollection $nftCollection)
    {
        //
    }

    // public function deleting(NftCollection $nftCollection)
    // {
    //     // Deleting image from storage digitalocean
    //     // Storage::disk('digitalocean')->delete($nftCollection->image_url);
    // }

    /**
     * Handle the NftCollection "deleted" event.
     *
     * @param  \App\Models\NftCollection  $nftCollection
     * @return void
     */
    public function deleted(NftCollection $nftCollection)
    {
        //
    }

    /**
     * Handle the NftCollection "restored" event.
     *
     * @param  \App\Models\NftCollection  $nftCollection
     * @return void
     */
    public function restored(NftCollection $nftCollection)
    {
        //
    }

    /**
     * Handle the NftCollection "force deleted" event.
     *
     * @param  \App\Models\NftCollection  $nftCollection
     * @return void
     */
    public function forceDeleted(NftCollection $nftCollection)
    {
        //
    }
}
