<?php

namespace App\Observers;

use Illuminate\Support\Facades\Storage;
use App\Models\Giveaway;

class GiveawayObserver
{
    /**
     * Handle the Giveaway "created" event.
     *
     * @param  \App\Models\Giveaway  $giveaway
     * @return void
     */
    public function created(Giveaway $giveaway)
    {
        //
    }

    public function updating(Giveaway $giveaway)
    {
        //
    }

    /**
     * Handle the Giveaway "updated" event.
     *
     * @param  \App\Models\Giveaway  $giveaway
     * @return void
     */
    public function updated(Giveaway $giveaway)
    {
        //
    }

    public function deleting(Giveaway $giveaway)
    {
        // Deleting event_nfts
        $giveaway->event_nfts()->delete();
        // Deleting event_gifts
        $giveaway->event_gifts()->delete();
        // Deleting event_custom_addresses
        $giveaway->event_custom_addresses()->delete();
        // Deleting redemptions
        $giveaway->redemptions()->delete();
        // Deleting image from storage digitalocean
        Storage::disk('digitalocean')->delete($giveaway->image_url);
    }

    /**
     * Handle the Giveaway "deleted" event.
     *
     * @param  \App\Models\Giveaway  $giveaway
     * @return void
     */
    public function deleted(Giveaway $giveaway)
    {
        //
    }

    /**
     * Handle the Giveaway "restored" event.
     *
     * @param  \App\Models\Giveaway  $giveaway
     * @return void
     */
    public function restored(Giveaway $giveaway)
    {
        //
    }

    /**
     * Handle the Giveaway "force deleted" event.
     *
     * @param  \App\Models\Giveaway  $giveaway
     * @return void
     */
    public function forceDeleted(Giveaway $giveaway)
    {
        //
    }
}
