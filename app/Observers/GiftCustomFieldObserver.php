<?php

namespace App\Observers;

use App\Models\GiftCustomField;

class GiftCustomFieldObserver
{
    /**
     * Handle the GiftCustomField "created" event.
     *
     * @param  \App\Models\GiftCustomField  $giftCustomField
     * @return void
     */
    public function created(GiftCustomField $giftCustomField)
    {
        //
    }

    /**
     * Handle the GiftCustomField "updated" event.
     *
     * @param  \App\Models\GiftCustomField  $giftCustomField
     * @return void
     */
    public function updated(GiftCustomField $giftCustomField)
    {
        //
    }

    public function deleting(GiftCustomField $giftCustomField)
    {
        // Deleting gift_custom_field_data
        $giftCustomField->gift_custom_field_datas()->delete();
    }

    /**
     * Handle the GiftCustomField "deleted" event.
     *
     * @param  \App\Models\GiftCustomField  $giftCustomField
     * @return void
     */
    public function deleted(GiftCustomField $giftCustomField)
    {
        //
    }

    /**
     * Handle the GiftCustomField "restored" event.
     *
     * @param  \App\Models\GiftCustomField  $giftCustomField
     * @return void
     */
    public function restored(GiftCustomField $giftCustomField)
    {
        //
    }

    /**
     * Handle the GiftCustomField "force deleted" event.
     *
     * @param  \App\Models\GiftCustomField  $giftCustomField
     * @return void
     */
    public function forceDeleted(GiftCustomField $giftCustomField)
    {
        //
    }
}
