<?php

namespace App\Observers;

use Illuminate\Support\Facades\Storage;
use App\Models\Gift;

class GiftObserver
{
    /**
     * Handle the Gift "created" event.
     *
     * @param  \App\Models\Gift  $gift
     * @return void
     */
    public function created(Gift $gift)
    {
        //
    }

    /**
     * Handle the Gift "updated" event.
     *
     * @param  \App\Models\Gift  $gift
     * @return void
     */
    public function updated(Gift $gift)
    {
        //
    }

    public function deleting(Gift $gift)
    {
        // Deleting event_gifts
        $gift->event_gifts()->delete();
        // Deleting gift_custom_fields
        $gift->gift_custom_fields()->delete();
        // Deleting image from storage digitalocean
        Storage::disk('digitalocean')->delete($gift->image_url);
    }

    /**
     * Handle the Gift "deleted" event.
     *
     * @param  \App\Models\Gift  $gift
     * @return void
     */
    public function deleted(Gift $gift)
    {
        //
    }

    /**
     * Handle the Gift "restored" event.
     *
     * @param  \App\Models\Gift  $gift
     * @return void
     */
    public function restored(Gift $gift)
    {
        //
    }

    /**
     * Handle the Gift "force deleted" event.
     *
     * @param  \App\Models\Gift  $gift
     * @return void
     */
    public function forceDeleted(Gift $gift)
    {
        //
    }
}
