<?php

namespace App\Observers;

use Illuminate\Support\Facades\Storage;
use App\Models\User;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        if (!$user->profile_photo_path) {
            $user->profile_photo_path = "https://ui-avatars.com/api/?name=".$user->name;
            $user->save();
        }
    }

    public function updating(User $user)
    {
        if ($user->isDirty('profile_photo_path')) {
            // Deleting previous image
            Storage::disk('digitalocean')->delete('images/'.$user->getOriginal('profile_photo_path'));
        }
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
