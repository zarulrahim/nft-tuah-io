<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GiftCustomFieldData extends Model
{
    use HasFactory;

    protected $table = "gift_custom_field_datas";

    public function gift_custom_field()
    {
        return $this->belongsTo(GiftCustomField::class);
    }

    public function redemption()
    {
        return $this->belongsTo(Redemption::class);
    }

    protected $fillable = [
        'value',
        'gift_custom_field_id',
    ];
}
