<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventCustomAddress extends Model
{
    use HasFactory;

    protected $table = 'event_custom_addresses';

    public function campaign() {
        return $this->belongsTo(Campaign::class);
    }

    public function giveaway() {
        return $this->belongsTo(Giveaway::class);
    }
    
    protected $fillable = [
        'address',
        'giveaway_id',
    ];
}
