<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RedemptionGiftDetail extends Model
{
    use HasFactory;

    public function redemption()
    {
        return $this->belongsTo(Redemption::class);
    }

    public function nft_collection()
    {
        return $this->belongsTo(NftCollection::class);
    }

    public function nft_collection_group()
    {
        return $this->belongsTo(NftCollectionGroup::class);
    }

    protected $fillable = [
        "nft_collection_id",
        "nft_collection_group_id",
    ];
}
