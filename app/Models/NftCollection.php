<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NftCollection extends Model
{
    use HasFactory;

    protected $appends = [
        'stream_path'
    ];

    // public function nft_collection_group_pivots() {
    //     return $this->hasMany(NftCollectionGroupPivot::class);
    // }

    public function gifts() {
        return $this->hasMany(Gift::class);
    }

    public function nft_collection_groups() {
        return $this->belongsToMany(NftCollectionGroup::class);
    }

    public function getStreamPathAttribute() {
        return 'https://'.env('DIGITALOCEAN_SPACES_BUCKET').'.'.env('DIGITALOCEAN_SPACES_REGION').'.'.env('DIGITALOCEAN_SPACES_DOMAIN').'.com'.'/'.$this->image_url;
    }

    protected $fillable = [
        'title',
        'remark',
        'amount',
        'currency',
        'external_url',
        'marketplace_url',
        'image_url'
    ];
}
