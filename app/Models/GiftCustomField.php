<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GiftCustomField extends Model
{
    use HasFactory;

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }

    public function gift_custom_field_datas()
    {
        return $this->hasMany(GiftCustomFieldData::class);
    }

    protected $fillable = [
        'name',
        'type'
    ];
}
