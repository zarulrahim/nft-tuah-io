<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Giveaway extends Model
{
    use HasFactory;

    protected $appends = [
        'stream_path'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function event_gifts() {
        return $this->hasMany(EventGift::class)->with('gift', 'gift.nft_collection', 'gift.nft_collection_group');
    }

    public function event_nfts() {
        return $this->hasMany(EventNft::class);
    }

    public function event_custom_addresses() {
        return $this->hasMany(EventCustomAddress::class);
    }

    public function redemptions() {
        return $this->hasMany(Redemption::class)->with('gift', 'redemption_gift_detail', 'redemption_gift_detail.nft_collection', 'redemption_gift_detail.nft_collection_group');
    }

    public function getStreamPathAttribute() {
        return 'https://'.env('DIGITALOCEAN_SPACES_BUCKET').'.'.env('DIGITALOCEAN_SPACES_REGION').'.'.env('DIGITALOCEAN_SPACES_DOMAIN').'.com'.'/'.$this->image_url;
    }

    protected $fillable = [
        'title',
        'remark',
        'start_date',
        'end_date',
        'type',
    ];
}
