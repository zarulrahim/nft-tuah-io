<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventNft extends Model
{
    use HasFactory;
    protected $table = 'event_nfts';

    public function campaign() {
        return $this->belongsTo(Campaign::class);
    }

    public function giveaway() {
        return $this->belongsTo(Giveaway::class);
    }

    protected $fillable = [
        'nft_id',
        'giveaway_id',
        'campaign_id'
    ];
}
