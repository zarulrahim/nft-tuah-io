<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NftCollectionGroup extends Model
{
    use HasFactory;

    // public function nft_collection_group_pivots() {
    //     return $this->hasMany(NftCollectionGroupPivot::class);
    // }

    public function gifts() {
        return $this->hasMany(Gift::class);
    }

    public function nft_collections() {
        return $this->belongsToMany(NftCollection::class);
    }

    protected $fillable = [
        'title',
        'remark',
    ];
}
