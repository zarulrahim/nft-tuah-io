<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RedemptionStatusDetail extends Model
{
    use HasFactory;

    public $table = "redemption_status_detail";

    protected $fillable = [
        'status',
        'message',
        'is_current'
    ];

    public function redemption()
    {
        return $this->belongsTo(Redemption::class);
    }
}
