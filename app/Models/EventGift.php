<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventGift extends Model
{
    use HasFactory;

    protected $table = 'event_gifts';

    public function gift() {
        return $this->belongsTo(Gift::class);
    }

    public function campaign() {
        return $this->belongsTo(Campaign::class);
    }

    public function giveaway() {
        return $this->belongsTo(Giveaway::class);
    }

    protected $fillable = [
        'gift_id',
        'giveaway_id',
        'campaign_id',
        'hold_amount',
        'redeem_quantity'
    ];
}
