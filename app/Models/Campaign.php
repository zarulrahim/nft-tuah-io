<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use HasFactory;

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function event_gifts() {
        return $this->hasMany(EventGift::class);
    }

    public function event_nfts() {
        return $this->hasMany(EventNft::class);
    }

    protected $fillable = [
        'title',
        'remark',
        'start_date',
        'end_date',
        'target_amount'
    ];
}
