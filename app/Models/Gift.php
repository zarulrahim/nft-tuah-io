<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    use HasFactory;

    protected $appends = [
        'stream_path'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function event_gifts() {
        return $this->hasMany(EventGift::class);
    }

    // public function event_nfts() {
    //     return $this->hasMany(EventNft::class);
    // }

    public function gift_custom_fields()
    {
        return $this->hasMany(GiftCustomField::class);
    }

    public function nft_collection()
    {
        return $this->belongsTo(NftCollection::class);
    }

    public function nft_collection_group()
    {
        return $this->belongsTo(NftCollectionGroup::class)->with('nft_collections');
    }

    public function getStreamPathAttribute() {
        return $this-> nft_collection_group->nft_collections->first()->stream_path;
        // return 'https://'.env('DIGITALOCEAN_SPACES_BUCKET').'.'.env('DIGITALOCEAN_SPACES_REGION').'.'.env('DIGITALOCEAN_SPACES_DOMAIN').'.com'.'/'.$this->image_url;
    }

    protected $fillable = [
        'title',
        'remark',
        'amount',
        'gift_type_id',
        'image_url',
        'nft_collection_id',
        'nft_collection_group_id'
    ];
}
