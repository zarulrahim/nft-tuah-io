<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Redemption extends Model
{
    use HasFactory;

    public function redemption_gift_detail()
    {
        return $this->hasOne(RedemptionGiftDetail::class);
    }
    
    public function giveaway()
    {
        return $this->belongsTo(Giveaway::class);
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }

    public function gift_custom_field_datas() 
    {
        return $this->hasMany(GiftCustomFieldData::class);
    }

    public function redemption_status_details()
    {
        return $this->hasMany(RedemptionStatusDetail::class);
    }

    protected $fillable = [
        "eth_address",
        "name",
        "email",
        "twitter_username",
        "code",
        "status",
        "giveaway_id",
        "campaign_id",
        "gift_id",
    ];
}
