<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'eth_address',
        'name',
        'email',
        'subdomain',
        'status',
        // 'subscription_id'
        // 'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that shou ld be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'stream_path',
        'permission'
    ];

    private function isLink($url) {
        if ((strpos($url, "http://") !== false) || strpos($url, "https://") !== false) {
            return true;
        } else {
            return false;
        }
    } 

    public function getStreamPathAttribute() {
        if ($this->isLink($this->profile_photo_path)) {
            return $this->profile_photo_path;
        } else {
            return 'https://'.env('DIGITALOCEAN_SPACES_BUCKET').'.'.env('DIGITALOCEAN_SPACES_REGION').'.'.env('DIGITALOCEAN_SPACES_DOMAIN').'.com'.'/'.$this->profile_photo_path;
        }
    }

    public function getPermissionAttribute()
    {
        $roles = $this->roles;
        $array = [];

        foreach ($roles as $role) {
            foreach ($role->permissions as $permission) {
                array_push($array, $permission->title);
            }
        }

        return $array;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function giveaways()
    {
        return $this->hasMany(Giveaway::class);
    }

    public function gifts()
    {
        return $this->hasMany(Gift::class);
    }

    public function nft_collections()
    {
        return $this->hasMany(NftCollection::class);
    }

    public function nft_collection_groups()
    {
        return $this->hasMany(NftCollectionGroup::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function redemptions()
    {
        return $this->giveaways()->with('redemptions', 'redemptions.gift', 'redemptions.redemption_status_details')->get()->pluck('redemptions')->flatten();
    }
}
