<?php

namespace App\Helper;

class Helper
{
  public static function giftTypeList()
  {
      return [
          [ 
              "id" => 1,
              "title" => "Fixed Merchandise",
          ],
          [
              "id" => 2,
              "title" => "Fixed Airdrop",
          ],
          [
              "id" => 3,
              "title" => "Random Multi Airdrop"
          ]
      ];
  }
}