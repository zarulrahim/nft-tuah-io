<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Redemption;

class GiveawayRedemption extends Mailable
{
    use Queueable, SerializesModels;
    public $redemption;
    public $target;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Redemption $redemption, $target)
    {
        $this->redemption = $redemption;
        $this->target = $target;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->target === 'admin') {
            return $this->from('noreply@tuah.io', 'You Have 1 New Redemption @ '. $this->redemption->giveaway->title  .' - Tuah.io')->markdown('mails.admin_giveaway_redemption');
        } else if ($this->target === 'user') {
            return $this->from('noreply@tuah.io', 'Giveaway Redemption @ '. $this->redemption->giveaway->user->name  .' - Tuah.io')->markdown('mails.giveaway_redemption');
        }
        
    }
}