<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Redemption;

class RedemptionStatusUpdate extends Mailable
{
    use Queueable, SerializesModels;
    public $redemption;
    public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Redemption $redemption, $message)
    {
        $this->redemption = $redemption;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@tuah.io', 'Giveaway Redemption @ '. $this->redemption->giveaway->user->name  .' - Tuah.io')->markdown('mails.redemption_status_update');
    }
}