<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;
use Illuminate\Support\Facades\Session;
use Rinvex\Attributes\Models\Attribute;
use App\Models\Giveaway;
use App\Models\Gift;
use App\Models\GiftCustomField;
use App\Models\User;
use \App\Observers\UserObserver;
use \App\Observers\GiveawayObserver;
use \App\Observers\GiftObserver;
use \App\Observers\GiftCustomFieldObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Giveaway::observe(GiveawayObserver::class);
        Gift::observe(GiftObserver::class);
        GiftCustomField::observe(GiftCustomFieldObserver::class);
    }
}
