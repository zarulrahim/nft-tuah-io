<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\GiveawayRedemption;

class MailController extends Controller
{
    public function mail()
    {
        $redemption = \App\Models\Redemption::first();
        Mail::to('zarulizham97@gmail.com')->send(new GiveawayRedemption($redemption));
        return 'Email sent Successfully';
    }
}
