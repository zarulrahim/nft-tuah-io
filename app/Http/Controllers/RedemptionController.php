<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\RedemptionStatusUpdate;
use App\Helper\Helper;

class RedemptionController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $redemptions = Auth::user()->redemptions();
        return Inertia::render('Redemption/Index', [ "redemptions" => $redemptions, "giftTypeList" => Helper::giftTypeList() ]);
    }

    public function updateStatus(Request $request)
    {
        $redemption = Auth::user()->redemptions()->where("id", $request->id)->first();
        if ($redemption) {
            if ($redemption->redemption_status_details->last()->status !== "completed") {
                $redemption->redemption_status_details()->create([
                    'status' => 'completed',
                    'message' => $request->message,
                    'is_current' => true
                ]);
                Mail::to($redemption->email)->send(new RedemptionStatusUpdate($redemption, $request->message));
                return redirect('/dashboard/redemptions')
                ->with(
                    'message',
                    'Status redemption "'. $redemption->code . '"' . ' has succesfully updated!'
                );
                // return response()->json([ "status" => "success" ]);
            } else {
                return redirect('/dashboard/redemptions')
                ->with(
                    'message',
                    'Status redemption "'. $redemption->code . '"' . ' has failed to update!'
                );
            } 
        } else {
            return redirect('/dashboard/redemptions')
            ->with(
                'message',
                'Status redemption "'. $redemption->code . '"' . ' has failed to update!'
            );
            // return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Action is invalid" ]);
        }
    }

    public function show($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $redemption = Auth::user()->redemptions()->find($id);
        return Inertia::render('Redemption/Show', [ "redemption" => $redemption ]);
    }

    public function edit($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $redemption = Auth::user()->redemptions()->find($id);
        return Inertia::render('Redemption/Edit', [ "redemption" => $redemption ]);
    }
}
