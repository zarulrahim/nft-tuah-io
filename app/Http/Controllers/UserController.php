<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
  public function index()
  {
  	abort_if(Gate::denies('admin_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
 		$users = User::with('roles')->get();
 		return Inertia::render('User/Index', [
      'users' => $users      
    ]);
  }

  public function create()
  {
  	abort_if(Gate::denies('admin_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
  	$roles = Role::all();
  	return Inertia::render('User/Create', [
      'roles' => $roles,
    ]);
  }

  public function store(StoreUserRequest $request)
  {
  	$user = User::create($request->validated());
  	$user->roles()->sync($request->role);
		return Redirect::route('users')->with('message', 'User ' . $user->name . ' created');
  }

  public function show($id)
  {
  	abort_if(Gate::denies('admin_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
  	// $users = User::with(['roles'])->get();
  	$user = User::find($id);
  	return Inertia::render('User/Show', [
  		// 'users' => $users,
  		'user' => $user
  	]);
  }

  public function edit($id)
  {
  	abort_if(Gate::denies('admin_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
  	$roles = Role::all();
  	$user = User::find($id)->load('roles');
  	return Inertia::render('User/Edit', [
  		'user' => $user,
  		'roles' => $roles
  	]);
  }

  public function update(UpdateUserRequest $request, $id)
  {
  	abort_if(Gate::denies('admin_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
  	//
  }

  public function destroy($id)
  {
  	abort_if(Gate::denies('admin_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
  	$user = User::find($id);
  	$user->delete();
  	return Redirect::route('users')->with('message', 'User ' . $user->name . ' deleted');
  }

}
