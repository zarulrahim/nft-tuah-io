<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Giveaway;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;

class PentasApiController extends Controller
{
    public static function owner_profile($wallet_address)
    {
        $userResponse = Http::get('https://api.pentas.io/user/'.$wallet_address);
        $analyticResponse = Http::get('https://api.pentas.io/analytics/user/'.$wallet_address);
        $userAnalyticMerge = array_merge($userResponse->json(), $analyticResponse->json());
        return $userAnalyticMerge;
    }

    public function nfts(Request $request, $wallet_address)
    {
        $response = Http::get('https://api.pentas.io/user/'.$wallet_address.'/nft/'.$request->query('filter').'?page='.$request->query('page').'&sorting='.$request->query('sorting_type'));
        return $response->json();
    }

    public function nft($pentas_id)
    {
        $response = Http::get('https://api.pentas.io/nft/'.$pentas_id);
        return $response->json();
    }

    public function nft_sale_history($pentas_id)
    {
        $response = Http::get('https://api.pentas.io/sale/'.$pentas_id);
        return collect(json_decode($response));
    }
    
    public function nft_transaction($pentas_id)
    {
        $response = Http::get('https://api.pentas.io/transaction/'.$pentas_id);
        return collect(json_decode($response));
    }

    // V2

    public static function pullNfts($page, $filter, $sorting_type,$wallet_address)
    {
        $response = Http::get('https://api.pentas.io/user/'.$wallet_address.'/nft/'.$filter.'?page='.$page.'&sorting='.$sorting_type);
        return collect(json_decode($response));
    }

    public function nfts_based_on_user(Request $request, $subdomain, $type, $id)
    {
        $wallet_address = //'0x814197E709746FdB5CaD76615FcCF84b0B7263B9'; 
        User::where('subdomain', $subdomain)->first()->eth_address;
        $module = null;
        if ($type === "giveaways") {
            $module = Giveaway::with('event_nfts')->find($id);
        } else if ($type === 'campaigns') {
            $module = Giveaway::with('event_nfts')->find($id);
        }
        // Get total page by dividing owner minted nfts and per-page-display-count from owner profile api
        $total_pages = ceil(json_decode(collect($this->owner_profile($wallet_address)))->mintCount / 8);
        $nfts = [];
        for ($current_page = 0; $current_page < $total_pages; $current_page++) {
            array_push($nfts, $this->pullNfts($current_page, $request->query('filter'), $request->query('sorting_type'), $wallet_address));
        };
        $eligible_nfts = [];
        foreach (collect($module->event_nfts) as $nft) {
            array_push($eligible_nfts, $nft->nft_id);
        };
        $filtered = [];
        if (collect($eligible_nfts)->count() > 0) {
            foreach($eligible_nfts as $tokenId) {
                array_push($filtered, collect($nfts)->flatten()->where('scTokenId', $tokenId)->all());
            }
        } else {
            array_push($filtered, collect($nfts)->flatten());
        }
        $filteredNfts = [];
        // $filteredNftsWithTransactions = [];
        foreach (collect($filtered)->flatten() as $filteredNft) {
            // Get transaction 
            // $transactions = $this->nft_transaction($filteredNft->scTokenId);
            array_push($filteredNfts, collect($filteredNft));
            // array_push($filteredNfts, collect($filteredNft)->merge((object)["transactions" => collect($transactions)]));
        };

        $unique_owners = [];
        foreach (collect($filteredNfts)->unique('ownerDetails.username') as $filteredNft) {
            $collected = collect($filteredNfts)->where('ownerDetails.username', $filteredNft['ownerDetails']->username)->where('owner', '!=', $wallet_address)->values();
            $merged = collect($filteredNft['ownerDetails'])->merge((object)["collected" => $collected, "total_collection" => collect($collected)->count()]);
            array_push($unique_owners, $merged);
        };
    
        $sorted = collect($unique_owners)->where('total_collection', '>', 0)->sortBy([
            ['total_collection', 'desc']
        ]);

        return $sorted;
    }

    public function selected_nfts(Request $request, $subdomain, $type, $id)
    {
        $wallet_address = User::where('subdomain', $subdomain)->first()->eth_address;
        $module = null;
        if ($type === "giveaways") {
            $module = Giveaway::with('event_nfts')->find($id);
        } else if ($type === 'campaigns') {
            $module = Giveaway::with('event_nfts')->find($id);
        }
        $nfts = [];
        // $campaign->event_nfts->map(function ($nft) {
        //     $response = Http::get('https://api.pentas.io/nft/'.$nft->nft_id);
        //     array_push($nfts, collect(json_decode($response)));
        // });
        foreach ($module->event_nfts as $nft) 
        {
            $response = Http::get('https://api.pentas.io/nft/'.$nft->nft_id);
            array_push($nfts, collect(json_decode($response)));
        }
        return $nfts;
    }
    

    // for orbitz.tuah.io
    public function user_with_nfts(Request $request, $wallet_address)
    {
        // $wallet_address = //'0x814197E709746FdB5CaD76615FcCF84b0B7263B9'; 
        // User::where('subdomain', $subdomain)->first()->eth_address;
        // $module = null;
        // if ($type === "giveaways") {
        //     $module = Giveaway::with('event_nfts')->find($id);
        // } else if ($type === 'campaigns') {
        //     $module = Giveaway::with('event_nfts')->find($id);
        // }
        // Get total page by dividing owner minted nfts and per-page-display-count from owner profile api
        $total_pages = ceil(json_decode(collect($this->owner_profile($wallet_address)))->mintCount / 8);
        $nfts = [];
        for ($current_page = 0; $current_page < $total_pages; $current_page++) {
            array_push($nfts, $this->pullNfts($current_page, $request->query('filter'), $request->query('sorting_type'), $wallet_address));
        };
        // $eligible_nfts = [];
        // foreach (collect($module->event_nfts) as $nft) {
        //     array_push($eligible_nfts, $nft->nft_id);
        // };
        $filtered = [];
        // if (collect($eligible_nfts)->count() > 0) {
        //     foreach($eligible_nfts as $tokenId) {
        //         array_push($filtered, collect($nfts)->flatten()->where('scTokenId', $tokenId)->all());
        //     }
        // } else {
            array_push($filtered, collect($nfts)->flatten());
        // }
        $filteredNfts = [];
        // $filteredNftsWithTransactions = [];
        foreach (collect($filtered)->flatten() as $filteredNft) {
            // Get transaction 
            // $transactions = $this->nft_transaction($filteredNft->scTokenId);
            array_push($filteredNfts, collect($filteredNft));
            // array_push($filteredNfts, collect($filteredNft)->merge((object)["transactions" => collect($transactions)]));
        };

        $unique_owners = [];
        foreach (collect($filteredNfts)->unique('ownerDetails.username') as $filteredNft) {
            $collected = collect($filteredNfts)->where('ownerDetails.username', $filteredNft['ownerDetails']->username)->where('owner', '!=', $wallet_address)->values();
            $merged = collect($filteredNft['ownerDetails'])->merge((object)["collected" => $collected, "total_collection" => collect($collected)->count()]);
            array_push($unique_owners, $merged);
        };
    
        $sorted = collect($unique_owners)->where('total_collection', '>', 0)->sortBy([
            ['total_collection', 'desc']
        ]);

        return $sorted;
    }

}
