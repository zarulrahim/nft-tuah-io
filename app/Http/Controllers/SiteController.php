<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Site/Homepage');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showGiveaways($subdomain, $id)
    {
        $giveaway = User::where('subdomain', $subdomain)->first()->giveaways()->with('user', 'event_gifts','event_gifts.gift', 'event_custom_addresses', 'event_gifts.gift.gift_custom_fields', 'event_gifts.gift.nft_collection_group', 'event_gifts.gift.nft_collection', 'redemptions', 'redemptions.gift')->find($id);
        abort_if(!$giveaway, Response::HTTP_FORBIDDEN, '403 Forbidden');
        return Inertia::render('Site/Giveaway/Show', [
            'giveaway' => $giveaway
        ]);
    }

    public function showCampaigns($subdomain, $id)
    {
        $giveaway = User::where('subdomain', $subdomain)->first()->giveaways()->with('user', 'event_gifts','event_gifts.gift', 'event_gifts.gift.gift_custom_fields', 'redemptions', 'redemptions.gift')->find($id);
        abort_if(!$giveaway, Response::HTTP_FORBIDDEN, '403 Forbidden');
        return Inertia::render('Site/Campaign/Show', [
            'giveaway' => $giveaway
        ]);
    }

    public function circles()
    {
        return Inertia::render('Site/Circle/Homepage');
    }

    public function circleResults()
    {
        return Inertia::render('Site/Circle/Result');
    }
}
