<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\StoreGiftRequest;
use App\Models\EventGift;
use App\Models\EventNft;
use App\Helper\Helper;

class GiftController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $gifts = Auth::user()->gifts()->get();
        return Inertia::render('Gift/Index', [ "gifts" => $gifts, "giftTypeList" => Helper::giftTypeList() ]);
    }

    public function show($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $gift = Auth::user()->gifts()->find($id);
        return Inertia::render('Gift/Show', [ "gift" => $gift ]);
    }

    public function edit($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $gift = Auth::user()->gifts()->find($id);
        $nft_collections = Auth::user()->nft_collections()->get();
        $nft_collection_groups = Auth::user()->nft_collection_groups()->with('nft_collections')->get();
        return Inertia::render('Gift/Edit', [ "giftTypeList" => Helper::giftTypeList(), "nft_collections" => $nft_collections, "nft_collection_groups" => $nft_collection_groups, "gift" => $gift ]);
    }

    private function is_base64_encoded($string) 
    {
        // Check if there is no invalid character in string
        if (!preg_match('/^(?:[data]{4}:(text|image|application)\/[a-z]*)/', $string)){
            return false;
        } else {
            return true;
        }
     }

    public function update(StoreGiftRequest $request, $id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $gift = Auth::user()->gifts()->find($id);
        $gift->update(array($request->validated())[0]);
        // Upload Image
        if ($request->poster) {
            if ($this->is_base64_encoded($request->poster)) {
                // Deleting previous image
                Storage::disk('digitalocean')->delete($gift->image_url);
                // Storing new image
                $directory = "images/" . Auth::user()->subdomain . "/gifts";
                $raw = $request->poster;
                $extension = explode('/', explode(':', substr($raw, 0, strpos($raw, ';')))[1])[1];
                $fileName = 'tuah-io-gift-img-' . $gift->id . '-' . time() . Str::random(6) . '.' . $extension;
                $image = \Image::make($raw);
                $image->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::disk('digitalocean')->put($directory . '/' . $fileName, $image->stream(), 'public');
                $gift->image_url = $directory . '/' . $fileName;
            } 
        }
        $gift->save();
        return redirect('/dashboard/gifts')
        ->with(
            'message',
            '"'. $gift->title . '"' . ' has succesfully updated!'
        );
    }

    public function create()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collections = Auth::user()->nft_collections()->get();
        $nft_collection_groups = Auth::user()->nft_collection_groups()->with('nft_collections')->get();
        return Inertia::render('Gift/Create', [ "giftTypeList" => Helper::giftTypeList(), "nft_collections" => $nft_collections, "nft_collection_groups" => $nft_collection_groups ]);
    }

    public function store(StoreGiftRequest $request) {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $gift = Auth::user()->gifts()->create(array($request->validated())[0]);
        if ($request->gift_type_id === 1) { // only run this process if gift_type is fixed merchandise
            // Create Custom Attributes
            foreach ($request->custom_attributes as $attribute) 
            {
                $gift->gift_custom_fields()->create([
                    "name" => $attribute['attribute_name'],
                    "type" => "text",
                ]);
            }
             // Upload Image
            if ($request->poster) {
                $directory = "images/" . Auth::user()->subdomain . "/gifts";
                $raw = $request->poster;
                $extension = explode('/', explode(':', substr($raw, 0, strpos($raw, ';')))[1])[1];
                $fileName = 'tuah-io-gift-img-' . $gift->id . '-' . time() . Str::random(6) . '.' . $extension;
                $image = \Image::make($raw);
                $image->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::disk('digitalocean')->put($directory . '/' . $fileName, $image->stream(), 'public');
                $gift->image_url = $directory . '/' . $fileName;
            }
        }
        $gift->save();
        return redirect('/dashboard/gifts')
        ->with(
            'message',
            '"'. $gift->title . '"' . ' has succesfully created!'
        );
    }

    public function destroy($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $gift = Auth::user()->gifts()->find($id);
        $temp = $gift->replicate();
        Storage::disk('digitalocean')->delete($gift->image_url);
        $gift->delete();
        return redirect('/dashboard/gifts')
        ->with(
            'message',
            '"'. $temp->title . '"' . ' has succesfully deleted!'
        );
    }
}
