<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\StoreGiveawayRequest;
use App\Models\EventGift;
use App\Models\EventNft;
use App\Models\EventCustomAddress;

class GiveawayController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $giveaways = Auth::user()->giveaways()->with('event_nfts', 'event_gifts')->get();
        return Inertia::render('Giveaway/Index', [ "giveaways" => $giveaways ]);
    }

    public function show($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $giveaway = Auth::user()->giveaways()->with('event_nfts', 'event_gifts')->find($id);
        return Inertia::render('Giveaway/Show', [ "giveaway" => $giveaway ]);
    }

    public function edit($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $giveaway = Auth::user()->giveaways()->with('event_nfts', 'event_gifts', 'event_custom_addresses')->find($id);
        $gifts = Auth::user()->gifts()->get();
        return Inertia::render('Giveaway/Edit', [ "giveaway" => $giveaway, "gifts" => $gifts ]);
    }

    private function is_base64_encoded($string) 
    {
        // Check if there is no invalid character in string
        if (!preg_match('/^(?:[data]{4}:(text|image|application)\/[a-z]*)/', $string)){
            return false;
        } else {
            return true;
        }
     }

    public function update(StoreGiveawayRequest $request, $id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $giveaway = Auth::user()->giveaways()->find($id);
        $giveaway->update(array($request->validated())[0]);
        foreach ($request->event_gifts as $gift) {
            EventGift::updateOrCreate([
                "gift_id" => $gift['id'],
                "redeem_quantity" => $gift['redeem_quantity'],
                "hold_amount" => $gift['hold_amount'],
                "giveaway_id" => $giveaway->id
            ]);
        };
        collect($request->event_nfts)->map(function ($nft) use ($giveaway) {
            EventNft::updateOrCreate([
                "nft_id" => $nft,
                "giveaway_id" => $giveaway->id
            ]);
        });
        foreach ($request->event_custom_addresses as $custom_address) {
            EventCustomAddress::updateOrCreate([
                "address" => $custom_address['address'],
                "giveaway_id" => $giveaway->id
            ]);
        };
        // Upload Image
        if ($request->poster) {
            if ($this->is_base64_encoded($request->poster)) {
                // Deleting previous image
                Storage::disk('digitalocean')->delete($giveaway->image_url);
                // Storing new image
                $directory = "images/" . Auth::user()->subdomain . "/giveaways";
                $raw = $request->poster;
                $extension = explode('/', explode(':', substr($raw, 0, strpos($raw, ';')))[1])[1];
                $fileName = 'tuah-io-ga-img-' . $giveaway->id . '-' . time() . Str::random(6) . '.' . $extension;
                $image = \Image::make($raw);
                $image->resize(1200, 1200, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::disk('digitalocean')->put($directory . '/' . $fileName, $image->stream(), 'public');
                $giveaway->image_url = $directory . '/' . $fileName;
            } 
        }
        $giveaway->save();
        return redirect('/dashboard/giveaways')
        ->with(
            'message',
            '"'. $giveaway->title . '"' . ' has succesfully updated!'
        );
    }

    public function create()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $gifts = Auth::user()->gifts()->get();
        return Inertia::render('Giveaway/Create', [ "gifts" => $gifts ]);
    }

    public function store(StoreGiveawayRequest $request) {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $giveaway = Auth::user()->giveaways()->create(array($request->validated())[0]);
        foreach ($request->event_gifts as $gift) {
            EventGift::create([
                "gift_id" => $gift['id'],
                "redeem_quantity" => $gift['redeem_quantity'],
                "hold_amount" => $gift['hold_amount'],
                "giveaway_id" => $giveaway->id
            ]);
        };
        collect($request->event_nfts)->map(function ($nft) use ($giveaway) {
            EventNft::create([
                "nft_id" => $nft,
                "giveaway_id" => $giveaway->id
            ]);
        });
        foreach ($request->event_custom_addresses as $custom_address) {
            EventCustomAddress::create([
                "address" => $custom_address['address'],
                "giveaway_id" => $giveaway->id
            ]);
        };
        // Upload Image
        if ($request->poster) {
            $directory = "images/" . Auth::user()->subdomain . "/giveaways";
            $raw = $request->poster;
            $extension = explode('/', explode(':', substr($raw, 0, strpos($raw, ';')))[1])[1];
            $fileName = 'tuah-io-ga-img-' . $giveaway->id . '-' . time() . Str::random(6) . '.' . $extension;
            $image = \Image::make($raw);
            $image->resize(1200, 1200, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::disk('digitalocean')->put($directory . '/' . $fileName, $image->stream(), 'public');
            $giveaway->image_url = $directory . '/' . $fileName;
        }
        $giveaway->save();
        return redirect('/dashboard/giveaways')
        ->with(
            'message',
            '"'. $giveaway->title . '"' . ' has succesfully created!'
        );
    }

    public function destroy($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $giveaway = Auth::user()->giveaways()->find($id);
        $temp = $giveaway->replicate();
        // Deleting image from storage digitalocean
        Storage::disk('digitalocean')->delete($giveaway->image_url);
        $giveaway->delete();
        return redirect('/dashboard/giveaways')
        ->with(
            'message',
            '"'. $temp->title . '"' . ' has succesfully deleted!'
        );
    }
}
