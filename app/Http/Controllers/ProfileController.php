<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;

class ProfileController extends Controller
{
    public function edit()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return Inertia::render('Profile/Show');
    }
}
