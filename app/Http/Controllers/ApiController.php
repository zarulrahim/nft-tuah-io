<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Redemption;
use App\Models\Giveaway;
use Illuminate\Http\Request;
use Elliptic\EC;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use kornrunner\Keccak;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\PentasApiController;
use Illuminate\Support\Facades\Mail;
use App\Mail\GiveawayRedemption;
use DateTime;

class ApiController extends Controller
{
  public function verifyUser(Request $request)
  {
    if (! $this->authenticate($request)) {
      throw ValidationException::withMessages([
        'signature' => 'Invalid signature.'
      ]);
    }
    $user = User::where('eth_address', $request->eth_address)->exists();
    if ($user) {
      Auth::login(User::where('eth_address', $request->eth_address)->first());	
      // if ($authorized) {
        // return Redirect::route('home');
        return response()->json([ 'status' => 1, "message" => "User verified and logged in" ]);
      // }
    } else {
      return response()->json([ 'status' => 0, "message" => "User not found" ]);
      // if ($request->eth_address === "0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0") {
      //   $newUser = User::firstOrCreate([ 'name' => $request->eth_address, 'eth_address' => $request->eth_address, 'status' => 'active' ]);
      //   if ($newUser) {
      //     $newUser->roles()->sync(1);
      //     Auth::login($newUser);          
      //   }
      // } else {
      //   $newUser = User::firstOrCreate([ 'name' => $request->eth_address, 'eth_address' => $request->eth_address, 'status' => 'pending' ]);
      //   if ($newUser) {
      //     $newUser->roles()->sync(2);
      //     return response()->json([ 'status' => 'verified', 'user' => ])
      //   }            
      // }
      // Auth::login($newUser);
    }; 
  }

  public function createUser(Request $request)
  {
    if (! $this->authenticate($request)) {
      throw ValidationException::withMessages([
        'signature' => 'Invalid signature.'
      ]);
    }
    // if ($request->eth_address === "0x2032Bde1b65D453EC2b4D281F1d0341d82460dB0") {
    //   $newUser = User::firstOrCreate([ 
    //     'name' => $request->eth_address, 
    //     'eth_address' => $request->eth_address, 
    //     'name' => $request->name, 
    //     'email' => $request->email,
    //     'subdomain' => $request->subdomain, 
    //     'status' => 'active' 
    //   ]);
    //   if ($newUser) {
    //     $newUser->roles()->sync(1);
    //     $authorized = Auth::login($newUser); 
    //     if ($authorized) {
    //       return Redirect::route('home');
    //     }        
    //   }
    // } else {
      $exclude_subdomain_names = ['facebook', 'twitter', 'pentas', 'tuah', 'youtube', 'instagram', 'opensea', 'scam', 'spam'];
      $userExist = User::where('eth_address', $request->eth_address)->exists();
      if (!$userExist) {
        $emailExist = User::where('email', $request->email)->exists();
        if (!$emailExist) {
          $subdomainExist = User::where('subdomain', $request->subdomain)->exists();
          if (!$subdomainExist) {
            $subdomainInvalid = in_array($request->subdomain, $exclude_subdomain_names);
            if (!$subdomainInvalid) {
              $newUser = User::firstOrCreate([ 
                'eth_address' => $request->eth_address, 
                'name' => $request->name, 
                'email' => $request->email,
                'subdomain' => $request->subdomain, 
                'status' => 'active'
              ]);
              if ($newUser) {
                $newUser->roles()->sync(2);
                $newUser->subscriptions()->createMany(array([
                  "subscription_package_id" => 1,
                ]));
                Auth::login($newUser); 
                return response()->json([ 'status' => 1, "message" => "User created and logged in" ]);
                // if ($authorized) {
                  // return Redirect::route('home');
                // }
              } else {
                return response()->json([ 'status' => 0, "message" => "Something went wrong. Failed to create the user" ]);
              }
            } else {
              return response()->json([ "errorCode" => "ERRUSER03", "errorMessage" => "Subdomain is invalid" ]);
            }
          } else {
            return response()->json([ "errorCode" => "ERRUSER03", "errorMessage" => "Subdomain already exists" ]);
          }
        } else {
          return response()->json([ "errorCode" => "ERRUSER02", "errorMessage" => "Email already exists" ]);
        }
      } else {
        return response()->json([ "errorCode" => "ERRUSER01", "errorMessage" => "User already exists" ]);
      }    
    // }
  }

  public function updateWhitelistingRedemption($request, $giveaway, $event_gift)
  {
    // update redemption_gift_detail based on gift_type_id
    if ($event_gift->gift->gift_type_id === 1) { // Fixed Merchandise
      $redemption = Redemption::firstOrCreate([
        'eth_address' => $request->eth_address,
        'giveaway_id' => $giveaway->id,
        'gift_id' => $event_gift->gift_id,
        'name' => $request->name,
        'email' => $request->email,
        'code' => Str::random(6),
      ]);
      $redemption->save();
      if ($redemption) {
        // update redemption gift detail
        $redemption->redemption_gift_detail()->create();
        // Update redemption status detail
        $redemption->redemption_status_details()->create([
          'status' => 'processing',
          'message' => "User successfully redeemed the reward",
          'is_current' => true
        ]);
        if (collect($request->custom_fields)->count() > 0) {
          foreach ($request->custom_fields as $custom_field)
          {
            $redemption->gift_custom_field_datas()->create([
              "value" => $custom_field['value'],
              "gift_custom_field_id" => $custom_field['id'],
            ]);
          }
        }
        Mail::to($request->email)->send(new GiveawayRedemption($redemption, 'user'));
        Mail::to($giveaway->user->email)->send(new GiveawayRedemption($redemption, 'admin'));
        return response()->json([ "status" => "success", "redemption" => $giveaway->redemptions()->find($redemption->id)], 200)->send();
      } else {
        return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Something went wrong. Please try again later" ])->send();
      };
    } else if ($event_gift->gift->gift_type_id === 2) { // Fixed Airdrop
      $redemption = Redemption::firstOrCreate([
        'eth_address' => $request->eth_address,
        'giveaway_id' => $giveaway->id,
        'gift_id' => $event_gift->gift_id,
        'name' => $request->name,
        'email' => $request->email,
        'code' => Str::random(6),
      ]);
      $redemption->save();
      if ($redemption) {
        // update redemption gift detail
        $redemption->redemption_gift_detail()->create([
          "nft_collection_id" => $event_gift->gift->nft_collection->id
        ]);
        // Update redemption status detail
        $redemption->redemption_status_details()->create([
          'status' => 'processing',
          'message' => "User successfully redeemed the reward",
          'is_current' => true
        ]);
        if (collect($request->custom_fields)->count() > 0) {
          foreach ($request->custom_fields as $custom_field)
          {
            $redemption->gift_custom_field_datas()->create([
              "value" => $custom_field['value'],
              "gift_custom_field_id" => $custom_field['id'],
            ]);
          }
        }
        Mail::to($request->email)->send(new GiveawayRedemption($redemption, 'user'));
        Mail::to($giveaway->user->email)->send(new GiveawayRedemption($redemption, 'admin'));
        return response()->json([ "status" => "success", "redemption" => $giveaway->redemptions()->find($redemption->id)], 200)->send();
      } else {
        return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Something went wrong. Please try again later" ])->send();
      };
    } else if ($event_gift->gift->gift_type_id === 3) { // Random Multi Airdrops
      // Check if nft available at least one in collection
      $nft_ids = $event_gift->gift->nft_collection_group->nft_collections->pluck('id');
      $redeemed_nft_ids = $giveaway->redemptions()->count() > 0 ? $giveaway->redemptions->pluck('redemption_gift_detail.nft_collection_id') : collect([]);
      $available_nft_ids = [];
      foreach ($nft_ids as $nft_id) {
        if (!$redeemed_nft_ids->contains($nft_id)) {
          array_push($available_nft_ids, $nft_id);
        }
      }
      if (collect($available_nft_ids)->count() > 0) {
        $redemption = Redemption::firstOrCreate([
          'eth_address' => $request->eth_address,
          'giveaway_id' => $giveaway->id,
          'gift_id' => $event_gift->gift_id,
          'name' => $request->name,
          'email' => $request->email,
          'code' => Str::random(6),
        ]);
        $redemption->save();
        if ($redemption) {
          // update redemption gift detail
          $redemption->redemption_gift_detail()->create([
            "nft_collection_id" => collect($available_nft_ids)->random(), 
            "nft_collection_group_id" => $event_gift->gift->nft_collection_group->id,
          ]);
          // Update redemption status detail
          $redemption->redemption_status_details()->create([
            'status' => 'processing',
            'message' => "User successfully redeemed the reward",
            'is_current' => true
          ]);
          if (collect($request->custom_fields)->count() > 0) {
            foreach ($request->custom_fields as $custom_field)
            {
              $redemption->gift_custom_field_datas()->create([
                "value" => $custom_field['value'],
                "gift_custom_field_id" => $custom_field['id'],
              ]);
            }
          }
          Mail::to($request->email)->send(new GiveawayRedemption($redemption, 'user'));
          Mail::to($giveaway->user->email)->send(new GiveawayRedemption($redemption, 'admin'));
          return response()->json([ "status" => "success", "redemption" => $giveaway->redemptions()->find($redemption->id)], 200)->send();
        } else {
          return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Something went wrong. Please try again later" ])->send();
        };
      } else {
        return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Sorry. All NFTs in the collection has been redeemed" ])->send();
      }
    }
  }

  public function updateRedemption(Request $request)
  {
    $giveaway = Giveaway::find($request->giveaway_id);
    $event_gift = $giveaway->event_gifts->where("gift_id", $request->gift_id)->first();
    if ($giveaway->type === '1') {
      $this->updateWhitelistingRedemption($request, $giveaway, $event_gift);
    } else {
      $this->updateNormalRedemption($request, $giveaway, $event_gift);
    }


    // $giveaway = Giveaway::find($request->giveaway_id);
    // $event_gift = $giveaway->event_gifts->where("gift_id", $request->gift_id)->first();
    // if ($giveaway->type === '1') { // running this process if giveaway_type is for Current Holders
    //   $redemption = Redemption::firstOrCreate([
    //     'eth_address' => $request->eth_address,
    //     'giveaway_id' => $giveaway->id,
    //     'gift_id' => $event_gift->gift_id,
    //     'name' => $request->name,
    //     'email' => $request->email,
    //     'code' => Str::random(6),
    //   ]);
    //   $redemption->save();
    //   if ($redemption) {
    //     // Update redemption status detail
    //     $redemption->redemption_status_details()->create([
    //       'status' => 'processing',
    //       'message' => "User successfully redeemed the reward",
    //       'is_current' => true
    //     ]);
    //     if (collect($request->custom_fields)->count() > 0) {
    //       foreach ($request->custom_fields as $custom_field)
    //       {
    //         $redemption->gift_custom_field_datas()->create([
    //           "value" => $custom_field['value'],
    //           "gift_custom_field_id" => $custom_field['id'],
    //         ]);
    //       }
    //     }
    //     Mail::to($request->email)->send(new GiveawayRedemption($redemption, 'user'));
    //     Mail::to($giveaway->user->email)->send(new GiveawayRedemption($redemption, 'admin'));
    //     return response()->json([ "status" => "success", "redemption" => $redemption ]);
    //   } else {
    //     return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Something went wrong. Please try again later" ]);
    //   };
    // } else {
    //   $redemption = Redemption::firstOrCreate([
    //     'eth_address' => $request->eth_address,
    //     'giveaway_id' => $giveaway->id,
    //     'gift_id' => $event_gift->gift_id,
    //     // 'status' => 'success',
    //     'name' => $request->name,
    //     'email' => $request->email,
    //     'code' => Str::random(6),
    //   ]);
    //   $redemption->redemption_gift_detail()->create([
    //     "nft_collection_id" => 11, // dummy
    //     "nft_collection_group_id" => 11, // dummy
    //   ]);
    //   $redemption->save();
    //   if ($redemption) {
    //     // Update redemption status detail
    //     $redemption->redemption_status_details()->create([
    //       'status' => 'processing',
    //       'message' => "User successfully redeemed the reward",
    //       'is_current' => true
    //     ]);
    //     if (collect($request->custom_fields)->count() > 0) {
    //       foreach ($request->custom_fields as $custom_field)
    //       {
    //         $redemption->gift_custom_field_datas()->create([
    //           "value" => $custom_field['value'],
    //           "gift_custom_field_id" => $custom_field['id'],
    //         ]);
    //       }
    //     }
    //     Mail::to($request->email)->send(new GiveawayRedemption($redemption, 'user'));
    //     Mail::to($giveaway->user->email)->send(new GiveawayRedemption($redemption, 'admin'));
    //     return response()->json([ "status" => "success", "redemption" => $redemption ]);
    //   } else {
    //     return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Something went wrong. Please try again later" ]);
    //   };
    // }
  
    // $redemption = Redemption::find($request->redemption_id);
    // $redemption->status = "success";
    // $redemption->name = $request->name;
    // $redemption->email = $request->email;
    // // $redemption->twitter_username = $request->twitter_username;
    // $redemption->code = Str::random(6);
    // $redemption->save();
    // if ($redemption) {
    //   Mail::to($request->email)->send(new GiveawayRedemption($redemption));
    //   return response()->json([ "status" => "success", "redemption" => $redemption ]);
    // } else {
    //   return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Something went wrong. Please try again later" ]);
    // };
  }

  // public function updateRedemptionStatus(Request $request) // For creator only
  // {
  //   $redemption = Auth::user()->redemptions()->find($request->id);
  //   if ($redemption) {
  //     $redemption->redemption_status_details()->create([
  //       'status' => $request->status,
  //       'message' => $request->message,
  //       'is_current' => true
  //     ]);
  //     return response()->json([ "status" => "success" ]);
  //   } else {
  //     return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Action is invalid" ]);
  //   }
  // }

  public function updateNormalRedemption($request, $giveaway, $event_gift) 
  {
    $redemption = Redemption::firstOrCreate([
      'eth_address' => $request->eth_address,
      'giveaway_id' => $giveaway->id,
      'gift_id' => $event_gift->gift_id,
      // 'status' => 'success',
      'name' => $request->name,
      'email' => $request->email,
      'code' => Str::random(6),
    ]);
    $redemption->redemption_gift_detail()->create([
      "nft_collection_id" => 11, // dummy
      "nft_collection_group_id" => 11, // dummy
    ]);
    $redemption->save();
    if ($redemption) {
      // Update redemption status detail
      $redemption->redemption_status_details()->create([
        'status' => 'processing',
        'message' => "User successfully redeemed the reward",
        'is_current' => true
      ]);
      if (collect($request->custom_fields)->count() > 0) {
        foreach ($request->custom_fields as $custom_field)
        {
          $redemption->gift_custom_field_datas()->create([
            "value" => $custom_field['value'],
            "gift_custom_field_id" => $custom_field['id'],
          ]);
        }
      }
      Mail::to($request->email)->send(new GiveawayRedemption($redemption, 'user'));
      Mail::to($giveaway->user->email)->send(new GiveawayRedemption($redemption, 'admin'));
      return response()->json([ "status" => "success", "redemption" => $giveaway->redemptions()->find($redemption->id)], 200)->send();
    } else {
      return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Something went wrong. Please try again later" ])->send();
    };
  }

  public function verifyRedemption(Request $request)
  {
    if (! $this->authenticate($request)) {
      throw ValidationException::withMessages([
        'signature' => 'Invalid signature.'
      ]);
    }
    // Request will have -> eth_address, giveaway_id, gift_id
    $giveaway = Giveaway::find($request->giveaway_id);
    $event_gift = $giveaway->event_gifts->where('gift_id', $request->gift_id)->firstOrFail();
    if ($giveaway->type === '1') { // running this process if giveaway_type is for whitelisting giveaway
     // check existing redemption from giveaway redemption
      $redemption = $giveaway->redemptions()->where([['eth_address', '=', $request->eth_address],['gift_id', '=', $request->gift_id]])->exists();
      // Check if user is redeeming within the start_date and end_date
      if (new DateTime() > new DateTime($giveaway->start_date) && new DateTime() < new DateTime($giveaway->end_date)) {
        if (!$redemption) {
          $event_custom_addresses = [];
          foreach($giveaway->event_custom_addresses as $custom_address) {
            array_push($event_custom_addresses, $custom_address['address']);
          };
          if (in_array($request->eth_address, $event_custom_addresses)) {
            // Check gift_type_id whether "Fixed Merchandise / Fixed Airdrop / Random Multi Airdrops"
            if ($event_gift->gift->gift_type_id === 1) {
              return response()->json([ "status" => "verified", "type" => "Fixed Merchandise" ]);
            } else if ($event_gift->gift->gift_type_id === 2) {
              return response()->json([ "status" => "verified", "type" => "Fixed Airdrop" ]);
            } else if ($event_gift->gift->gift_type_id === 3) {
              // Check if nft available at least one in collection
              $nft_ids = $event_gift->gift->nft_collection_group->nft_collections->pluck('id');
              $redeemed_nft_ids = $giveaway->redemptions()->count() > 0 ? $giveaway->redemptions->pluck('redemption_gift_detail.nft_collection_id') : collect([]);
              $available_nft_ids = [];
              foreach ($nft_ids as $nft_id) {
                if (!$redeemed_nft_ids->contains($nft_id)) {
                  array_push($available_nft_ids, $nft_id);
                }
              }
              if (collect($available_nft_ids)->count() > 0) {
                return response()->json([ "status" => "verified", "type" => "Random Multi Airdrops" ]);
              } else {
                return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Sorry. All NFTs in the collection has been redeemed" ]);
              }
            }
          } else {
            return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Wallet not eligible to redeem" ]);
          }
        } else {
          return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Wallet already redeemed the reward" ]);
        }
      } else {
        return response()->json([ "errorCode" => "ERR000", "errorMessage" => "Giveaway is not available" ]);
      }      
    } else { // otherwise giveaway_type will be assumed as Normal giveaway for current NFT holders
      // check existing redemption from giveaway redemption
      $redemption = $giveaway->redemptions()->where('eth_address', $request->eth_address)->exists();
      // get assigned NFTs from giveaway
      $event_nfts = $giveaway->event_nfts;
      // get gift condition e.g requires 3 nfts or 1 nft
      // $event_gift = $giveaway->event_gifts->where("gift_id", $request->gift_id)->first();
      // Check if user is redeeming within the start_date and end_date
      if (new DateTime() > new DateTime($giveaway->start_date) && new DateTime() < new DateTime($giveaway->end_date)) {
        if (!$redemption) {
          if ($event_gift->redeem_quantity > 0) {
            // get user's owned nfts from Pentas API
            $user_pentas_collected_nfts = [];
            $new_array = [];
            $current_page = 1;

            do {
              $response = PentasApiController::pullNfts($current_page, "collected", "latest", $request->eth_address);
              $current_page++;
              $new_array = $response;
              array_push($user_pentas_collected_nfts, $new_array);
            } while ($new_array->count() === 8);

            // $total_pages = ceil(json_decode(collect(PentasApiController::owner_profile($request->eth_address)))->mintCount / 8);
            // for ($current_page = 0; $current_page < $total_pages; $current_page++) {
              // array_push($user_pentas_collected_nfts, PentasApiController::pullNfts($current_page, "collected", "latest", $request->eth_address));
            // };
            if (collect($user_pentas_collected_nfts)->count() > 0) {
              // create new redemption
              if ($giveaway != null && $event_gift != null) {
                // filtered fetched pentas nfts based on event_nfts
                $filtered = [];
                foreach($event_nfts as $event_nft) {
                  array_push($filtered, collect($user_pentas_collected_nfts)->flatten()->where('scTokenId', intval($event_nft->nft_id))->first());
                };
                if (collect($filtered)->filter()->count() > 0 && collect($filtered)->filter()->count() >= $event_gift->hold_amount) {
                  return response()->json([ "status" => "verified" ]);
                  // $newRedemption = Redemption::firstOrCreate([
                  //   'eth_address' => $request->eth_address,
                  //   'giveaway_id' => $giveaway->id,
                  //   'gift_id' => $event_gift->id,
                  //   'status' => 'pending'
                  // ]);
                  // $newRedemption->save();
                  // if ($newRedemption) {
                  //   return response()->json([ "status" => "verified", "redemption" => $newRedemption ]);
                  // } else {
                  //   return response()->json([ "errorCode" => "ERR006", "errorMessage" => "Something went wrong. Please try again later" ]);
                  // }
                } else {
                  return response()->json([ "errorCode" => "ERR005", "errorMessage" => "You don't have enough NFTs" ]);
                  // redeem invalid, user don't have enough nfts
                }
              } else {
                return response()->json([ "errorCode" => "ERR004", "errorMessage" => "Giveaway already expired" ]);
                // $giveaway or $event_gift not exists
              }
            } else {
              return response()->json([ "errorCode" => "ERR003", "errorMessage" => "Buy required NFTs and try again" ]);
              // return Redirect::route('home')->with('message', "user_pentas_collected_nfts invalid fetching");
              // $user_pentas_collected_nfts invalid fetching
            }
          } else {
            return response()->json([ "errorCode" => "ERR002", "errorMessage" => "Reward quantity is exceeded" ]);
            // $event_gift out of redeem_quantity 
          }
        } else {
          return response()->json([ "errorCode" => "ERR001", "errorMessage" => "Reward already redeemed" ]);
          // return Redirect::back([ "error" => "The reward is already redeemed"  ]);
          // return Redirect::route('home')->with('message', "redemption is exist, you cannot redeem twice");
          // $redemption is exist, you cannot redeem twice
        }
      } else {
        return response()->json([ "errorCode" => "ERR000", "errorMessage" => "Giveaway is not available" ]);
      }
    }
   
  }

  

  // public function postVerificationTweet(Request $request)
  // {

  // }

  protected function authenticate(Request $request): bool
  {
    return $this->verifySignature(
      $request->message,
      $request->signature,
      $request->eth_address,
    );
  }

  protected function verifySignature($message, $signature, $address): bool
  {
    $messageLength = strlen($message);
    $hash = Keccak::hash("\x19Ethereum Signed Message:\n{$messageLength}{$message}", 256);
    $sign = [
      "r" => substr($signature, 2, 64),
      "s" => substr($signature, 66, 64)
    ];

    $recId  = ord(hex2bin(substr($signature, 130, 2))) - 27;

    if ($recId != ($recId & 1)) {
      return false;
    }

    $publicKey = (new EC('secp256k1'))->recoverPubKey($hash, $sign, $recId);

    return $this->pubKeyToAddress($publicKey) === Str::lower($address);
  }

  protected function pubKeyToAddress($publicKey): string
  {
    return "0x" . substr(Keccak::hash(substr(hex2bin($publicKey->encode("hex")), 1), 256), 24);
  }
}
