<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\StoreNftCollectionGroupRequest;
use App\Models\NftCollectionGroupPivot;

class NftCollectionGroupController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection_groups = Auth::user()->nft_collection_groups()->with('nft_collections')->get();
        return Inertia::render('NftCollectionGroup/Index', [ "nft_collection_groups" => $nft_collection_groups ]);
    }

    public function show($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collections = Auth::user()->nft_collections()->get();
        $nft_collection_group = Auth::user()->nft_collection_groups()->with('nft_collections')->find($id);
        return Inertia::render('NftCollectionGroup/Show', [ "nft_collections" => $nft_collections,  "nft_collection_group" => $nft_collection_group ]);
    }

    public function create()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collections = Auth::user()->nft_collections()->get();
        return Inertia::render('NftCollectionGroup/Create', [ "nft_collections" => $nft_collections ]);
    }

    public function store(StoreNftCollectionGroupRequest $request)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection_group = Auth::user()->nft_collection_groups()->create(array($request->validated())[0]);
        // return $nft_collection_group->id;
        // collect($request->assigned_nfts)->map(function ($nft) {
        //     NftCollectionGroupPivot::create([
        //         "nft_collection_id" => $nft,
        //         "nft_collection_group_id" => $nft_collection_group->replicate()->id
        //     ]);
        // });
        $nft_collection_group->save();
        $nft_collection_group->nft_collections()->sync($request->assigned_nfts);
        // foreach ($request->assigned_nfts as $nft) 
        // {
        //     $nft_collection_group->nft_collections()->sync($nft);
        // }
        return redirect('/dashboard/collections')
        ->with(
            'message',
            '"'. $nft_collection_group->title . '"' . ' has succesfully created!'
        );
    }

    public function edit($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collections = Auth::user()->nft_collections()->get();
        $nft_collection_group = Auth::user()->nft_collection_groups()->with('nft_collections')->find($id);
        return Inertia::render('NftCollectionGroup/Edit', [ "nft_collections" => $nft_collections,  "nft_collection_group" => $nft_collection_group ]);
    }

    public function update(StoreNftCollectionGroupRequest $request, $id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection_group = Auth::user()->nft_collection_groups()->find($id);
        $nft_collection_group->update(array($request->validated())[0]);
        // return $nft_collection_group->id;
        // collect($request->assigned_nfts)->map(function ($nft) {
        //     NftCollectionGroupPivot::create([
        //         "nft_collection_id" => $nft,
        //         "nft_collection_group_id" => $nft_collection_group->replicate()->id
        //     ]);
        // });
        $nft_collection_group->save();
        $nft_collection_group->nft_collections()->sync($request->assigned_nfts);
        // foreach ($request->assigned_nfts as $nft) 
        // {
        //     $nft_collection_group->nft_collections()->sync($nft);
        // }
        return redirect('/dashboard/collections')
        ->with(
            'message',
            '"'. $nft_collection_group->title . '"' . ' has succesfully created!'
        );
    }

    public function destroy($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection_group = Auth::user()->nft_collection_groups()->find($id);
        $temp = $nft_collection_group->replicate();
        $nft_collection_group->delete();
        return redirect('/dashboard/collections')
        ->with(
            'message',
            '"'. $temp->title . '"' . ' has succesfully deleted!'
        );
    }
}
