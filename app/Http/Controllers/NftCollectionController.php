<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\StoreNftCollectionRequest;

class NftCollectionController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collections = Auth::user()->nft_collections()->get();
        return Inertia::render('NftCollection/Index', [ "nft_collections" => $nft_collections ]);
    }

    public function show($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection = Auth::user()->nft_collections()->find($id);
        return Inertia::render('NftCollection/Show', [ "nft_collection" => $nft_collection ]);
    }

    public function create()
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return Inertia::render('NftCollection/Create');
    }

    public function store(StoreNftCollectionRequest $request)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection = Auth::user()->nft_collections()->create(array($request->validated())[0]);
        // Upload Image
        if ($request->poster) {
            $directory = "images/" . Auth::user()->subdomain . "/nft_collections";
            $raw = $request->poster;
            $extension = explode('/', explode(':', substr($raw, 0, strpos($raw, ';')))[1])[1];
            $fileName = 'tuah-io-nft-img-' . $nft_collection->id . '-' . time() . Str::random(6) . '.' . $extension;
            $image = \Image::make($raw);
            $image->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::disk('digitalocean')->put($directory . '/' . $fileName, $image->stream(), 'public');
            $nft_collection->image_url = $directory . '/' . $fileName;
        }
        $nft_collection->save();
        return redirect('/dashboard/nfts')
        ->with(
            'message',
            '"'. $nft_collection->title . '"' . ' has succesfully created!'
        );
    }

    public function edit($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection = Auth::user()->nft_collections()->find($id);
        return Inertia::render('NftCollection/Edit', [ "nft_collection" => $nft_collection ]);
    }

    public function update(StoreNftCollectionRequest $request, $id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection = Auth::user()->nft_collections()->find($id);
        $nft_collection->update(array($request->validated())[0]);
        // Upload Image
        if ($request->poster) {
            if ($this->is_base64_encoded($request->poster)) {
                // Deleting previous image
                Storage::disk('digitalocean')->delete($nft_collection->image_url);
                // Storing new image
                $directory = "images/" . Auth::user()->subdomain . "/nft_collections";
                $raw = $request->poster;
                $extension = explode('/', explode(':', substr($raw, 0, strpos($raw, ';')))[1])[1];
                $fileName = 'tuah-io-nft-img-' . $nft_collection->id . '-' . time() . Str::random(6) . '.' . $extension;
                $image = \Image::make($raw);
                $image->resize(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                });
                Storage::disk('digitalocean')->put($directory . '/' . $fileName, $image->stream(), 'public');
                $nft_collection->image_url = $directory . '/' . $fileName;
            } 
        }
        $nft_collection->save();
        return redirect('/dashboard/nfts')
        ->with(
            'message',
            '"'. $nft_collection->title . '"' . ' has succesfully created!'
        );
    }

    public function destroy($id)
    {
        abort_if(Gate::denies('user_basic_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $nft_collection = Auth::user()->nft_collections()->find($id);
        $temp = $nft_collection->replicate();
        Storage::disk('digitalocean')->delete($nft_collection->image_url);
        $nft_collection->delete();
        return redirect('/dashboard/nfts')
        ->with(
            'message',
            '"'. $temp->title . '"' . ' has succesfully deleted!'
        );
    }

    private function is_base64_encoded($string) 
    {
        // Check if there is no invalid character in string
        if (!preg_match('/^(?:[data]{4}:(text|image|application)\/[a-z]*)/', $string)){
            return false;
        } else {
            return true;
        }
    }
}
