<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        '/redeem-web3',
        '/api/verify_redemption',
        '/api/update_redemption',
        '/api/verify_user',
        '/api/create_user',
    ];
}
