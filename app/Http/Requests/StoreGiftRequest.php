<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreGiftRequest  extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_basic_access');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required'
            ],
            'remark' => [
                'string',
                'required'
            ],
            'gift_type_id' => [
                'integer',
                'required'
            ],
            'amount' => [
                'regex:/^\d+(\.\d{1,2})?$/',
                'nullable',
                'required_if:gift_type_id,1'
            ],
            'nft_collection_group_id' => [
                'integer',
                'nullable',
                'required_if:gift_type_id,3'
            ],
            'nft_collection_id' => [
                'integer',
                'nullable',
                'required_if:gift_type_id,2'
            ],
            'poster' => [
                'string',
                'nullable',
                'required_if:gift_type_id,1',
            ],
        ];
    }
}
