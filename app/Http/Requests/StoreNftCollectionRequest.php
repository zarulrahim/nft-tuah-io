<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreNftCollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_basic_access');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required'
            ],
            'remark' => [
                'string',
                'nullable',
            ],
            'external_url' => [
                'string',
                'nullable',
            ],
            'marketplace_url' => [
                'string',
                'nullable',
            ],
            'currency' => [
                'string',
                'required'
            ],
            'amount' => [
                'regex:/^\d+(\.\d{1,2})?$/',
                'required'
            ],
            'poster' => [
                'string',
                'required',
            ],
        ];
    }
}
