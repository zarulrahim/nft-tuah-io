<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreShowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_access');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => [
                'string',
                'required',
            ],
            'summary'     => [
                'string',
                'required',
            ],
            'genre'     => [
                'integer',
                'required',
            ],
            'seasons'     => [
                'array',
                'required_if:type,==,2',
            ],
            'start_date'     => [
                'date',
                'required',
            ],
            'end_date'     => [
                'date',
                'required',
            ],
        ];
    }
}
