<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreEpisodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_access');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'show_id'     => [
                'string',
                'required',
            ],
            'season_id'     => [
                'string',
                'required',
            ],
            'title'     => [
                'string',
                'required',
            ],
            'summary'     => [
                'string',
                'required',
            ],
            'airdate'     => [
                'date',
                'required',
            ],
            'poster'     => [
                'mimes:jpeg,png,jpg,gif,svg',
                'required',
            ],
            'duration'     => [
                'integer',
                'required',
            ],
            'transcode_media'     => [
                'mimetypes:video/x-ms-asf,video/x-flv,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv,video/avi',
                'required',
            ],
        ];  
    }
}
