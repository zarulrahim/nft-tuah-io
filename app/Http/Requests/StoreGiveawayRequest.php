<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreGiveawayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('user_basic_access');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required'
            ],
            'type' => [
                'string',
                'required',
            ],
            'remark' => [
                'string',
                'required'
            ],
            'start_date' => [
                'date',
                'required'
            ],
            'end_date' => [
                'date',
                'required'
            ],
            'event_gifts' => [
                'array',
                'required'
            ],
            'event_custom_addresses' => [
                'array',
                'required'
            ],
            'event_nfts' => [
                'array',
                'required_if:type,=,"2"'
            ],
            'poster' => [
                'string',
                'required',
            ],
        ];
    }
}
